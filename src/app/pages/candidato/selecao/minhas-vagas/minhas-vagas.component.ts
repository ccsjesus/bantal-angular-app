import { noImage } from './../../../constantes/bantal_b64';
import { MensagemModel } from './../../../models/mensagem';
import { Component, OnInit, Injector, ElementRef, ViewChild, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { VagasCandidatoServices } from '../../../../shared/services/vaga.candidato.service';
import { Vaga } from '../../../models/vaga.model';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { FUNCAO_CANDIDATO } from '../../../constantes';
import { DadosServices } from '../../../../shared/services/dados.service';
import { DadosUsuarioModel } from '../../../models/dados-usuario.model';
import { NbActionComponent } from '@nebular/theme';
import { MensagemFromRecruiterService } from './../../../../shared/services/mensagem-from-recruiter.services';
import { map, delay, switchMap } from 'rxjs/operators';
import { forkJoin, fromEvent, of } from "rxjs";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import Swal from 'sweetalert2';



@Component({
  selector: 'minhas-vagas',
  templateUrl: './minhas-vagas.component.html',
  styleUrls: ['./minhas-vagas.component.css']
})

export class MinhasVagasComponent extends BaseResourceFormComponent<Vaga> implements OnInit {

  possuiVaga: Boolean = false;
  textoCarregando: string = 'Carregando suas vagas!'
  textoAguarde: string = 'Aguarde!'
  exibirLoading: Boolean = true;
  formDadosMinhasVagas: FormGroup;
  vagas: Vaga[];
  chats = [];
  @ViewChildren('nbAction') actionIcon:QueryList<any>;
  usuarioModel: DadosUsuarioModel;
  qtdMsg;
  semImagem: any = noImage;

  responsiveOptions;

  carregarBadgeMsgNaoLida(listaVagas) {
    setTimeout( ()=> {
      var instancia = this;
      this.actionIcon.toArray().forEach(function(value, key) {
        var chaveVaga = value.title.split(' - ')[0];
        listaVagas.forEach(element => {
          if(element.id === chaveVaga){
            value.badgeText = element.mensagens.length;
          }
        });
      });
    }, 1000 )

  }

  constructor(private fb: FormBuilder,
              protected injector: Injector,
              protected vagasCandadidatoServices: VagasCandidatoServices,
              protected router: Router,
              protected dadosUsuario: DadosServices,
              protected msgFromRecruiter: MensagemFromRecruiterService,
              private sanitizer: DomSanitizer) {
    super(injector, new Vaga(), null, Vaga.fromJson);

    this.responsiveOptions = [
      {
        breakpoint: '2560px',
        numVisible: 3,
        numScroll: 1
      },
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 1
      },
      {
          breakpoint: '768px',
          numVisible: 2,
          numScroll: 1
      },
      {
          breakpoint: '560px',
          numVisible: 1,
          numScroll: 1
      }
    ];

    this.dadosUsuario.obterDadosUsuarioToken().subscribe(
      dados => {
        this.usuarioModel = dados;
      }
    );

  }

  adicionarTag(valor){
    var foto;
    if(valor == null){
      foto = this.semImagem;
    } else{
      foto = valor;
    }
    return `data:image/png;base64,` + (this.sanitizer.bypassSecurityTrustResourceUrl(foto) as any).changingThisBreaksApplicationSecurity;
  }

  ngOnInit(): void {

    this.formDadosMinhasVagas = this.fb.group({
      vagas: ['']
    });

    let listaCdVagas = [];
    let listaVagas = [];
    const joinedAndDelayed$ =
      this.vagasCandadidatoServices.getMinhaVaga().subscribe(valor => {
      if(valor !== undefined) {
        this.possuiVaga = true;
        this.vagas = valor;
        this.carregarBadgeMsgNaoLida(valor);
      } else {
        this.exibirLoading = false;
        this.textoCarregando = 'Não foram encontradas vagas na sua conta!';
        this.textoAguarde = 'Participe de alguma seleção.';
      }
    });

  }

  armazenarListaMensagens(listaMsg){
    sessionStorage.setItem('listaMsg', JSON.stringify(listaMsg));
  }

  protected setCurrentAction() {

  }
  protected buildResourceForm(): void {

  }
  protected onLoadConfigurations(): void {

  }


  loadMinhasVaga(valor){
    const mapped = Object.keys(valor).map(key => ({type: key, value: valor[key]}));
    const control = <FormArray>this.formDadosMinhasVagas.controls['vagas'];
    mapped.forEach(vaga => control.push(this.fb.group(vaga.value)));
  }

  redirecionarPainelMensagensCandidato(cdVagaTituloSelecao: string){
    let dadosSala = this.obterDadosSalaChat(cdVagaTituloSelecao);

    this.vagas.forEach((value, key) => {
      if(value.id + '' === cdVagaTituloSelecao.split(' - ')[0]){
        this.armazenarListaMensagens(value.mensagens);
      }
    });

    sessionStorage.setItem('dadosSala', JSON.stringify(dadosSala));

    this.router.navigate(['/mensagem-candidato']);
    //this.router.navigate(['/mensagem-candidato']);
  }

  obterDadosSalaChat(cdVagaTituloSelecao){
    let dadosSala = {
      cpfcandidato: this.usuarioModel.identificacao,
      codigoselecao: cdVagaTituloSelecao,
    }
    return dadosSala;
  }

  obterString(val) {
    return (val + "").replace(/<[^>]*>?/gm, '');;
  }
  
  protected removerCandidatura(vaga: any) {

    var foto;
    if(vaga.foto == null){
      foto = this.semImagem;
    } else{
      foto = vaga.foto;
    }
    var areaAtuacao = vaga.areaAtuacaoModel.name === null ? 'Não determinado' :  vaga.areaAtuacaoModel.name;

    Swal.fire({
      title: vaga.id +" - "+ vaga.titulo,
      html: '<div class="row" style="align-items: center;justify-content: center;">'+
            '<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
            '</div>'+
            '<div class="row">'+
            ' <label><b>Título da Vaga:</b></label>'+
            ' &nbsp ' + vaga.id +" - "+ vaga.titulo +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Descrição:</b></label>'+
            ' &nbsp ' + this.removeCaractereHTML(vaga.descricao) +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Data Limite:</b></label>'+
            ' &nbsp ' + vaga.dataLimite +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Localização:</b></label>'+
            ' &nbsp ' + vaga.localizacao +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Salário:</b></label>'+
            ' &nbsp ' + vaga.salario +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Área de Atuação:</b></label>'+
            ' &nbsp ' + areaAtuacao +
            '</div>'
            ,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonColor: '#d33',
            confirmButtonText:
              '<i class="fa fa-check"></i> Remover Inscrição.',
            confirmButtonAriaLabel: 'Inscrição removida com sucesso!',
            cancelButtonText:
              '<i class="fa fa-times"></i> Cancelar.',
            cancelButtonAriaLabel: 'Thumbs down'

  }).then((result) => {
    if (result.value) {
      this.vagasCandadidatoServices.removerInscricaoVaga(vaga.id).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Sua inscrição foi removida com sucesso!',
          'success'
        ).then((resultado) => {
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./selecao']);
          });
        })
      });
    }
  })
  }
  
  removeCaractereHTML(text){
    return text.replace(/<style[^>]*>.*<\/style>/gm, '')
   // Remove script tags and content
   .replace(/<script[^>]*>.*<\/script>/gm, '')
   // Remove all opening, closing and orphan HTML tags
   .replace(/<[^>]+>/gm, '')
   // Remove leading spaces and repeated CR/LF
   .replace(/([\r\n]+ +)+/gm, '');
 }

}
