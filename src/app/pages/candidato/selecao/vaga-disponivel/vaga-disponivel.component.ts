import { Router } from '@angular/router';
import { Component, OnInit, Injector } from '@angular/core';
import { CarService } from '../../../../shared/services/carservices';
import { VagaServices } from '../../../../shared/services/vaga.service';
import { Vaga } from '../../../models/vaga.model';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';
import { VagasEmpresaServices } from '../../../../shared/services/vagas.empresa.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { noImage } from '../../../constantes/bantal_b64';
import Swal from 'sweetalert2';
import { VagasCandidatoServices } from '../../../../shared/services/vaga.candidato.service';
import { InscricaoVaga } from '../../../models/inscricao-vaga.model';
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { DadosServices } from '../../../../shared/services/dados.service';


@Component({
  selector: 'vaga-disponivel',
  templateUrl: './vaga-disponivel.component.html',
  styleUrls: ['./vaga-disponivel.component.css']
})
export class VagaDisponivelComponent extends BaseResourceFormComponent<Vaga> implements OnInit {

  public mainImage = 'https://angular.io/assets/images/logos/angular/logo-nav@2x.png';
  cars: any[];
  formDadosVagasEmpresa: FormGroup;
  vagas: Vaga[];
  semImagem: any = noImage;
  ready: Boolean = false;
  linkSearch = '/find-vaga-empresa';
  textoAguarde: string = 'Aguarde!'
  exibirLoading: Boolean = true;
	exibirNaoHaVagas: Boolean = false;
  curriculoCandidatoValido : Boolean = false;

  responsiveOptions;

  constructor(private fb: FormBuilder,
    private carService: CarService,
    protected injector: Injector,
    protected vagasServices: VagaServices,
    protected vagaCandidatoServices: VagasCandidatoServices,
    protected vagasEmpresaServices: VagasEmpresaServices,
    protected dadosServices: DadosServices,
    protected router: Router,
    private sanitizer: DomSanitizer) {

    super(injector, new Vaga(), null, Vaga.fromJson);
		this.responsiveOptions = [
      {
        breakpoint: '2560px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
          breakpoint: '768px',
          numVisible: 2,
          numScroll: 2
      },
      {
          breakpoint: '560px',
          numVisible: 1,
          numScroll: 1
      }
    ];
	}


	ngOnInit() {
    let img = new Image();

    setTimeout( ()=> { // only for delay
      //img.src = this.mainImage;
    }, 5000 )

		this.formDadosVagasEmpresa = this.fb.group({
      vagas: ['']
    });

    this.vagasEmpresaServices.getVagasVigentes().subscribe(valor => {
      if(valor.length > 0) {
        this.vagas = valor;
        this.ready = true;
      } else {
          this.ready = true;
          this.exibirNaoHaVagas = true;
          this.textoAguarde = 'Não foram encontradas vagas disponíveis.';
      }
    });

    this.dadosServices.obterDadosUsuario().subscribe((users) => {
      this.curriculoCandidatoValido = users.curriculoCandidatoValido;
    });
  }
  adicionarTag(valor){
    var foto;
    if(valor == null){
      foto = this.semImagem;
    } else{
      foto = valor;
    }
    return this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + foto);
  }

  protected realizarCandidatura(vaga: any) {

  if (!this.curriculoCandidatoValido){
    Swal.fire(
      'Alerta!',
      'O seu cadastro de currículo está incompleto, favor revisa-lo!',
      'warning'
    ).then((resultado) => {

    });
    return;
  }
    var foto;
    if(vaga.foto == null){
      foto = this.semImagem;
    } else{
      foto = vaga.foto;
    }
    var areaAtuacao = vaga.nomeAreaAtuacao === null ? 'Não determinado' :  vaga.nomeAreaAtuacao;

    Swal.fire({
      title: vaga.cdVaga +" - "+ vaga.titulo,
      html: '<div class="row" style="align-items: center;justify-content: center;">'+
            '<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
            '</div>'+
            '<div class="row">'+
            ' <label><b>Título da Vaga:</b></label>'+
            ' &nbsp ' + vaga.cdVaga +" - "+ vaga.titulo +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Descrição:</b></label>'+
            ' &nbsp' + vaga.descricao +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Data Limite:</b></label>'+
            ' &nbsp ' + vaga.dataLimite +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Localização:</b></label>'+
            ' &nbsp ' + vaga.localizacao +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Salário:</b></label>'+
            ' &nbsp ' + vaga.salario +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Área de Atuação:</b></label>'+
            ' &nbsp ' + areaAtuacao +
            '</div>'
            ,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonColor: '#d33',
            confirmButtonText:
              '<i class="fa fa-thumbs-up"></i> Candidatar!',
            confirmButtonAriaLabel: 'Ótimo, inscrito com sucesso!',
            cancelButtonText:
              '<i class="fa fa-thumbs-down"></i> Ainda não',
            cancelButtonAriaLabel: 'Thumbs down'

  }).then((result) => {
    if (result.value) {
      var dados = JSON.parse(localStorage.getItem('dados'));
      const inscricaoVaga: InscricaoVaga =  new InscricaoVaga();
      inscricaoVaga.nomeCandidato = dados.displayName;
      inscricaoVaga.codigoVaga = vaga.cdVaga
      this.vagaCandidatoServices.realizarInscricaoVaga(inscricaoVaga).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Sua inscrição foi realizada com sucesso!',
          'success'
        ).then((resultado) => {
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./selecao']);
          });
        })
      });
    }
  })
  }

  protected alertaJaInscrito() {

    Swal.fire(
        'Alerta!',
        'Voce já está inscrito nesta vaga, aguarde o termino do processo!',
        'warning'
      ).then((resultado) => {

      })
  }

  protected setCurrentAction() {

  }
  protected buildResourceForm(): void {

  }
  protected onLoadConfigurations(): void {

  }

  removeCaractereHTML(text){
     return text.replace(/<style[^>]*>.*<\/style>/gm, '')
    // Remove script tags and content
    .replace(/<script[^>]*>.*<\/script>/gm, '')
    // Remove all opening, closing and orphan HTML tags
    .replace(/<[^>]+>/gm, '')
    // Remove leading spaces and repeated CR/LF
    .replace(/([\r\n]+ +)+/gm, '');
  }

  obterString(val) {
    return (val + "").replace(/<[^>]*>?/gm, '');;
  }
  
}
