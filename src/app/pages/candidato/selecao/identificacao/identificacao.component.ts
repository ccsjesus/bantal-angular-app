import { Component, OnInit, Injector } from '@angular/core';
import { CarService } from '../../../../shared/services/carservices';
import { VagaServices } from '../../../../shared/services/vaga.service';
import { Vaga } from '../../../models/vaga.model';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';
import { DadosServices } from '../../../../shared/services/dados.service';


@Component({
  selector: 'app-identificacao',
  templateUrl: './identificacao.component.html',
  styleUrls: ['./identificacao.component.css']
})
export class identificacaoComponent extends BaseResourceFormComponent<Vaga> implements OnInit {

  cars: any[];

  responsiveOptions;

  linkPlanos = '/planos';

  nomePlano: string;

  nomeUsuario: string;

	constructor(private carService: CarService,
   protected injector: Injector,
   protected vagasServices: VagaServices,
  protected dadosServices: DadosServices) {
    super(injector, new Vaga(), null, Vaga.fromJson);
	}

	ngOnInit() {
    this.dadosServices.obterDadosUsuario().subscribe((users) => {
      this.nomeUsuario = users.displayName;
      if(users.planoUsuario) {
        this.nomePlano = users.planoUsuario.nomePlano;
      } else {
        this.nomePlano = 'Plano Inicial';
      }
    });
  }

  protected setCurrentAction() {

  }
  protected buildResourceForm(): void {

  }
  protected onLoadConfigurations(): void {

  }

}
