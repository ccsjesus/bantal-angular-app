import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { SelecaoRoutingModule } from './selecao-routing.module';
import { SelecaoComponent } from './selecao.component';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import { VagaDisponivelComponent } from './vaga-disponivel/vaga-disponivel.component';
import { MinhasVagasComponent } from './minhas-vagas/minhas-vagas.component';
import { identificacaoComponent } from './identificacao/identificacao.component';


@NgModule({  
  imports: [
    SharedModule,
    CommonModule,
    SelecaoRoutingModule,
    RouterModule,
    ReactiveFormsModule,    
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    SelecaoComponent,
    VagaDisponivelComponent,
    MinhasVagasComponent,
    identificacaoComponent
  ],
})
export class SelecaoModule { }
