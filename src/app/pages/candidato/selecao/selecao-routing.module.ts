import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SelecaoComponent } from './selecao.component';

const routes: Routes = [
  { path: '', component: SelecaoComponent, },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class SelecaoRoutingModule { }
