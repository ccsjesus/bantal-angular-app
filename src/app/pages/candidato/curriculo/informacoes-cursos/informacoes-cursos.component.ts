import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { FormacaoAcademicaModel } from '../../../models/formacao.academica.model';
import Swal from 'sweetalert2';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';
@Component({
  selector: 'app-informacoes-cursos',
  templateUrl: './informacoes-cursos.component.html',
  styleUrls: ['./informacoes-cursos.component.css']
})
export class InformacoesCursosComponent implements OnInit {

  @Input('parentForm')
  public parentForm: FormGroup;

  @Output() ngSubmit = new EventEmitter<any>();

  @ViewChild('addTab', {static: true}) addTabEl: NbTabComponent;

  @ViewChild('tabset', {static: true}) tabsetEl: NbTabsetComponent;


  constructor(private fb: FormBuilder) { }

  ngOnInit() {

  }

  adicionar(){
    const control = <FormArray>this.parentForm.controls['cursos'];
    control.push(this.createCursos());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  atualizar(i: number) {
    this.ngSubmit.emit();
  }

  createCursos() {
    return this.fb.group({
      instituicao: this.parentForm.controls.instituicao.value,
      nomeCurso: this.parentForm.controls.nomeCurso.value,
      anoInicio: this.parentForm.controls.anoInicio.value,
      anoFim: this.parentForm.controls.anoFim.value,
      cargaHoraria: this.parentForm.controls.cargaHoraria.value
    });
  }

  remover(i: number) {
    // remove address from the list
    const control = <FormArray>this.parentForm.controls['cursos'];
    let formacao = control.get(i+"").value;

    let valor1 = formacao.instituicao === null ? "" : formacao.instituicao;
    let valor2 = formacao.nomeCurso === null ? "" : formacao.nomeCurso;
    let valor3 = formacao.anoInicio === null ? "" : formacao.anoInicio;
    let valor4 = formacao.anoFim === null ? "" : formacao.anoFim;
    let valor5 = formacao.cargaHoraria === null ? "" : formacao.cargaHoraria;
    //control.removeAt(i);
    Swal.fire({
      title: "Excluir Curso?",
      html:
            '<div class="row">'+
            ' <label><b>Instituição:</b></label>'+
            ' &nbsp ' + valor1 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Nome do Curso:</b></label>'+
            ' &nbsp ' + valor2 +
            '</div>' +
            ' <div class="row">'+
            ' <label><b>Ano Início:</b></label>'+
            ' &nbsp ' + valor3 +
            '</div>' +
            ' <div class="row">'+
            ' <label><b>Ano Fim:</b></label>'+
            ' &nbsp ' + valor4 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Carga horária:</b></label>'+
            ' &nbsp ' + valor5 +
            '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Sim !',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Não! ',
      cancelButtonAriaLabel: 'Thumbs down'
  }).then((result) => {
     if(result.value){
      const control = <FormArray>this.parentForm.controls['cursos'];
      control.removeAt(i);
      this.ngSubmit.emit();
      this.setFirstTab();
     }
  })
  }

  setFirstTab(){
    this.tabsetEl.selectTab(this.tabsetEl.tabs.first)
  }

  limparFormulario(){
    this.parentForm.controls.instituicao.setValue("");
    this.parentForm.controls.nomeCurso.setValue("");
    this.parentForm.controls.anoInicio.setValue("");
    this.parentForm.controls.anoFim.setValue("");
    this.parentForm.controls.cargaHoraria.setValue("");
  }

}
