import { Curriculo } from './../../../models/curriculo.model';
import { ExperienciaProfissionalServices } from './../../../../shared/services/experiencia-profissional.services';
import Swal from 'sweetalert2';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';
import { Component, Input, Output, OnInit, ViewChild, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef, Injector, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';
import { Observable } from 'rxjs';
import { FuncaoModel } from '../../../models/funcao.model';
import { FuncaoCargoServices } from '../../../../shared/services/funcao-cargo.services';
import { Profissao } from '../../../models/profissao.model';


@Component({
  selector: 'app-informacoes-experiencia',
  templateUrl: './informacoes-experiencia.component.html',
  styleUrls: ['./informacoes-experiencia.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class InformacoesExperienciaComponent extends BaseResourceFormComponent<Curriculo> implements OnInit, OnDestroy {

  @Input('parentForm')
  public parentForm: FormGroup;

  @Output() ngSubmit = new EventEmitter<any>();

  @ViewChild('addTab', {static: true}) addTabEl: NbTabComponent;

  @ViewChild('tabset', {static: true}) tabsetEl: NbTabsetComponent;

  exibirSemXp: boolean = false;

  xp: boolean = false;

  xp2: boolean = false;

  experiencia: boolean = false;

  experiencia2: boolean = false;

  descAtribuicao: string = '';

  id: string = '';

  profissoes$: Observable<Profissao[]>;

  profissoes: Profissao[];

  constructor(private fb: FormBuilder,
              private cd: ChangeDetectorRef,
              private xpServices: ExperienciaProfissionalServices,
              protected funcaoCargoServices: FuncaoCargoServices,
              protected injector: Injector) {
                super(injector, new Curriculo(), null, Curriculo.fromJson);
              }

  ngOnDestroy(): void {}

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }

  ngOnInit() {
    var exp = this.parentForm.controls.experienciaProfissional;
    if(exp.value[0]?.experiencia){
      this.id = this.parentForm.controls.experienciaProfissional.value[0].id
      this.exibirSemXp = exp.value[0].experiencia;
      this.descAtribuicao = this.parentForm.controls.experienciaProfissional.value[0].descAtribuicao;
    }
    this.carregarProfissoes();
  }

  private carregarProfissoes() {
    this.profissoes$ = this.funcaoCargoServices.getTodasFuncoesCargo();
    
    this.parentForm.controls.profissao.setValue(-1);

    this.profissoes$.subscribe(valor => {
      this.profissoes = valor;
    });
  }

  atualizarCursoDialog(){

  }

  adicionar(){
    const control = <FormArray>this.parentForm.controls['experienciaProfissional'];

    control.push(this.createFormacaoAcademica());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  adicionar2(){
    const control = <FormArray>this.parentForm.controls['experienciaProfissional'];
    control.push(this.createExperienciaAcademicaSemExp());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  adicionarSemExp(){
    const control = <FormArray>this.parentForm.controls['experienciaProfissional'];
    control.push(this.createFormacaoAcademicaSemExp());
    this.onAdicionarSemExperienciaProfissional();
  }

  onSubmitSemExperienciaProfissional(){
    if (this.parentForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const resource: Curriculo = new Curriculo();
    resource.experienciaProfissional = this.parentForm.controls.experienciaProfissional.value;

    this.xpServices.adicionarSemExperiencia(resource).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        //this.hasDadosExperiencia = resource.experienciaProfissional.length > 0;
        this.exibirAlertaParaInscricaoVaga();
      })
    })
    }
  }

  atualizar(i: number) {
    this.ngSubmit.emit();
  }

  adicionarSemExp2(){
    const control = <FormArray>this.parentForm.controls['experienciaProfissional'];
    control.push(this.createFormacaoAcademicaSemExp2());
    control.removeAt(0);
    this.onAdicionarSemExperienciaProfissional();
  }

  onAdicionarSemExperienciaProfissional(){
    if (this.parentForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const resource: Curriculo = new Curriculo();
    resource.experienciaProfissional = this.parentForm.controls.experienciaProfissional.value;

    this.xpServices.atualizarSemExperiencia(resource).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        //this.hasDadosExperiencia = resource.experienciaProfissional.length > 0;
        this.exibirAlertaParaInscricaoVaga();
      })
    })
    }
  }

  createFormacaoAcademica() {
    return this.fb.group({
      empregador: this.parentForm.controls.empregador.value,
      nomeTrabalho: this.parentForm.controls.nomeTrabalho.value,
      profissao: this.parentForm.controls.profissao.value,
      dataInicio: this.parentForm.controls.dataInicio.value,
      dataTermino: this.parentForm.controls.dataTermino.value,
      notas: this.parentForm.controls.notas.value,
      experiencia: this.xp,
      descAtribuicao: ''
    });
  }

  createFormacaoAcademicaSemExp() {
    return this.fb.group({
      id: this.id,
      empregador: this.parentForm.controls.empregador.value,
      nomeTrabalho: this.parentForm.controls.nomeTrabalho.value,
      profissao: this.parentForm.controls.profissao.value,
      dataInicio: this.parentForm.controls.dataInicio.value,
      dataTermino: this.parentForm.controls.dataTermino.value,
      notas: this.parentForm.controls.notas.value,
      experiencia: this.experiencia,
      descAtribuicao: this.descAtribuicao
    });
  }

  createFormacaoAcademicaSemExp2() {
    return this.fb.group({
      id: this.id,
      empregador: this.parentForm.controls.empregador.value,
      nomeTrabalho: this.parentForm.controls.nomeTrabalho.value,
      profissao: this.parentForm.controls.profissao.value,
      dataInicio: this.parentForm.controls.dataInicio.value,
      dataTermino: this.parentForm.controls.dataTermino.value,
      notas: this.parentForm.controls.notas.value,
      experiencia: !this.experiencia2,
      descAtribuicao: this.descAtribuicao
    });
  }

  createExperienciaAcademicaSemExp() {
    return this.fb.group({
      empregador: this.parentForm.controls.empregador.value,
      nomeTrabalho: this.parentForm.controls.nomeTrabalho.value,
      profissao: this.parentForm.controls.profissao.value,
      dataInicio: this.parentForm.controls.dataInicio.value,
      dataTermino: this.parentForm.controls.dataTermino.value,
      notas: this.parentForm.controls.notas.value,
      experiencia: !this.experiencia2,
      descAtribuicao: ''
    });
  }

  remover(i: number) {
    // remove address from the list
    const control = <FormArray>this.parentForm.controls['experienciaProfissional'];
    let formacao = control.get(i+"").value;

    let valor1 = formacao.empregador === null ? "" : formacao.empregador;
    let valor2 = formacao.nomeTrabalho === null ? "" : formacao.nomeTrabalho;
    let valor3 = formacao.dataInicio === null ? "" : formacao.dataInicio;
    let valor4 = formacao.notas === null ? "" : formacao.notas;
    //control.removeAt(i);
    Swal.fire({
      title: "Excluir Formação?",
      html:
            '<div class="row">'+
            ' <label><b>Formação:</b></label>'+
            ' &nbsp ' + valor1 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Qualificação:</b></label>'+
            ' &nbsp ' + valor2 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Data de Início:</b></label>'+
            ' &nbsp ' + valor3 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Notas:</b></label>'+
            ' &nbsp ' + valor4 +
            '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Sim !',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Não! ',
      cancelButtonAriaLabel: 'Thumbs down'
  }).then((result) => {
     if(result.value){
      const control = <FormArray>this.parentForm.controls['experienciaProfissional'];
      control.removeAt(i);
      this.ngSubmit.emit();
      this.setFirstTab();
     }
  })
  }

  setFirstTab(){
    this.tabsetEl.selectTab(this.tabsetEl.tabs.first)
  }

  limparFormulario(){
    this.parentForm.controls.empregador.setValue("");
    this.parentForm.controls.nomeTrabalho.setValue("");
    this.parentForm.controls.dataInicio.setValue("");
    this.parentForm.controls.dataTermino.setValue("");
    this.parentForm.controls.notas.setValue("");
    this.parentForm.controls.experiencia.value.setValue("");
    this.parentForm.controls.descAtribuicao.setValue("");
  }

  handleChanges(event){
    this.xp = this.experiencia;
  }

  handleChangesSemXP(event){
    this.experiencia2 = !event.checked;
    this.xp2 = !event.checked;
  }

  exibirAlertaParaInscricaoVaga(){
     this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
          this.router.navigate(['./curriculo']);
     });
  }

}
