import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { FormacaoAcademicaModel } from '../../../models/formacao.academica.model';
import Swal from 'sweetalert2';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';

@Component({
  selector: 'app-informacoes-formacao',
  templateUrl: './informacoes-formacao.component.html',
  styleUrls: ['./informacoes-formacao.component.scss']
})
export class InformacoesFormacaoComponent implements OnInit {

  @Input('parentForm')
  public parentForm: FormGroup;

  @Output() ngSubmit = new EventEmitter<any>();

  @ViewChild('addTab', {static: true}) addTabEl: NbTabComponent;

  @ViewChild('tabset', {static: true}) tabsetEl: NbTabsetComponent;


  constructor(private fb: FormBuilder,) { }

  ngOnInit() {
  }

  setUniversidade(valor, indice){
    return valor;

  }

  atualizarCursoDialog(){

  }

  adicionarFormacao(){
    const control = <FormArray>this.parentForm.controls['formacaoAcademica'];
    control.push(this.createFormacaoAcademica());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  atualizarFormacaoAcademica(i: number) {
    this.ngSubmit.emit();
  }

  createFormacaoAcademica() {
    return this.fb.group({
      universidade: this.parentForm.controls.universidade.value,
      qualificacao: this.parentForm.controls.qualificacao.value,
      dataInicio: this.parentForm.controls.dataInicio.value,
      dataFim: this.parentForm.controls.dataFim.value,
      nota: this.parentForm.controls.nota.value
    });
  }

  removeFormacaoAcademica(i: number) {
    // remove address from the list
    const control = <FormArray>this.parentForm.controls['formacaoAcademica'];
    let formacao = control.get(i+"").value;

    let universidade = formacao.universidade === null ? "" : formacao.universidade;
    let qualificacao = formacao.qualificacao === null ? "" : formacao.qualificacao;
    let dataInicio = formacao.dataInicio === null ? "" : formacao.dataInicio;
    let nota = formacao.nota === null ? "" : formacao.nota;
    //control.removeAt(i);
    Swal.fire({
      title: "Excluir Qualificação?",
      html:
            '<div class="row">'+
            ' <label><b>Instituição:</b></label>'+
            ' &nbsp ' + universidade +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Qualificação:</b></label>'+
            ' &nbsp ' + qualificacao +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Data de Início:</b></label>'+
            ' &nbsp ' + dataInicio +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Notas:</b></label>'+
            ' &nbsp ' + nota +
            '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Sim !',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Não! ',
      cancelButtonAriaLabel: 'Thumbs down'
  }).then((result) => {
    if(result.value){
      const control = <FormArray>this.parentForm.controls['formacaoAcademica'];
      control.removeAt(i);
      this.ngSubmit.emit();
      this.setFirstTab();
    }

  })
  }

  setFirstTab(){
    this.tabsetEl.selectTab(this.tabsetEl.tabs.first)
  }

  limparFormulario(){
    this.parentForm.controls.universidade.setValue("");
    this.parentForm.controls.qualificacao.setValue("");
    this.parentForm.controls.dataInicio.setValue("");
    this.parentForm.controls.dataFim.setValue("");
    this.parentForm.controls.nota.setValue("");
  }

}
