import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CurriculoRoutingModule } from './curriculo-routing.module';
import { DadosPessoaisComponent } from './dados-pessoais/dados-pessoais.component';
import { InformacoesCargoComponent } from './informacoes-cargo/informacoes-cargo.component';
import { InformacoesFormacaoComponent } from './informacoes-formacao/informacoes-formacao.component';
import { InformacoesExperienciaComponent } from './informacoes-experiencia/informacoes-experiencia.component';
import { CurriculoComponent } from './curriculo.component';
import { CommonModule } from '@angular/common';
import { InformacoesCursosComponent } from './informacoes-cursos/informacoes-cursos.component';
import { InformacoesAdicionaisComponent } from './informacoes-adicionais/informacoes-adicionais.component';
import { InformacoesInformaticaComponent } from './informacoes-informatica/informacoes-informatica.component';


@NgModule({  
  imports: [
    SharedModule,
    CommonModule,
    CurriculoRoutingModule,
    RouterModule,
    ReactiveFormsModule    
  ],
  declarations: [
    CurriculoComponent,
    DadosPessoaisComponent,
    InformacoesCargoComponent,
    InformacoesFormacaoComponent,
    InformacoesExperienciaComponent,
    InformacoesCursosComponent,
    InformacoesAdicionaisComponent,
    InformacoesInformaticaComponent
  ],
})
export class CurriculoModule { }
