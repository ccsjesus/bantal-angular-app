import { ParametroModel } from './../../../models/parametro.model';
import { CNHServices } from './../../../../shared/services/cnh.services';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Curriculo } from '../../../models/curriculo.model';
import { FormGroup, AbstractControl } from '@angular/forms';
import { options_estado_civil } from '../../../constantes'
import { options_formacao } from '../../../constantes/cons_formacao';
import Swal from 'sweetalert2';
import { CNH } from '../../../models/cnh.model';
import { of as observableOf,  Observable } from 'rxjs';

@Component({
  selector: 'app-dados-pessoais',
  templateUrl: './dados-pessoais.component.html',
  styleUrls: ['./dados-pessoais.component.scss']
})
export class DadosPessoaisComponent implements OnInit {

  curriculo: Curriculo;
  options = options_estado_civil;
  //listaCnh = cnh;
  submitted = false;
  optionsFormacao = options_formacao;

  @Input('parentForm')
  public parentForm: FormGroup;

  @Output() ngSubmit = new EventEmitter<any>();

  cnhOptions$: Observable<ParametroModel[]>;
  cnhList: ParametroModel[];

  constructor(
    protected cnhServices: CNHServices
  ) { }

  ngOnInit(): void {
  }

  get estadoCivil(): AbstractControl {
    return this.parentForm.get('estadoCivil');
  }

  get listaCnh(): [CNH] {
    return this.parentForm.get('cnh').value;
  }

  get formacao(): AbstractControl {
    return this.parentForm.get('formacao');
  }

  abrirConfirmacaoDialog(){
    this.submitted = true;

    this.ngSubmit.emit();
  }

}
