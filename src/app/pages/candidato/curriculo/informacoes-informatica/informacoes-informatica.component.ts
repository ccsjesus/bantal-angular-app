import { Curriculo } from './../../../models/curriculo.model';
import { TecnologiaServices } from './../../../../shared/services/tecnologia.services';
import { informatica_opcoes } from './../../../constantes/option_informatica';
import { options_nivel_idioma } from './../../../constantes/option_nivel_idioma';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy, Injector } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';
import { of as observableOf,  Observable } from 'rxjs';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';


@Component({
  selector: 'app-informacoes-informatica',
  templateUrl: './informacoes-informatica.component.html',
  styleUrls: ['./informacoes-informatica.component.scss']
})
export class InformacoesInformaticaComponent extends BaseResourceFormComponent<Curriculo> implements OnInit, OnDestroy {

  @Input('parentForm')
  public parentForm: FormGroup;

  options = options_nivel_idioma;

  options_informatica = informatica_opcoes;

  @Output() ngSubmit = new EventEmitter<any>();

  @ViewChild('addTab', {static: true}) addTabEl: NbTabComponent;

  @ViewChild('tabset', {static: true}) tabsetEl: NbTabsetComponent;

  exibirSemXp: boolean = false;

  xp: boolean = false;

  xp2: boolean = false;

  outros: boolean = false;

  experiencia: boolean = false;

  experiencia2: boolean = false;

  descAtribuicao: string = '';

  descOutros: string = '';

  id: string = '';


  constructor(private fb: FormBuilder,
              private xpServices: TecnologiaServices,
              protected injector: Injector) {
      super(injector, new Curriculo(), null, Curriculo.fromJson);
  }

  ngOnDestroy(): void {}

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }
  ngOnInit() {
      this.parentForm.controls.tecnologia.setValue("-1");
      console.log(this.parentForm.controls.tecnologia);
      var exp = this.parentForm.controls.informatica;
      if(exp.value[0]?.experiencia){
        this.id = this.parentForm.controls.informatica.value[0].id
        this.exibirSemXp = exp.value[0].experiencia;
        this.descAtribuicao = this.parentForm.controls.informatica.value[0].descAtribuicao;
      }
      
  }

  adicionar(){
    const control = <FormArray>this.parentForm.controls['informatica'];
    control.push(this.createInformatica());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  adicionar2(){
    const control = <FormArray>this.parentForm.controls['informatica'];
    control.push(this.createInformaticaSemExp());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  adicionarSemExp(){
    const control = <FormArray>this.parentForm.controls['informatica'];
    control.push(this.createInformaticaSemExp());
    this.onAdicionarSemExperienciaInformatica();
  }

  atualizar(i: number) {
    this.ngSubmit.emit();
  }

  adicionarSemExp2(){
    const control = <FormArray>this.parentForm.controls['informatica'];
    control.push(this.createInformaticaSemExp2());
    control.removeAt(0);
    this.onAdicionarSemExperienciaInformatica();
  }

  onAdicionarSemExperienciaInformatica(){
    if (this.parentForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const resource: Curriculo = new Curriculo();
    resource.informatica = this.parentForm.controls.informatica.value;

    this.xpServices.atualizarSemExperiencia(resource).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        //this.hasDadosExperiencia = resource.experienciaProfissional.length > 0;
        this.exibirAlertaParaInscricaoVaga();
      })
    })
    }
  }

  createInformatica() {
    var tecnologia: string = this.getDescTecnologia();

    return this.fb.group({
      tecnologia: tecnologia,
      nivel: this.parentForm.controls.nivel.value,
      experiencia: this.xp,
      descAtribuicao: ''
    });
  }

  createInformaticaSemExp() {
    var tecnologia: string = this.getDescTecnologia();

    return this.fb.group({
      tecnologia: tecnologia,
      nivel: this.parentForm.controls.nivel.value,
      experiencia: this.experiencia,
      descAtribuicao: this.descAtribuicao
    });
  }

  createInformaticaSemExp2() {
    var tecnologia: string = this.getDescTecnologia();

    return this.fb.group({
      tecnologia: tecnologia,
      nivel: this.parentForm.controls.nivel.value,
      experiencia: !this.experiencia2,
      descAtribuicao: this.descAtribuicao
    });
  }

  createExperienciaInformaticaSemExp() {
    var tecnologia: string = this.getDescTecnologia();

    return this.fb.group({
      tecnologia: tecnologia,
      nivel: this.parentForm.controls.nivel.value,
      experiencia: !this.experiencia2,
      descAtribuicao: ''
    });
  }

  remover(i: number) {
    // remove address from the list
    const control = <FormArray>this.parentForm.controls['informatica'];
    let formacao = control.get(i+"").value;

    let valor1 = formacao.tecnologia === null ? "" : formacao.tecnologia;
    let valor2 = formacao.nivel === null ? "" : formacao.nivel;
    //control.removeAt(i);
    Swal.fire({
      title: "Excluir Tecnologia?",
      html:
            '<div class="row">'+
            ' <label><b>Tecnologia:</b></label>'+
            ' &nbsp ' + valor1 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Nível:</b></label>'+
            ' &nbsp ' + valor2 +
            '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Sim !',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Não! ',
      cancelButtonAriaLabel: 'Thumbs down'
  }).then((result) => {
     if(result.value){
      const control = <FormArray>this.parentForm.controls['informatica'];
      control.removeAt(i);
      this.ngSubmit.emit();
      this.setFirstTab();
     }
  })
  }

  setFirstTab(){
    this.tabsetEl.selectTab(this.tabsetEl.tabs.first)
  }

  limparFormulario(){
    this.parentForm.controls.tecnologia.setValue("-1");
    this.parentForm.controls.nivel.setValue("");
    this.parentForm.controls.experiencia.value.setValue("");
    this.parentForm.controls.descAtribuicao.setValue("");
    this.parentForm.controls.descOutros.setValue("");
  }

  handleChanges(event: any){
    this.xp = this.experiencia;
  }

  handleChangeSelect(value: string){
    if(value === 'Outros'){
      this.outros = !this.outros;
    } else {
      this.outros = false
    }    
  }

  handleChangesSemXP(event: any){
    this.experiencia2 = !event.checked;
    this.xp2 = !event.checked;
  }

  exibirAlertaParaInscricaoVaga(){
     this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
          this.router.navigate(['./curriculo']);
     });
  }

  getDescTecnologia(){
    var tecnologia: string;

    if(this.outros){
      tecnologia = this.parentForm.controls.descOutros.value
    } else {
      tecnologia = this.parentForm.controls.tecnologia.value
    }

    return tecnologia
  }

  isOpcaoOutros(indice: string): boolean{    
    const controlInformatica = <FormArray>this.parentForm.controls['informatica'];    
    let informaticaModel = controlInformatica.value[indice];

    for(var i = 0; i < this.options_informatica.length; i++ ){
      var opcaoInformatica = this.options_informatica[i]
      if(opcaoInformatica.value == informaticaModel.tecnologia){        
        return true;
      }
    }
    return false;
  }

}
