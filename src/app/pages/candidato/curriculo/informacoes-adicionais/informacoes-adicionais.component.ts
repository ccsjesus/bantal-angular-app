import { options_idioma } from './../../../constantes/option_idioma';
import { options_nivel_idioma } from './../../../constantes/option_nivel_idioma';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';
import { of as observableOf,  Observable } from 'rxjs';

@Component({
  selector: 'app-informacoes-adicionais',
  templateUrl: './informacoes-adicionais.component.html',
  styleUrls: ['./informacoes-adicionais.component.scss']
})
export class InformacoesAdicionaisComponent implements OnInit {

  @Input('parentForm')
  public parentForm: FormGroup;

  options = options_nivel_idioma;

  options_idiomas = options_idioma;

  @Output() ngSubmit = new EventEmitter<any>();

  @ViewChild('addTab', {static: true}) addTabEl: NbTabComponent;

  @ViewChild('tabset', {static: true}) tabsetEl: NbTabsetComponent;

    statuses: any[] = ['Leitura', 'Escrita', 'Conversacao'];




  constructor(private fb: FormBuilder) { }

  ngOnInit() {
      this.parentForm.controls.idioma.setValue("-1");
  }

  adicionar(){
    const control = <FormArray>this.parentForm.controls['idiomas'];
    control.push(this.createIdioma());
    this.ngSubmit.emit();
    this.setFirstTab();
    this.limparFormulario();
  }

  atualizar(i: number) {
    this.ngSubmit.emit();
  }

  createIdioma() {
    return this.fb.group({
      idioma: this.parentForm.controls.idioma.value,
      leitura: this.parentForm.controls.leitura.value,
      escrita: this.parentForm.controls.escrita.value,
      conversacao: this.parentForm.controls.conversacao.value,
    });
  }

  remover(i: number) {
    // remove address from the list
    const control = <FormArray>this.parentForm.controls['idiomas'];
    let formacao = control.get(i+"").value;

    let valor1 = formacao.idioma === null ? "" : formacao.idioma;
    let valor2 = formacao.leitura === null ? "" : formacao.leitura;
    let valor3 = formacao.escrita === null ? "" : formacao.escrita;
    let valor4 = formacao.conversacao === null ? "" : formacao.conversacao;
    //control.removeAt(i);
    Swal.fire({
      title: "Excluir Idioma?",
      html:
            '<div class="row">'+
            ' <label><b>Idioma:</b></label>'+
            ' &nbsp ' + valor1 +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Leitura:</b></label>'+
            ' &nbsp ' + valor2 +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Escrita:</b></label>'+
            ' &nbsp ' + valor3 +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Conversação:</b></label>'+
            ' &nbsp ' + valor4 +
            '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> Sim !',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Não! ',
      cancelButtonAriaLabel: 'Thumbs down'
  }).then((result) => {
     if(result.value){
      const control = <FormArray>this.parentForm.controls['idiomas'];
      control.removeAt(i);
      this.ngSubmit.emit();
      this.setFirstTab();
     }
  })
  }

  setFirstTab(){
    this.tabsetEl.selectTab(this.tabsetEl.tabs.first)
  }

  limparFormulario(){
    this.parentForm.controls.idioma.setValue("-1");
    this.parentForm.controls.leitura.setValue("");
    this.parentForm.controls.escrita.setValue("");
    this.parentForm.controls.conversacao.setValue("");
  }

}
