import { IdiomaModel } from './../../models/idioma.model';
import { RelatorioServices } from './../../../shared/services/relatorio.services';
import { Router } from '@angular/router';
import { AreaAtuacaoModel } from './../../models/area.atuacao.model';
import { Component, ViewChild, ChangeDetectionStrategy, OnInit, OnDestroy, Injector, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { PDFSource, PDFProgressData, PDFDocumentProxy } from 'pdfjs-dist';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { Curriculo } from '../../models/curriculo.model';
import { CurriculoServices } from '../../../shared/services/curriculo.services';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
import { MessageService } from '../../../shared/services/message';
import {user64} from '../../../pages/constantes/user';



declare var jQuery: any;

@Component({
  selector: 'app-curriculo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './curriculo.component.html',
  styleUrls: ['./curriculo.component.scss']
})
export class CurriculoComponent extends BaseResourceFormComponent<Curriculo> implements OnInit, OnDestroy {


  pdfSrc: string | PDFSource | ArrayBuffer = '../../../../assets/images/jack.png';

  error: any;
  page = 1;
  rotation = 0;
  zoom = 1.0;
  originalSize = true;
  pdf: any;
  renderText = true;
  progressData: PDFProgressData;
  isLoaded = false;
  stickToPage = false;
  showAll = true;
  autoresize = true;
  fitToPage = false;
  outline: any[];
  isOutlineShown = false;
  pdfQuery = '';

  @ViewChild(PdfViewerComponent, { static: true }) private pdfComponent: PdfViewerComponent;

  v:any = "";
  dadosCurriculo: Curriculo = new Curriculo();
  formDadosPessoais: FormGroup;
  hasDadosPessoais:boolean = false;
  formDadosFormacao: FormGroup;
  hasDadosFormacao:boolean = false;
  formDadosExperiencia: FormGroup;
  hasDadosExperiencia:boolean = false;
  formDadosCargo: FormGroup;
  hasDadosCargo:boolean = false;
  formDadosCursos: FormGroup;
  hasDadosCurso:boolean = false;
  formDadosIdiomas: FormGroup;
  hasDadosIdiomas:boolean = false;
  formDadosInformatica: FormGroup;
  hasDadosInformatica:boolean = false;
  ready: Boolean = false;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;

  @ViewChildren('nbAccordionInformatica') nbAccordionInformatica:QueryList<any>;
  @ViewChild("nbAccordionInformatica") nbAccordionInformatica2: ElementRef<HTMLElement>;




  constructor(protected router: Router,
              protected injector: Injector,
              private fb: FormBuilder,
              protected curriculoService: CurriculoServices,
              private messageService: MessageService,
              private relatorioServices: RelatorioServices) {
    super(injector, new Curriculo(), null, Curriculo.fromJson);
   }

  ngOnDestroy(): void {}

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }

  ngOnInit(): void {

    this.formDadosPessoais = this.fb.group({
      nome: ['', Validators.required],
      nacionalidade: ['', Validators.required],
      estadoCivil: ['', Validators.required],
      filhos: ['', Validators.required],
      idade: ['', Validators.required],
      telefone: ['', Validators.required],
      celular: ['', Validators.required],
      telefoneRecados: [''],
      email: ['', Validators.required],
      linkedin: ['', ],
      cnh: ['', ],
      identificacao: ['', Validators.required],
      rg: ['', Validators.required],
      endereco: ['', Validators.required],
      formacao: ['', ],
      apresentacao: ['', ],
      objetivoProfissional: ['', ],
      foto: ['', ]
    });

    this.formDadosFormacao = this.fb.group({
      formacaoAcademica: this.fb.array([]),
      universidade: [''],
      qualificacao: [''],
      dataInicio: [''],
      dataFim: [''],
      nota: [''],
    });

    this.formDadosExperiencia = this.fb.group({
      experienciaProfissional: this.fb.array([]),
      empregador: [''],
      nomeTrabalho: [''],
      profissao: [''],
      dataInicio: [''],
      dataTermino: [''],
      notas: [''],
      experiencia: [''],
      descAtribuicao: [''],
    });

    this.formDadosCargo = this.fb.group({
      areaAtuacao: [''],
      nivel: [''],
      funcaoCargo: [''],
      formacaoAcademica: [''],
      pretensaoSalarial: [''],
    });

    this.formDadosCursos = this.fb.group({
      cursos: this.fb.array([]),
      instituicao: [''],
      nomeCurso: [''],
      anoInicio: [''],
      anoFim: [''],
      cargaHoraria: [''],
    });

    this.formDadosIdiomas = this.fb.group({
      idiomas: this.fb.array([]),
      idioma: [''],
      leitura: [''],
      escrita: [''],
      conversacao: [''],
    });

    this.formDadosInformatica = this.fb.group({
      informatica: this.fb.array([]),
      tecnologia: [''],
      nivel: [''],
      experiencia: [''],
      descAtribuicao: [''],
      descOutros: ['']
    });

     this.curriculoService.getCurriculoCandidato().subscribe(valor => {
      this.dadosCurriculo = valor;
      this.loadDadosPessoais(valor);
      this.loadDadosFormacao(valor);
      this.loadDadosExperiencia(valor);
      this.loadDadosCargo(valor);
      this.loadDadosCursos(valor);
      this.loadDadosIdiomas(valor);
      this.loadDadosInformatica(valor);
      if(valor !== null ){
        this.ready = true;
      }

    },
      error => this.messageService.add({severity:'info', summary:'Usuario Selecionado', detail:'Vin: ' + error}));
  }

  isDadosBasicoCompleto(valor): boolean {
    if (valor == null)
      return false;

    if ((valor.nome != null && valor.nome != "") && (valor.nacionalidade != null && valor.nacionalidade != "") && (valor.estadoCivil != null && valor.estadoCivil != "") &&
      (valor.filhos != null && valor.filhos != "") && (valor.idade != null && valor.idade != "") && (valor.endereco != null && valor.endereco != "") &&
      (valor.telefone != null && valor.telefone != "") && (valor.celular != null && valor.celular != "") && (valor.email != null && valor.email != "") &&
      (valor.identificacao != null && valor.identificacao != "") && (valor.rg != null && valor.rg != "")) 
        return true;
      
      return false;
  }

  loadDadosPessoais(valor){
    this.hasDadosPessoais = this.isDadosBasicoCompleto(valor);
    this.formDadosPessoais.controls.nome.setValue(valor.nome);
    this.formDadosPessoais.controls.nacionalidade.setValue(valor.nacionalidade);
    this.formDadosPessoais.controls.estadoCivil.setValue(valor.estadoCivil);
    this.formDadosPessoais.controls.email.setValue(valor.email);
    this.formDadosPessoais.controls.filhos.setValue(valor.filhos);
    this.formDadosPessoais.controls.identificacao.setValue(valor.identificacao);
    this.formDadosPessoais.controls.rg.setValue(valor.rg);
    this.formDadosPessoais.controls.telefone.setValue(valor.telefone);
    this.formDadosPessoais.controls.celular.setValue(valor.celular);
    this.formDadosPessoais.controls.telefoneRecados.setValue(valor.telefoneRecados);
    this.formDadosPessoais.controls.cnh.setValue(valor.cnh);
    this.formDadosPessoais.controls.idade.setValue(valor.idade);
    this.formDadosPessoais.controls.endereco.setValue(valor.endereco);
    this.formDadosPessoais.controls.linkedin.setValue(valor.linkedin);
    this.formDadosPessoais.controls.formacao.setValue(valor.formacao);
    this.formDadosPessoais.controls.apresentacao.setValue(valor.apresentacao);
    this.formDadosPessoais.controls.objetivoProfissional.setValue(valor.objetivoProfissional);

    if(valor.foto === null){
      this.formDadosPessoais.controls.foto.setValue(user64);
    } else {
      this.retrieveResonse = valor.foto;
      this.base64Data = this.retrieveResonse;
      this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      this.formDadosPessoais.controls.foto.setValue(this.retrievedImage);
    }
  }

  loadDadosFormacao(valor){
    if(valor.formacaoAcademica){
      const mapped = Object.keys(valor.formacaoAcademica).map(key => ({type: key, value: valor.formacaoAcademica[key]}));
      const control = <FormArray>this.formDadosFormacao.controls['formacaoAcademica'];
      mapped.forEach(formacao => control.push(this.fb.group(formacao.value)));
      this.hasDadosFormacao = control.length > 0;
    }
  }

  loadDadosExperiencia(valor){
    if(valor.experienciaProfissional){
      const mapped = Object.keys(valor.experienciaProfissional).map(key => ({type: key, value: valor.experienciaProfissional[key]}));
      const control = <FormArray>this.formDadosExperiencia.controls['experienciaProfissional'];
      mapped.forEach(experiencia => control.push(this.fb.group(experiencia.value)));
      this.hasDadosExperiencia = control.length > 0;
    }
  }

  loadDadosCargo(valor){
    this.formDadosCargo.controls.pretensaoSalarial.setValue(valor.pretensaoSalarial);
    this.formDadosCargo.controls.nivel.setValue(valor.nivel);
    this.formDadosCargo.controls.funcaoCargo.setValue(valor.funcaoCargo);
    if(valor.areaAtuacao){
      const mapped = Object.keys(valor.areaAtuacao).map(key => (valor.areaAtuacao[key].termId));
      const control = <FormControl>this.formDadosCargo.controls['areaAtuacao'];
      control.setValue(mapped);
       this.hasDadosCargo = this.createCurriculoCargo(valor);
    }

  }

  loadDadosCursos(valor){
    this.hasDadosCurso = !valor.cursos;
    if(valor.cursos){
      const mapped = Object.keys(valor.cursos).map(key => ({type: key, value: valor.cursos[key]}));
      const control = <FormArray>this.formDadosCursos.controls['cursos'];
      mapped.forEach(cursos => control.push(this.fb.group(cursos.value)));
    }
  }

  loadDadosIdiomas(valor){
    if(valor.idiomas){
      const mapped = Object.keys(valor.idiomas).map(key => ({type: key, value: valor.idiomas[key]}));
      const control = <FormArray>this.formDadosIdiomas.controls['idiomas'];
      mapped.forEach(idioma => control.push(this.fb.group(idioma.value)));
      this.hasDadosIdiomas = control.length > 0;
    }
  }

  loadDadosInformatica(valor){
    if(valor.informatica){
      const mapped = Object.keys(valor.informatica).map(key => ({type: key, value: valor.informatica[key]}));
      const control = <FormArray>this.formDadosInformatica.controls['informatica'];
      mapped.forEach(info => control.push(this.fb.group(info.value)));
      this.hasDadosInformatica = control.length > 0;
    }
  }

  onDadosPessoaisSubmit() {

    if (this.formDadosPessoais.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

      const resource: Curriculo = this.jsonDataToResourceFn(this.formDadosPessoais.value);
      resource.foto = null;

      this.curriculoService.atualizarDadosPessoais(resource).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Seus dados foram publicados com sucesso!',
          'success'
        ).then((resultado) => {
          this.hasDadosPessoais = this.isDadosBasicoCompleto(resource);
          this.formDadosPessoais.controls.universidade.setValue("");
          this.formDadosPessoais.controls.qualificacao.setValue("");
          this.formDadosPessoais.controls.dataInicio.setValue("");
          this.formDadosPessoais.controls.dataFim.setValue("");
          this.formDadosPessoais.controls.nota.setValue("");

          this.exibirAlertaParaInscricaoVaga();

        })

      })
    }
  }


  onSubmitFormacao() {

    if (this.formDadosFormacao.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const resource: Curriculo = new Curriculo();
    resource.formacaoAcademica = this.formDadosFormacao.controls.formacaoAcademica.value;

    this.curriculoService.atualizarFormacaoCandidato(resource).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        this.hasDadosFormacao = resource.formacaoAcademica.length > 0;
        this.exibirAlertaParaInscricaoVaga();
      })

    })

    }
  }

  onSubmitExperienciaProfissional(){
    if (this.formDadosExperiencia.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const resource: Curriculo = new Curriculo();
    resource.experienciaProfissional = this.formDadosExperiencia.controls.experienciaProfissional.value;

    this.curriculoService.atualizarExperienciaProfissionalCandidato(resource).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        this.hasDadosExperiencia = resource.experienciaProfissional.length > 0;
        this.exibirAlertaParaInscricaoVaga();
      })
    })
    }
  }

  onSubmitCargoPreterido(){
    if (this.formDadosCargo.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const curriculo: Curriculo = new Curriculo()
    let area = this.formDadosCargo.controls.areaAtuacao.value;

    curriculo.areaAtuacao = area.map(k => (new AreaAtuacaoModel(k)));
    curriculo.nivel = this.formDadosCargo.controls.nivel.value;
    curriculo.funcaoCargo = this.formDadosCargo.controls.funcaoCargo.value;
    curriculo.pretensaoSalarial = this.formDadosCargo.controls.pretensaoSalarial.value;
    this.curriculoService.atualizarCargoPreteridoCandidato(curriculo).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        this.hasDadosCargo = this.verificarCargoPreterido(curriculo);
        this.exibirAlertaParaInscricaoVaga();
      })
    })
    }
  }

  onSubmitInformacoes(){
    if (this.formDadosIdiomas.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const curriculo: Curriculo = new Curriculo()
      curriculo.idiomas = this.formDadosIdiomas.controls.idiomas.value;

      this.curriculoService.atualizarIdiomasCandidato(curriculo).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Seus dados foram publicados com sucesso!',
          'success'
        ).then((resultado) => {
          this.hasDadosIdiomas = curriculo.idiomas.length > 0;
          this.exibirAlertaParaInscricaoVaga();
        })
      })

    }
  }

  onSubmitInformatica($event){
    if (this.formDadosInformatica.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {

    const curriculo: Curriculo = new Curriculo()
      curriculo.informatica = this.formDadosInformatica.controls.informatica.value;

      this.curriculoService.atualizarInformaticaCandidato(curriculo).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Seus dados foram publicados com sucesso!',
          'success'
        ).then((resultado) => {
          this.hasDadosInformatica = curriculo.informatica.length > 0;
          this.exibirAlertaParaInscricaoVaga();
        })
      })

    }
  }

  atualizarPanelPrincipal(){

    const curriculo: Curriculo = new Curriculo();
    curriculo.apresentacao = this.formDadosPessoais.controls.apresentacao.value;
    curriculo.objetivoProfissional = this.formDadosPessoais.controls.objetivoProfissional.value;

    this.curriculoService.atualizarInformacoesPrincipais(curriculo).subscribe(res => {
      Swal.fire(
        'Ótimo!',
        'Seus dados foram publicados com sucesso!',
        'success'
      ).then((resultado) => {
        this.exibirAlertaParaInscricaoVaga();
      })
    })
  }


  createForm(Input) {
    if(Input){
      const arr = Input.map(valor => {
        return new FormControl(valor);
      });
      return new FormArray(arr);
    }
    return [];
  }

  gerarMeuCurriculo(): boolean {    
    var varIdPessoa = []
    
      this.relatorioServices.gerarRelatorio(varIdPessoa).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Seu relatório foi gerado com sucesso click para continuar!',
          'success'

        ).then((result) => {
          var file = new Blob([res], {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL, '_blank');
        })
      });
    
    return true;
  }

  exibirAlertaParaInscricaoVaga(){
    if(this.hasDadosCargo &&
       //this.hasDadosCurso &&
       this.hasDadosExperiencia &&
       this.hasDadosFormacao &&
       this.hasDadosIdiomas &&
       this.hasDadosInformatica &&
       this.hasDadosPessoais){

      Swal.fire(
        'Ótimo!',
        'Agora você pode se inscrever nas vagas!',
        'success'
      ).then((resultado) => {
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./selecao']);
          });
      })

    } else {
     this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
          this.router.navigate(['./curriculo']);
     });
    }
  }

  verificarCargoPreterido(c: Curriculo) {

    if (c.areaAtuacao &&
        //c.nivel &&
        //c.funcaoCargo &&
        c.pretensaoSalarial){
      return true;
    } else {
      return false;
    }
  }

  createCurriculoCargo(c:any){
    const curriculo: Curriculo = new Curriculo();
    const mapped = Object.keys(c.areaAtuacao).map(key => (c.areaAtuacao[key].termId));
    curriculo.areaAtuacao = mapped
    curriculo.nivel = c.nivel;
    curriculo.funcaoCargo = c.funcaoCargo;
    curriculo.pretensaoSalarial = c.pretensaoSalarial;
    return this.verificarCargoPreterido(curriculo);
  }

}
