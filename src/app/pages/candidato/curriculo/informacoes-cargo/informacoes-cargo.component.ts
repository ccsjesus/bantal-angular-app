import { FuncaoCargoServices } from './../../../../shared/services/funcao-cargo.services';
import { ParametroModel } from './../../../models/parametro.model';
import { NivelCargoServices } from './../../../../shared/services/nivel-cargo.services';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { FuncaoModel } from '../../../models/funcao.model';
import { CurriculoServices } from '../../../../shared/services/curriculo.services';
import { AreaAtuacaoServices } from '../../../../shared/services/area.atuacao.services';

import { AreaAtuacaoModel } from '../../../models/area.atuacao.model';
import { HabilidadeServices } from '../../../../shared/services/habilidades.services';
import { SalarioServices } from '../../../../shared/services/salario.services';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'app-informacoes-cargo',
  templateUrl: './informacoes-cargo.component.html',
  styleUrls: ['./informacoes-cargo.component.scss']
})
export class InformacoesCargoComponent implements OnInit {

  @Input('parentForm')
  public parentForm: FormGroup;

  @Output() ngSubmit = new EventEmitter<any>();

  //tipoNivelServices$: Observable<ParametroModel[]>;

  //tipoFuncaoServices$: Observable<FuncaoModel[]>;

  areas: SelectItem[] = [];

  habilidades: SelectItem[] = [];

  optionsSalario = [];


  constructor(protected areaAtuacaoServices: AreaAtuacaoServices,
              protected habilidadeServices: HabilidadeServices,
              protected salarioServices: SalarioServices
              //protected nivelCargoService: NivelCargoServices,
              //protected funcaoCargoServices: FuncaoCargoServices
              ) { }


  ngOnInit() {
    //this.loadNivel();
    //this.loadFuncao();
    this.loadAreaAtuacao();
    this.loadHabilidades();
    this.loadTodosSalarios();
    this.loadSalarioSelecionado();
  }

  /*private loadNivel() {
    this.tipoNivelServices$ = this.nivelCargoService.getTodosNiveisCargo();

      this.parentForm.controls.nivel.valueChanges.subscribe(valor => {

        if(valor === "" || valor === undefined){
          this.parentForm.controls.nivel.setValue("-1");
        }
      });
  }*/

  /*private loadFuncao() {
    this.tipoFuncaoServices$ = this.funcaoCargoServices.getTodasFuncoesCargo();
    this.parentForm.controls.funcaoCargo.valueChanges.subscribe(valor => {

      if(valor === "" || valor === undefined){
        this.parentForm.controls.funcaoCargo.setValue(-1);
      }
    });


  }*/

  private loadAreaAtuacao() {
    this.areaAtuacaoServices.getAreaAtuacaoHabilitadas().subscribe(valor => {
      valor.forEach(area => this.areas.push({label: area.name, value: area.termId}));
    });
  }

  private loadHabilidades() {
    this.habilidadeServices.getTodasHabilidades().subscribe(valor => {
      valor.forEach(hab => this.habilidades.push({label: hab.name, value: hab.termId}));
    });
  }


  private loadTodosSalarios() {
    this.salarioServices.getTodosSalarios().subscribe(valor => {
      valor.forEach(hab => {
        this.optionsSalario.push({label: hab.name, value: hab.name})

      });
    });
  }

  private loadSalarioSelecionado() {

  }

  atualizar()
  {
    this.ngSubmit.emit();
  }

}
