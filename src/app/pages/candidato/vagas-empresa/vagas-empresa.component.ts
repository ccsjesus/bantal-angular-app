import Swal from 'sweetalert2';
import { InscricaoVaga } from './../../models/inscricao-vaga.model';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VagasEmpresaServices } from './../../../shared/services/vagas.empresa.service';
import { VagasCandidatoServices } from './../../../shared/services/vaga.candidato.service';
import { SelectItem } from 'primeng/api/selectitem';
import { Vaga } from '../../models/vaga.model';
import { Component, OnInit, Injector } from '@angular/core';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
import { noImage } from '../../constantes/bantal_b64';

@Component({
  selector: 'app-vagas-empresa',
  templateUrl: './vagas-empresa.component.html',
  styleUrls: ['./vagas-empresa.component.scss']
})
export class VagasEmpresaComponent extends BaseResourceFormComponent<Vaga> implements OnInit {

  vagas: Vaga[];

  sortOptions: SelectItem[];

  sortOrder: number;

  sortField: string;

  formDadosVagasEmpresa: FormGroup;

  ready: Boolean = false;

  semImagem: any = noImage;


  constructor(
    private fb: FormBuilder,
    protected injector: Injector,
    protected vagaCandidatoServices: VagasCandidatoServices,
    protected vagasEmpresaServices: VagasEmpresaServices,
    private sanitizer: DomSanitizer,
    protected router: Router) {
    super(injector, new Vaga(), null, Vaga.fromJson);

    }

  ngOnInit(): void {

  this.formDadosVagasEmpresa = this.fb.group({
      vagas: ['']
    });

    this.vagasEmpresaServices.getVagas().subscribe(valor => {
      if(valor.length > 0) {
        this.vagas = valor;
        this.ready = true;
      }
    });

  }

 adicionarTag(valor){
    //let objectURL = 'data:image/jpeg;base64,' + valor;
    var foto;
    if(valor == null){
      foto = this.semImagem;
    } else{
      foto = valor;
    }
    return this.sanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + foto);
  }

protected realizarCandidatura(vaga: any) {

    var foto;
    if(vaga.foto == null){
      foto = this.semImagem;
    } else{
      foto = vaga.foto;
    }
    var areaAtuacao = vaga.nomeAreaAtuacao === null ? 'Não determinado' :  vaga.nomeAreaAtuacao;

    Swal.fire({
      title: vaga.cdVaga +" - "+ vaga.titulo,
      html: '<div class="row" style="align-items: center;justify-content: center;">'+
            '<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
            '</div>'+
            '<div class="row">'+
            ' <label><b>Título da Vaga:</b></label>'+
            ' &nbsp ' + vaga.cdVaga +" - "+ vaga.titulo +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Descrição:</b></label>'+
            ' &nbsp ' + this.removeCaractereHTML(vaga.descricao) +
            '</div>'+
            '<div class="row">'+
            ' <label><b>Data Limite:</b></label>'+
            ' &nbsp ' + vaga.dataLimite +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Localização:</b></label>'+
            ' &nbsp ' + vaga.cidade + '/' + vaga.estado +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Salário:</b></label>'+
            ' &nbsp ' + vaga.salario +
            '</div>' +
            '<div class="row">'+
            ' <label><b>Área de Atuação:</b></label>'+
            ' &nbsp ' + areaAtuacao +
            '</div>'
            ,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            cancelButtonColor: '#d33',
            confirmButtonText:
              '<i class="fa fa-thumbs-up"></i> Candidatar!',
            confirmButtonAriaLabel: 'Ótimo, inscrito com sucesso!',
            cancelButtonText:
              '<i class="fa fa-thumbs-down"></i> Ainda não',
            cancelButtonAriaLabel: 'Thumbs down'

  }).then((result) => {
    if (result.value) {
      var dados = JSON.parse(localStorage.getItem('dados'));
      const inscricaoVaga: InscricaoVaga =  new InscricaoVaga();
      inscricaoVaga.nomeCandidato = dados.displayName;
      inscricaoVaga.codigoVaga = vaga.cdVaga
      this.vagaCandidatoServices.realizarInscricaoVaga(inscricaoVaga).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Sua inscrição foi realizada com sucesso!',
          'success'
        ).then((resultado) => {
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./selecao']);
          });
        })
      });
    }
  })
  }

  protected setCurrentAction() {

  }
  protected buildResourceForm(): void {

  }
  protected onLoadConfigurations(): void {

  }

   removeCaractereHTML(text){
     return text.replace(/<style[^>]*>.*<\/style>/gm, '')
    // Remove script tags and content
    .replace(/<script[^>]*>.*<\/script>/gm, '')
    // Remove all opening, closing and orphan HTML tags
    .replace(/<[^>]+>/gm, '')
    // Remove leading spaces and repeated CR/LF
    .replace(/([\r\n]+ +)+/gm, '');
  }

  obterString(val) {
    return (val + "").replace(/<[^>]*>?/gm, '');;
  }
  
}
