import { VagasEmpresaComponent } from './vagas-empresa.component';
import { VagasEmpresasRoutingModule } from './vagas-empresa-routing.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {ButtonModule} from 'primeng/button';
import {PanelModule} from 'primeng/panel';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import {RatingModule} from 'primeng/rating';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    VagasEmpresasRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    VagasEmpresaComponent
  ],
})
export class VagasEmpresaModule { }
