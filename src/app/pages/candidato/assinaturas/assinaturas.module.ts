import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import { AssinaturasRoutingModule } from './assinaturas-routing.module';
import { AssinaturasComponent } from './assinaturas.component';


@NgModule({  
  imports: [
    SharedModule,
    CommonModule,
    AssinaturasRoutingModule,
    RouterModule,
    ReactiveFormsModule,    
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    AssinaturasComponent
  ],
})
export class AssinaturasModule { }
