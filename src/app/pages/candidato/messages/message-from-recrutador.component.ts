import { MensagemCandidatoService } from './../../../shared/services/mensagem-candidato.services';
  import { FormBuilder, FormGroup } from '@angular/forms';
  import { Router } from '@angular/router';
  import { MensagemModel } from './../../models/mensagem';
  import Swal from 'sweetalert2';
  import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
  import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';


  @Component({
    selector: 'app-messages-recrutador',
    templateUrl: './message-from-recrutador.component.html',
    styleUrls: ['./message-from-recrutador.component.css']
  })
  export class MessagesFromRecrutadorComponent extends BaseResourceFormComponent<MensagemModel> implements OnInit, OnDestroy {

    text: string;
    nomeProcessoSeletivo = '';
    usuario: string;
    formDadosMensagem: FormGroup;
    listaMensagem: MensagemModel[];
    codigoVaga: string;
    possuiMensagem: boolean = false;
    textoCarregando: string = 'Carregando novas mensagens!';
    exibirLoading: Boolean = true;
    textoAguarde: string = 'Aguarde!';
    qtdMsgs: number = 0;

    constructor(protected router: Router,
                protected injector: Injector,
                private fb: FormBuilder,
                protected msgCandidato: MensagemCandidatoService
      ) {
      super(injector, new MensagemModel(), null, MensagemModel.fromJson);

      let objSessao = sessionStorage.getItem('dadosSala');
      let objListaMsg = sessionStorage.getItem('listaMsg');
      if(objSessao){
        var dadosSala = JSON.parse(objSessao);
        this.nomeProcessoSeletivo = dadosSala.codigoselecao;
        this.usuario = localStorage.getItem('nome');

        this.codigoVaga = dadosSala.codigoselecao.split(" ")[0];
        this.msgCandidato.getMensagemCandidato(this.codigoVaga).subscribe(
          dados => {
            if(dados.length > 0) {
              this.possuiMensagem = true;
              this.listaMensagem = dados;
              this.atualizarStatusMsg(JSON.parse(objListaMsg));

            } else {
              this.exibirLoading = false;
              this.textoCarregando = 'Não há mensagens disponíveis!';
              this.textoAguarde = 'Em breve voce receberá alguma notificação.';
            }
            this.qtdMsgs = dados.length;
          }
        );
      }
    }

    atualizarStatusMsg(listaMsg){

      this.msgCandidato.atualizarSitucaoMsgLida(listaMsg).subscribe(dados => {
      })
    }

  ngOnInit(): void {
      this.formDadosMensagem = this.fb.group({
        mensagem: [''],
      });
  }

    ngOnDestroy(): void {}

    protected buildResourceForm() {}

    protected setCurrentAction() {

    }
    protected onLoadConfigurations(): void {

    }

    enviarMensagem(): void {
      const resource: MensagemModel = new MensagemModel();
      resource.mensagem = this.formDadosMensagem.controls.mensagem.value;
      resource.tipoMensagem = 1;
      resource.idVaga = this.codigoVaga;
      this.msgCandidato.inserirMensagem(resource).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Sua Mensagem foi enviada com sucesso !',
          'success'
        ).then((resultado) => {
            this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/mensagem-candidato']);
            });
        })
      })
      }
    }
