import { MessagesFromRecrutadorRoutingModule } from './message-from-recrutador.routing.module';
import { MessagesFromRecrutadorComponent } from './message-from-recrutador.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule,
    MessagesFromRecrutadorRoutingModule,
    ReactiveFormsModule,
    FormsModule

  ],
  declarations: [
    MessagesFromRecrutadorComponent
  ],
})
export class MessagesFromRecrutadorModule { }
