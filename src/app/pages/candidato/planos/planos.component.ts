import { PlanoModel } from './../../../shared/models/plano.model';
import { PlanoServices } from './../../../shared/services/plano.services';
import { ExpiracaoPlanoModel } from './../../models/expiracao-plano.model';
  import { MercadopagoService } from './../../../shared/services/mercadopago.services';
  import { Plano } from './../../models/plano.model';
  import { Component, ViewEncapsulation, OnInit, Injector } from '@angular/core';
  import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
  import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
  import { planos } from '../../constantes/planos'
  import { PagseguroService } from '../../../shared/services/pagseguro.services';
  import { Pagamento } from '../../models/pagamento.model';
  import { Remetente } from '../../models/remetente.model';
  import { Telefone } from '../../models/telefone.model';
  import { Produto } from '../../models/produto.model';
  import { RegisteredCheckout } from '../../models/registered-checkout.model';
  import { PreAprovado } from '../../models/pre-aprovado.model';
  import { RequisicaoPreAprovado } from '../../models/requisicao-pre-aprovado.model';
  import { RegisteredPreApproval } from '../../models/registered-pre-approval.model';
  import { DadosServices } from '../../../shared/services/dados.service';
  import { DadosUsuarioModel } from '../../models/dados-usuario.model';
  import Swal from 'sweetalert2';
  import { PlanoPagamento } from '../../../shared/models/plano-pagamento';
  declare let PagSeguroLightbox: any;
  declare var jQuery: any;
  import { environment } from '../../../../environments/environment';
  import { map, delay, switchMap } from 'rxjs/operators';
  import { forkJoin, fromEvent, of , Observable} from "rxjs";
  import { UsuarioRequest } from '../../models/usuario.pass';


  @Component({
      templateUrl: './planos.component.html',
      styleUrls: ['./planos.component.scss'],
    encapsulation: ViewEncapsulation.None
  })
  export class PlanosComponent  extends BaseResourceFormComponent<Plano> implements OnInit {

    formDadosPlanos: FormGroup;

    slugPlano: Plano;

    planoPagamento: PlanoPagamento;
    id_pagamento: string;
    status: string;
    usuarioModel: DadosUsuarioModel;

    finalizarLoad: boolean = false;
    textoCarregando: string = 'Carregando painel de Planos!';
    exibirLoading: Boolean = true;
    textoAguarde: string = 'Aguarde!';


    protected urlSistema: string = environment.URL_SISTEMA;

    dadosServices$: Observable<UsuarioRequest>;
    dadosExpiracaoPlano$: Observable<ExpiracaoPlanoModel>;
    renovarPlano: boolean = false;
    planoCandidato$: Observable<PlanoModel[]>;
    listaPlanos : PlanoModel[];

    constructor(
      private fb: FormBuilder,
      protected injector: Injector,
      protected pagseguro: PagseguroService,
      protected dadosUsuario: DadosServices,
      protected  mercadoPago: MercadopagoService,
      protected planoService: PlanoServices) {

      super(injector, new Plano(), null, Plano.fromJson);
      this.dadosServices$ = this.dadosUsuario.obterDadosUsuario();
      this.dadosExpiracaoPlano$ = this.dadosUsuario.obterExpiracaoPlano();
      this.planoCandidato$ = this.planoService.getPlanosCandidatos();
    }

    ngOnInit(): void {
      this.obterInformacoesPlano();
      this.obterPlanoUsuario();
      this.obterExpiracaoPlano();
      this.dadosExpiracaoPlano$ = this.dadosUsuario.obterExpiracaoPlano();
      this.route.queryParams.subscribe(params => {
      let prefId = params['preference_id'];
      this.id_pagamento = params['payment_id'];
      this.status = params['status'];
      if(this.id_pagamento != undefined && this.status == 'approved') {

        const joinedAndDelayed$ = forkJoin(
        this.dadosServices$.pipe(
          map(dados => {
            //this.usuarioModel = dados;
            let planoPagamento = new PlanoPagamento();
            planoPagamento.cdTransacao = this.id_pagamento;
            planoPagamento.cdSolicitacaoTransacao = prefId;
            planoPagamento.statusPagamento = this.status;
            return planoPagamento;
        }),
        switchMap( (plano: PlanoPagamento) => this.mercadoPago.registrarPagamento(plano)
        ))
        ).subscribe(valor => {
          this.mercadoPago.exibirMensagemPagamento(this.status);
        })
      }
      });
      this.formDadosPlanos = this.fb.group({
        planos: ['']
      });
  }

    protected obterPlanoUsuario(){
      this.dadosServices$.subscribe((users) => {
        let plan = new Plano();
        if(users.planoUsuario) {
          plan.slug = users.planoUsuario.slug;
          this.slugPlano = plan;
        } else {
          plan.slug = 'inicial';
          this.slugPlano = plan;
        }
        this.finalizarLoad = true;
        this.exibirLoading = false;
      });
    }

    protected obterExpiracaoPlano(){
      this.dadosExpiracaoPlano$.subscribe((expiracaoPlano) => {
        this.renovarPlano = expiracaoPlano.exibirAlertaExpiracao;
      });
    }

    protected obterInformacoesPlano(){
        this.planoCandidato$.subscribe((planos) => {
          this.listaPlanos = planos;
        });
    }

    obterValorPlano(valor1, valor2){
      if(valor1 == 0){
        return '0.00';
      }
      return (valor1 / valor2).toFixed(2);
    }

    protected setCurrentAction() {

    }

    protected verificarPagamentoRealizado(){

    }

    protected buildResourceForm(): void {

    }
    protected onLoadConfigurations(): void {

    }


    checkoutMercadoPago(i) {
      let plano = this.listaPlanos[i];
      let preference = {
        items: [
          {
          title: plano.nome,
          unit_price: Number(plano.valor),
          quantity: 1,
          }
        ],
        "back_urls": {
          "success": this.urlSistema + "/planos",
          "failure": this.urlSistema + "/planos",
          "pending": this.urlSistema + "/planos"
        },
        "auto_return": "approved",
        };
      this.mercadoPago.checkout(preference).subscribe(url => {
        Swal.fire(
          'Só mais um Momento!',
          'Você será redirecionado para o site do Mercado Pago!',
          'warning',
          ).then((resultado) => {
            var novaUrl = new URL(url.url);
            var prefId = novaUrl.searchParams.get("pref_id");
            let solicitacaoPagamentoPlano = new PlanoPagamento();
            solicitacaoPagamentoPlano.cdSolicitacaoTransacao = prefId;
            solicitacaoPagamentoPlano.cdPlano = plano.cdPlano;
            this.mercadoPago.registrarSolicitacaoPagamento(solicitacaoPagamentoPlano).subscribe(valor => {

              window.location.href = url.url;

          })
        });
      });

    }

    }
