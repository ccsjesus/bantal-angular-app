import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import { PlanosRoutingModule } from './planos-routing.module';
import { PlanosComponent } from './planos.component';


@NgModule({  
  imports: [
    SharedModule,
    CommonModule,
    PlanosRoutingModule,
    RouterModule,
    ReactiveFormsModule,    
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    PlanosComponent
  ],
})
export class PlanosModule { }
