import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuardCandidato } from '../../guards/auth-candidato.guard';
import { CandidatoComponent } from './candidato.component';

const routesCandidato: Routes = [{
  path: '',
  component: CandidatoComponent,
  canActivate: [AuthGuardCandidato],
  children: [
    {
      path: '',
      loadChildren: './selecao/selecao.module#SelecaoModule',
    },
    {
      path: 'selecao',
      loadChildren: './selecao/selecao.module#SelecaoModule',
    },
    {
      path: 'curriculo',
      loadChildren: './curriculo/curriculo.module#CurriculoModule',
    },
    {
      path: 'planos',
      loadChildren: './planos/planos.module#PlanosModule',
    },
    {
      path: 'assinaturas',
      loadChildren: './assinaturas/assinaturas.module#AssinaturasModule',
    },
    {
      path: 'informacoes',
      loadChildren: '../dados/dados.module#DadosModule',
    },
    {
      path: 'mensagem-candidato',
      loadChildren: './messages/message-from-recrutador.module#MessagesFromRecrutadorModule',
    },
    {
      path: 'find-vaga-empresa',
      loadChildren: './vagas-empresa/vagas-empresa.module#VagasEmpresaModule',
    },
    {
      path: 'suporte-candidato',
      loadChildren: '../suporte/suporte.module#SuporteModule',
    },
    {
      path: 'candidato',
      redirectTo: 'selecao',
      pathMatch: 'full',
      runGuardsAndResolvers: 'always',
    }
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routesCandidato)],

  exports: [RouterModule],
})
export class CandidatoRoutingModule {
}
