import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { AuthGuardEmpresa } from '../guards/auth-empresa.guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuardEmpresa],
  children: [
    {
      path: '',
      loadChildren: './painel-empresa/painel-empresa.module#PainelEmpresaModule',
    },
    {
      path: 'empresa',
      loadChildren: './painel-empresa/painel-empresa.module#PainelEmpresaModule',
    },
    {
      path: 'recrutamento',
      loadChildren: './recrutamento/recrutamento.module#RecrutamentoModule',
    },
    {
      path: 'colaborador',
      loadChildren: './dashboard/dashboard.module#DashboardModule',
    },
    {
      path: 'vaga',
      loadChildren: './vagas/vagas.module#VagasModule',
    },
    {
      path: 'dados',
      loadChildren: './dados/dados.module#DadosModule',
    },
    {
      path: 'messagem-empresa',
      loadChildren: './recrutamento/messages-recrutador/message-recrutador.module#MessagesRecrutadorModule',
    },
    {
      path: 'planos-empresa',
      loadChildren: './planos-empresa/planos-empresa.module#PlanosEmpresaModule',
    },
    {
      path: 'suporte',
      loadChildren: './suporte/suporte.module#SuporteModule',
    },
    {
      path: 'banco-talentos',
      loadChildren: './banco-talentos/banco-talentos.module#BancoTalentosModule',
    },

    {
      path: 'empresa',
      redirectTo: 'empresa',
      pathMatch: 'full',
      runGuardsAndResolvers: 'always',
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule],
})
export class PagesRoutingModule {
}
