import { PlanosEmpresaComponent } from './planos-empresa.component';
import { PlanosEmpresaRoutingModule } from './planos-empresa-routing.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    PlanosEmpresaRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    PlanosEmpresaComponent
  ],
})
export class PlanosEmpresaModule { }
