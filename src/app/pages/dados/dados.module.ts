import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DadosRoutingModule } from './dados.routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { DadosComponent } from './dados.component';

@NgModule({  
  imports: [
    RouterModule,
    DadosRoutingModule,    
    ReactiveFormsModule,    
    FormsModule,    
    SharedModule
  ],
  declarations: [ 
    DadosComponent
  ],
})
export class DadosModule { }
