import { imgPerfil64 } from './../constantes/user';
import { Component, OnInit, Injector } from '@angular/core';
import { UserService } from '../../@core/mock/users.service';
import { User } from '../../@core/data/users';
import { of as observableOf,  Observable } from 'rxjs';
import { UsuarioRequest } from '../models/usuario.pass';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { BaseResourceFormComponent } from '../../shared/components/base-resource-form/base-resource-form.component';
import { DadosServices } from '../../shared/services/dados.service';
import Swal from 'sweetalert2';
import { Validacoes } from '../../shared/components/validacoes';
import { ROLE_EMPLOYER } from '../../pages/constantes';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.scss']
})
export class DadosComponent extends BaseResourceFormComponent<UsuarioRequest> implements OnInit {

  isCandidato: boolean = true;

  user: UsuarioRequest;

  constructor(protected injector: Injector,
    protected dadosServices: DadosServices) {
    super(injector, new UsuarioRequest(), null, UsuarioRequest.fromJson)
   }

  ngOnInit() {

    let funcaoSistema = localStorage.getItem('funcaoSistema');
    if (funcaoSistema === ROLE_EMPLOYER) {
      this.isCandidato = false;
    }

    this.buildResourceForm();
    this.getDadosUsuario().subscribe((users: UsuarioRequest) => {
      this.resourceForm.controls.displayName.setValue(users.displayName);
      this.resourceForm.controls.first_name.setValue(users.first_name);
      this.resourceForm.controls.email.setValue(users.userEmail);
      this.resourceForm.controls.sobreEmpresa.setValue(users.sobreEmpresa);
      this.resourceForm.controls.sigla.setValue(users.sigla);
      this.resourceForm.controls.last_name.setValue(users.last_name);
      if(users.foto === null){
        this.resourceForm.controls.foto.setValue(imgPerfil64);
      } else {
        this.resourceForm.controls.foto.setValue(users.foto);
      }
    });//this.user = users);
  }

  getDadosUsuario(): Observable<UsuarioRequest> {
    return this.dadosServices.obterDadosUsuario();
  }

  protected setCurrentAction() {
    throw new Error("Method not implemented.");
  }
  protected onLoadConfigurations(): void {
    throw new Error("Method not implemented.");
  }

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      first_name: [null],
      email:[null],
      sobreEmpresa:[null],
      sigla:[null],
      userPass: [null],
      newSenha: [null],
      confSenha: [null, Validators.compose([Validators.required])],
      displayName:[null],
      last_name: [null],
      foto: [null]
    },
    {
      validator: Validacoes.SenhasCombinam
    });
  }

  get newSenha() {
    return this.resourceForm.get('newSenha');
  }

  get confSenha() {
    return this.resourceForm.get('confSenha');
  }

  validarSenhas = (confirmarSenha: FormControl): ValidatorFn => {
    if (this.resourceForm) {
    }
    return null;
  }

  updateDados(){
    const resource: UsuarioRequest = this.jsonDataToResourceFn(this.resourceForm.value);
    this.dadosServices.alterarDados(resource).subscribe(res => {

      Swal.fire(
        'Ótimo!',
        'Dados atualizados com sucesso!',
        'success'
      ).then((resultado) => {
        this.router.navigate(['./recrutamento']);
      })

    })
  }

}
