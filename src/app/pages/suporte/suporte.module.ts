import { SuporteRoutingModule } from './suporte-routing.module';
import { SuporteComponent } from './suporte.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    SuporteRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    SuporteComponent
  ],
})
export class SuporteModule { }
