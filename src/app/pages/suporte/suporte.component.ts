import { SuporteServices } from './../../shared/services/suporte.services';
import { UsuarioRequest } from './../models/usuario.pass';
import { SuporteModel } from './../models/suporte.model';
import { DadosServices } from './../../shared/services/dados.service';
import { Validators } from '@angular/forms';
import { of as observableOf,  Observable } from 'rxjs';
import { Component, OnInit, Injector } from '@angular/core';
import { BaseResourceFormComponent } from '../../shared/components/base-resource-form/base-resource-form.component';
import Swal from 'sweetalert2';


@Component({
  selector: 'suporte',
  templateUrl: './suporte.component.html',
  styleUrls: ['./suporte.component.scss']
})
export class SuporteComponent  extends BaseResourceFormComponent<SuporteModel> implements OnInit {

  tituloSuporte: string = 'Receba um atendimento personalizado pela nossa equipe. Basta preencher o formulário abaixo que entraremos em contato.';
  suporte: SuporteModel;

  constructor(protected injector: Injector,
              protected dadosServices: DadosServices,
              protected suporteServices: SuporteServices) {
    super(injector, new SuporteModel(), null, SuporteModel.fromJson)
   }

  ngOnInit() {
    this.buildResourceForm();
    this.getDadosUsuario().subscribe((users: UsuarioRequest) => {
      this.resourceForm.controls.destinatario.setValue(users.userEmail);
      this.resourceForm.controls.nome.setValue(users.first_name);
    });
  }

  getDadosUsuario(): Observable<UsuarioRequest> {
    return this.dadosServices.obterDadosUsuario();
  }

  protected buildResourceForm() {

    this.resourceForm = this.formBuilder.group({
      mensagem: [null, [Validators.required]],
      nome: [null],
      destinatario: [null],
      assunto: [null, [Validators.required]],
      suporte: []
    });
  }

  protected setCurrentAction() {
    throw new Error("Method not implemented.");
  }
  protected onLoadConfigurations(): void {
    throw new Error("Method not implemented.");
  }

  enviarEmail(){
    const resource: SuporteModel = this.jsonDataToResourceFn(this.resourceForm.value);
    this.suporteServices.enviarEmail(resource).subscribe(res => {

      Swal.fire(
        'Ótimo!',
        'Sua Mensagem foi enviada. Em até 24h te daremos um retorno!',
        'success'
      ).then((resultado) => {
        this.resourceForm.controls.assunto.setValue("");
        this.resourceForm.controls.mensagem.setValue("");
      })

    })
  }

}
