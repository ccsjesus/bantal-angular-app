import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NbCardModule, NbStepperModule, NbButtonModule, NbInputModule, NbCheckboxModule, NbRadioModule, NbActionsModule, NbSelectModule, NbIconModule, NbListModule, NbTabsetModule } from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router'
import { AvaliacaoComponent } from './avaliacao.component';
import { InformacoesComponent } from './informacoes/informacoes.component';
import { AnaliseComportamentalComponent } from './analise-comportamental/analise-comportamental.component';
import { StatusCardComponent } from '../status-card/status-card.component';
import { CandidatosComponent } from './candidatos/candidatos.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AccordionModule } from 'primeng/accordion';
import { CurriculoComponent } from './curriculo/curriculo.component';
import { DadosPessoaisComponent } from './curriculo/dados-pessoais/dados-pessoais.component';
import { InformacoesCargoComponent } from './curriculo/informacoes-cargo/informacoes-cargo.component';
import { InformacoesFormacaoComponent } from './curriculo/informacoes-formacao/informacoes-formacao.component';
import { InformacoesExperienciaComponent } from './curriculo/informacoes-experiencia/informacoes-experiencia.component';
import { PerguntasComponent } from './perguntas/perguntas.component';
import { InputTextareaModule } from 'primeng/inputtextarea';

@NgModule({
  imports: [
    NbActionsModule,
    NbSelectModule,
    NbIconModule,
    ReactiveFormsModule,
    FormsModule,
    NbCardModule,
    ThemeModule,
    TableModule, 
    ButtonModule,
    RouterModule,
    NbStepperModule,
    NbButtonModule,
    NbInputModule,
    NbCheckboxModule, 
    NbRadioModule, 
    TableModule,
    PdfViewerModule,
    AccordionModule,
    NbListModule,
    InputTextareaModule,
    NbTabsetModule
  ],
  declarations: [
    AvaliacaoComponent,
    InformacoesComponent,
    AnaliseComportamentalComponent,
    StatusCardComponent,
    CandidatosComponent,
    CurriculoComponent,
    DadosPessoaisComponent,
    InformacoesCargoComponent,
    InformacoesFormacaoComponent,
    InformacoesExperienciaComponent,
    PerguntasComponent
  ],
  providers:[  ]
})
export class AvaliacaoModule { }
