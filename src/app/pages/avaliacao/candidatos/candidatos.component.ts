import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-candidatos',
  templateUrl: './candidatos.component.html',
  styleUrls: ['./candidatos.component.scss']
})
export class CandidatosComponent implements OnInit {

  @Input() parentForm: FormGroup;

  @Input() parentFormSecundario: FormGroup;
  
  constructor() { }

  ngOnInit() {
  }

}
