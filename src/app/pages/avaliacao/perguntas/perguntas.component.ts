import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perguntas',
  templateUrl: './perguntas.component.html',
  styleUrls: ['./perguntas.component.scss']
})
export class PerguntasComponent implements OnInit {

  perguntas = ["1","2","3","4","5","6","7","8","9","10","11"]

  // current page of items
  pageOfItems: Array<any>;
  
  constructor() { }

  ngOnInit() {
    
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

}
