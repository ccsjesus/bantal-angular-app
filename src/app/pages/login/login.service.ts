import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../models/usuario.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, catchError } from "rxjs/operators";
import { BaseResourceService } from '../../shared/services/base-resource.service';
import { UserService } from '../../@core/mock/users.service';
import { UsuarioRequest } from '../models/usuario.pass';
import { environment } from '../../../environments/environment';
import { FUNCAO_CANDIDATO, FUNCAO_EMPRESA, FUNCAO_ADMINISTRATOR, ROLE_EMPLOYER, ROLE_CANDIDATE, ROLE_ADMIN } from '../constantes';
import { LoginRequest } from '../models/login.model';
import { LoginResponseModel } from '../models/login.response.model';
import { decodeJwtPayload } from '@nebular/auth';


@Injectable()
export class LoginService extends BaseResourceService<Usuario>{

  private usuarioLogado: boolean = false;
  public token: string;
  protected url: string = environment.API + environment.BASE_URL +'/login';
  private _loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public loggedIn: Observable<boolean> = this._loggedIn.asObservable();
  private userSubject: BehaviorSubject<LoginResponseModel>;
  public user: Observable<LoginResponseModel>;
  private refreshTokenTimeout;


  constructor(private router: Router, protected injector: Injector, protected mockServices: UserService) {
    super(environment.API + environment.BASE_URL +'/auth/login', injector, Usuario.fromJson);
    this.userSubject = new BehaviorSubject<LoginResponseModel>(null);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): LoginResponseModel {
    return this.userSubject.value;
}
  login(usuario: LoginRequest) {
    return this.http.post<any>(this.url, usuario, { withCredentials: true })
        .pipe(map(user => {
            this.userSubject.next(user);
            this.startRefreshTokenTimer();
            return user;
        }));
  }

    logout2() {
      this.http.post<any>(`${this.url}/users/revoke-token`, {}, { withCredentials: true }).subscribe();
      this.stopRefreshTokenTimer();
      this.userSubject.next(null);
      this.router.navigate(['./auth/login']);
  }

  refreshToken() {
    let refreshToken = {
      refreshToken: this.userValue.refreshToken
    }
    return this.http.post<any>(`${this.url}/auth/refreshtoken`, { refreshToken }, { withCredentials: true })
        .pipe(map((user) => {
            this.userSubject.next(user);
            this.startRefreshTokenTimer();
            return user;
        }));
  }
  private startRefreshTokenTimer() {
    const jwtToken = JSON.parse(atob(this.userValue.accessToken.split('.')[1]));
    
    const expires = new Date(jwtToken.expiresIn * 1000);
    const timeout = expires.getTime() - Date.now() - (60 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
  }

  private stopRefreshTokenTimer() {
      clearTimeout(this.refreshTokenTimeout);
  }

  realizarLogin(usuario: LoginRequest) : Observable<LoginResponseModel>{
    console.log(this.url);

    return this.http.post<LoginResponseModel>(this.url, usuario).pipe(
      map((res) => {        
        var jwt = decodeJwtPayload(res.accessToken)
        var token = res.accessToken;
        var nome = jwt.name;
        var funcaoSistema = jwt.roles;

        if (token) {
            this._loggedIn.next(true);
            localStorage.setItem('token', token);
            localStorage.setItem('nome', nome);
            localStorage.setItem('dados', JSON.stringify(res));
            localStorage.setItem('funcaoSistema', funcaoSistema);
        }
     }), catchError(this.handleError)
        //this.jsonDataToResource.bind(this)),
    )
  }

  realizarLogin2(usuario: UsuarioRequest) : Observable<Usuario>{

    return this.http.post<UsuarioRequest>(this.apiPath, usuario).pipe(
      map((res) => {
        var token = res.tokens[0].token;
        var nome = res.first_name;
        var funcaoSistema = res.funcaoSistema;

        if (token) {
            this._loggedIn.next(true);
            localStorage.setItem('token', token);
            localStorage.setItem('nome', nome);
            localStorage.setItem('dados', JSON.stringify(res));
            localStorage.setItem('funcaoSistema', funcaoSistema);
        }
     }), catchError(this.handleError)
        //this.jsonDataToResource.bind(this)),
    )
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('nome');
    localStorage.removeItem('dados');
    this.router.navigate(["./auth/login"]);
  }
  

  isLoggedIn() {
    let token = localStorage.getItem('token');

    if(token){ //essa atribuição é feita para atualizar a variavel e o resto do sistema ser notificado
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }

  isAdmin() {
    let funcaoSistema = localStorage.getItem('funcaoSistema');

    if(funcaoSistema == FUNCAO_ADMINISTRATOR || funcaoSistema == ROLE_ADMIN){
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }

  isCandidato() {
    let funcaoSistema = localStorage.getItem('funcaoSistema');

    if(funcaoSistema == FUNCAO_CANDIDATO || funcaoSistema == ROLE_CANDIDATE){
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }

  isEmpresa() {
    let funcaoSistema = localStorage.getItem('funcaoSistema');

    if(funcaoSistema === FUNCAO_EMPRESA || funcaoSistema == ROLE_EMPLOYER){
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }

  isAdministrador() {
    let funcaoSistema = localStorage.getItem('funcaoSistema');

    if(funcaoSistema === FUNCAO_ADMINISTRATOR || funcaoSistema == ROLE_ADMIN){
      this._loggedIn.next(true);
    } else {
      this._loggedIn.next(false);
    }

    return this._loggedIn.getValue();
  }
}
