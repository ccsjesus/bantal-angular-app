import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule }   from '@angular/forms';

import { NbCardModule, NbLayoutModule, NbAlertModule, NbButtonModule, NbInputModule, NbUserModule, NbContextMenuModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';

import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { PasswordModule } from 'primeng/password';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    NbContextMenuModule,
    ReactiveFormsModule,
    NbUserModule,
    NbInputModule,
    NbButtonModule,
    NbLayoutModule,
    NbAlertModule,
    FormsModule,
    CommonModule,
    NbCardModule,
    ThemeModule,
    NbCardModule,
    PasswordModule,
    InputTextModule,
    ButtonModule,
    SharedModule
  ],
  declarations: [
    LoginComponent
  ],
})
export class LoginModule { }
