import { Component, Injector } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { Usuario } from '../models/usuario.model';
import { BaseResourceFormComponent } from '../../shared/components/base-resource-form/base-resource-form.component';
import { LoginService } from './login.service';
import { Validators } from '@angular/forms';
import { NgBrazilValidators } from 'ng-brazil';
import { utilsBr } from 'js-brasil';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./login.component.scss']
})

export class LoginComponent extends BaseResourceFormComponent<Usuario> {
  user: any = {};
  errors: string[] = [];
  messages: string[] = [];
  submitted: boolean = false;
  showMessages: any;
  usuario: Usuario = new Usuario();
  public MASKS = utilsBr.MASKS;

  private loaders: Promise<any>[] = [];

  constructor(protected loginService: LoginService,
              protected injector: Injector) {
    super(injector, new Usuario(), loginService, Usuario.fromJson) ;
  }

  protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      identifier: [''],
      password: ['']
    });
  }

  protected onLoadConfigurations(): void {

  }

  protected setCurrentAction() {
    this.currentAction = "auth";
  }

  onKeyUpEvent (e): void  {

    const message = document.querySelector('.message');
    if (e.getModifierState('CapsLock')) {
        message.textContent = ' CAPS LOCK  ligado !';
    } else {
        message.textContent = '';
    }
}

  submit(form) {
    Object.keys(form.controls).forEach((key) => {
      if(form.get(key).value.length > 11) {
        form.get(key).setValue(form.get(key).value.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5"));
      } else {
        form.get(key).setValue(form.get(key).value.replace(/^(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4"));
      }
    }
    );
    super.submitForm();
  }

}
