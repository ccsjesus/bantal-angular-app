import { AdminComponent } from './admin.component';
import { AuthGuardAdministrador } from './../../guards/auth-admin.guard';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  canActivate: [AuthGuardAdministrador],
  children: [
    {
      path: '',
      loadChildren: './dashboard/dashboard.admin.module#DashboardAdminModule',
    },
    {
      path: 'admin',
      loadChildren: './dashboard/dashboard.admin.module#DashboardAdminModule',
    },
    {
      path: 'admin-vagas',
      loadChildren: './configuracoes/configuracoes.admin.module#ConfiguracoesAdminModule',
    },
    {
      path: 'admin-area-atuacao',
      loadChildren: './configuracoes/admin-area-atuacao/admin-area-atuacao.module#AdminAreaAtuacaoModule',
    },
    {
      path: 'admin',
      redirectTo: 'admin',
      pathMatch: 'full',
      runGuardsAndResolvers: 'always',
    }
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule],
})
export class AdminRoutingModule {
}
