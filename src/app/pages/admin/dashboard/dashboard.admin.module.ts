import { SharedModule } from './../../../shared/shared.module';
import { DashboardAdminRoutingModule } from './dashboard.admin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { RouterModule, Routes } from '@angular/router';
import { DashboardAdminComponent } from './dashboard.admin.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaVagasComponent } from './lista-vagas/lista-vagas.component';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    DashboardAdminRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
    DashboardAdminComponent,
    ListaVagasComponent
  ],
})
export class DashboardAdminModule { }
