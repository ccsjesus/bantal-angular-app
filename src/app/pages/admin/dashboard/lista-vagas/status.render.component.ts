import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';


@Component({
  template: `
   <div title="{{value}}" [style]="styleFromParent" class="tamanho-status text-center visible" [innerHtml]="renderValue">
   </div>
   <div class="text-center">
    <span>{{value}}</span>
   </div>

`,
  styleUrls: ['./lista-vagas.component.scss']
})
export class StatusRenderComponent implements ViewCell, OnInit {

  renderValue: string;
  styleFromParent;

  @Input() value: any;
  @Input() rowData: any;

   constructor(private _detectChanges: ChangeDetectorRef) {}

  ngOnInit() {
    setTimeout(() => {

        if(this.value === 'approved'){
          this.value = 'APROVADA';
          this.renderValue = '<i class="nb-checkmark-circle"></i>';
          this.styleFromParent = { color: 'green', height: '100%', width: '100%'};
        } else if(this.value === 'new') {
          this.value = 'NOVA';
          this.renderValue = '<i class="nb-alert"></i>';
          this.styleFromParent = { color: '#F4A460', height: '100%', width: '100%'};
        } else if(this.value === 'expired') {
          this.value = 'EXPIRADA';
          this.renderValue = '<i class="nb-close-circled"></i>';
          this.styleFromParent = { color: 'red', height: '100%', width: '100%'};
        } else if(this.value === 'excluded') {
          this.value = 'EXCLUÍDA';
          this.renderValue = '<i class="nb-close-circled"></i>';
          this.styleFromParent = { color: 'red', height: '100%', width: '100%'};
        }

        this._detectChanges.detectChanges();
      }, 100);

    }

  }

