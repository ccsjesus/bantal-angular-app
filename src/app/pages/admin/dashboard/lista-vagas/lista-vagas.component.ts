import { VagaServices } from './../../../../shared/services/vaga.service';
import { Router } from '@angular/router';
import  Swal  from 'sweetalert2';
import { StatusRenderComponent } from './status.render.component';
import { TipoVagaRenderComponent } from './tipo-vaga.render.component';
import { AreaAtuacaoRenderComponent } from './area-atuacao.render.component';
import { TipoVagaModel } from './../../../models/tipo.vaga.model';
import { FormGroup } from '@angular/forms';
import { DadosAdministrativo } from './../../../models/dados-administrativo.model';
import { AdministrativoService } from './../../../../shared/services/administrativo.service';
import { Component, OnInit, Input } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'app-lista-vagas',
  templateUrl: './lista-vagas.component.html',
  styleUrls: ['./lista-vagas.component.scss']
})
export class ListaVagasComponent implements OnInit {

@Input('parentForm')
public parentForm: FormGroup;

settings = {
  //hideSubHeader: true,
  actions: {
    custom: [
      {
        name: 'editAction',
        title: '<i class="ion-edit" title="Edit"></i>'
      },
      {
        name: 'deleteAction',
        title: '<i class="ion-trash-a" title="Delete"></i>'
      },
    ],
    add: false,
    edit: false,
    delete: false
  },
  columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      areaAtuacaoModel: {
        title: 'Area de Atuacao',
        type: 'custom',
        renderComponent: AreaAtuacaoRenderComponent,
      },
      titulo: {
        title: 'Cargo',
        type: 'string',
      },
      tipoVagaModel: {
        title: 'Tipo Vaga',
        type: 'custom',
        renderComponent: TipoVagaRenderComponent,
      },
      nomeEmpresa: {
        title: 'Empresa',
        type: 'string',
      },
      dataLimite: {
        title: 'Expira em',
        type: 'number',
      },
      status: {
        title: 'Status',
        type: 'custom',
        renderComponent: StatusRenderComponent,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(protected admService: AdministrativoService,
              protected vagaService: VagaServices,
              protected router: Router) {
    admService.getObterDados().subscribe(data => {
      this.source.load(data.listaVagas);
    });
  }
   customFunction(event): void {
     if(event.action === 'deleteAction') {
      this.abrirDeleteDialog(event);
     } else {
      this.abrirConfirmacaoDialog(event);
     }
  }

  abrirConfirmacaoDialog(data){

        const vaga = data.data;
        const tituloVaga = '';

        if(vaga.status !== 'new'){

          Swal.fire(
              'Ops!',
              'Vagas com as situações EXPIRADA ou PUBLICADA não podem ser aprovadas!',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Aprovar vaga ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Título da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.titulo +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Localização:</b></label>'+
                  ' &nbsp ' + vaga.localizacao +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Tipo da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.tipoVagaModel.nome +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Área de Atuação:</b></label>'+
                  ' &nbsp ' + vaga.areaAtuacaoModel.name +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Descrição:</b></label>'+
                  ' &nbsp ' + vaga.descricao +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Data Limite:</b></label>'+
                  ' &nbsp ' + vaga.dataLimite +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Salário:</b></label>'+
                  ' &nbsp ' + vaga.salario +
                  '</div>',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Publicar ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.aprovar(vaga.id);
          }

        })
        }
  }

  abrirDeleteDialog(data) {

        const vaga = data.data;
        const tituloVaga = '';

        if(vaga.status !== 'new'){

          Swal.fire(
              'Ops!',
              'Vagas com as situações EXPIRADA ou PUBLICADA não podem ser exluídas! ',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Excluir vaga ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Título da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.titulo +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Localização:</b></label>'+
                  ' &nbsp ' + vaga.localizacao +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Tipo da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.tipoVagaModel.nome +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Área de Atuação:</b></label>'+
                  ' &nbsp ' + vaga.areaAtuacaoModel.name +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Descrição:</b></label>'+
                  ' &nbsp ' + vaga.descricao +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Data Limite:</b></label>'+
                  ' &nbsp ' + vaga.dataLimite +
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Salário:</b></label>'+
                  ' &nbsp ' + vaga.salario +
                  '</div>',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Excluir ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.excluir(vaga.id);
          }

        })
        }
  }

  aprovar(codigoVaga) {

      this.vagaService.aprovarVaga(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Vaga foi aprovada com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin']);
          });

        })
      });
  }

  excluir(codigoVaga) {

      this.vagaService.excluirVaga(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Vaga foi excluída com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin']);
          });

        })
      });

  }

ngOnInit(): void {
  }

}
