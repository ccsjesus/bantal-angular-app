import { TipoVagaModel } from './../../../models/tipo.vaga.model';
import { AreaAtuacaoModel } from './../../../models/area.atuacao.model';
import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  template: `
    {{renderValue}}
  `,
})
export class TipoVagaRenderComponent implements ViewCell, OnInit {

  renderValue: string;

  @Input() value: any;
  @Input() rowData: any;

  ngOnInit() {
    if(this.value){
      this.renderValue = this.value.nome.toString().toUpperCase();
    }
  }

}
