import { DadosAdministrativo } from './../../models/dados-administrativo.model';
import { AdministrativoService } from './../../../shared/services/administrativo.service';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Injector } from '@angular/core';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.admin.component.html',
  styleUrls: ['./dashboard.admin.component.scss']
})
export class DashboardAdminComponent extends BaseResourceFormComponent<DadosAdministrativo> implements OnInit {

  user: any = {};
  errors: string[] = [];
  messages: string[] = [];
  submitted: boolean = false;
  showMessages: any;
  protected formBuilder: FormBuilder;
  resourceForm: FormGroup;
  dados: DadosAdministrativo = {
    listaPerfil: [],
    listaVagas: [],
    listaCurriculos: [],
    listaInscricaoVagas: []
  };
  inscricoesVaga: any;
  vagasAbertas: any;
  formDados: FormGroup;



  constructor( protected admService: AdministrativoService,
               protected injector: Injector,
               private fb: FormBuilder) {
    super(injector, new DadosAdministrativo(), null, DadosAdministrativo.fromJson);
  }

  ngOnInit(): void {
    this.admService.getObterDados().subscribe(data => {
      this.dados = data;
    });
  }

  loadDados(valor){

  }

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }

}
