import { MENU_ITEMS_ADMINISTRADOR } from './../pages-menu';
import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS_EMPRESA, MENU_ITEMS_CANDIDATO } from '../pages-menu';

import { FUNCAO_ADMINISTRATOR, FUNCAO_CANDIDATO, FUNCAO_EMPRESA, FUNCAO_UNDEFINED, ROLE_ADMIN, ROLE_CANDIDATE, ROLE_EMPLOYER } from '../constantes';

@Component({
  selector: 'ngx-pages',
  templateUrl: './admin.component.html',
  styleUrls: ['admin.component.scss'],
})
export class AdminComponent implements OnInit {

  menu = [];

  ngOnInit() {
    let acesso = localStorage.getItem('funcaoSistema');

    if( acesso === FUNCAO_CANDIDATO || acesso === ROLE_CANDIDATE){
      this.menu = MENU_ITEMS_CANDIDATO;
      return;
    } else if(acesso === FUNCAO_EMPRESA || acesso === ROLE_EMPLOYER) {
      this.menu = MENU_ITEMS_EMPRESA;
      return;
    } else if (acesso === FUNCAO_ADMINISTRATOR || acesso === ROLE_ADMIN) {
      this.menu = MENU_ITEMS_ADMINISTRADOR;
      return;
    }
  }
}
