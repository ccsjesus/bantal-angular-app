import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAreaAtuacaoComponent } from './admin-area-atuacao.component';

describe('AdminAreaAtuacaoComponent', () => {
  let component: AdminAreaAtuacaoComponent;
  let fixture: ComponentFixture<AdminAreaAtuacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAreaAtuacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAreaAtuacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
