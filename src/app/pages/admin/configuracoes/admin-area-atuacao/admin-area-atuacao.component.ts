import { NAO_HABILITADO } from './../../../constantes';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AreaAtuacaoModel } from './../../../models/area.atuacao.model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AreaAtuacaoServices } from './../../../../shared/services/area.atuacao.services';
import { Component, OnInit, Injector } from '@angular/core';
import { BaseResourceFormComponent } from '../../../../shared/components/base-resource-form/base-resource-form.component';

@Component({
  selector: 'admin-area-atuacao',
  templateUrl: './admin-area-atuacao.component.html',
  styleUrls: ['./admin-area-atuacao.component.scss']
})
export class AdminAreaAtuacaoComponent extends BaseResourceFormComponent<AreaAtuacaoModel> implements OnInit {
    tipoVagaDisponiveis: AreaAtuacaoModel[];

    tipoVagaDesabilitados: AreaAtuacaoModel[];

    tipoVagaSelecionados: AreaAtuacaoModel[];

    draggedTipoVaga: AreaAtuacaoModel;

    formDadosTipoVaga: FormGroup;


    constructor(
               private areaAtuacaoServices: AreaAtuacaoServices,
               protected injector: Injector,
               private fb: FormBuilder) {
      super(injector, new AreaAtuacaoModel(), null, AreaAtuacaoModel.fromJson)
    }

    ngOnInit() {
      this.tipoVagaSelecionados = [];
      this.areaAtuacaoServices.getTodasAreaAtuacao().subscribe(data => {
        this.tipoVagaDisponiveis = data;
      });

      this.areaAtuacaoServices.getAreaAtuacaoDesabilitadas().subscribe(data => {
        this.tipoVagaDesabilitados = data;
      });

      this.formDadosTipoVaga = this.fb.group({
        nome: ['', ],
        valor: ['', ],
        habilitada: ['', ]
    });
    }

    incluir() {

      const resource: AreaAtuacaoModel = new AreaAtuacaoModel()
      resource.name = this.formDadosTipoVaga.controls.nome.value;
      resource.slug = this.formDadosTipoVaga.controls.nome.value;;


      this.areaAtuacaoServices.incluirAreaAtuacao(resource).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Area de atuação foi incluída com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-vagas']);
          });

        })
      });

  }

  ngOnDestroy(): void {}

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }

}
