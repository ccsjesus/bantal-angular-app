import { SharedModule } from './../../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { AdminAreaAtuacaoComponent } from './admin-area-atuacao.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminAreaAtuacaoRoutingModule } from './admin-area-atuacao-routing.module';
import { PainelAdminAreaAtuacaoComponent } from './painel-admin-area-atuacao/painel-admin-area-atuacao.component';
@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    AdminAreaAtuacaoRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule

  ],
  declarations: [
    AdminAreaAtuacaoComponent,
    PainelAdminAreaAtuacaoComponent
  ],
})
export class AdminAreaAtuacaoModule { }
