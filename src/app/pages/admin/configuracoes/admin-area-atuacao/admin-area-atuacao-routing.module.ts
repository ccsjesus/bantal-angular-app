import { AdminAreaAtuacaoComponent } from './admin-area-atuacao.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {
  path: '', component: AdminAreaAtuacaoComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminAreaAtuacaoRoutingModule { }
