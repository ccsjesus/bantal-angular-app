import { StatusRenderComponent } from './status.render.component';
import { LocalDataSource } from 'ng2-smart-table';
import Swal from 'sweetalert2';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AreaAtuacaoServices } from './../../../../../shared/services/area.atuacao.services';
import { Component, OnInit, Injector } from '@angular/core';

@Component({
  selector: 'app-painel-admin-area-atuacao',
  templateUrl: './painel-admin-area-atuacao.component.html',
  styleUrls: ['./painel-admin-area-atuacao.component.scss']
})
export class PainelAdminAreaAtuacaoComponent implements OnInit {

settings = {
  //hideSubHeader: true,
  actions: {
    custom: [
      {
        name: 'editAction',
        title: '<i class="icon ion-checkmark-circled" title="Habilitar"></i>'
      },
      {
        name: 'deleteAction',
        title: '<i class="icon ion-close-circled" style="color:red" title="Desabilitar"></i>'
      },

    ],
    add: false,
    edit: false,
    delete: false
  },
  columns: {
      termId: {
        title: 'Identificação',
        type: 'number',
      },
      name: {
        title: 'Área de Atuação',
        type: 'string'
      },
      habilitada: {
        title: 'Status',
        type: 'custom',
        renderComponent: StatusRenderComponent,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private areaAtuacaoServices: AreaAtuacaoServices,
              protected router: Router,
               protected injector: Injector,
               private fb: FormBuilder) {

    //super(injector, new DadosAdministrativo(), null, DadosAdministrativo.fromJson);
    areaAtuacaoServices.getTodasAreaAtuacao().subscribe(data => {
      this.source.load(data);
    });
  }
   customFunction(event): void {
     if(event.action === 'deleteAction') {
      this.abrirDeleteDialog(event);
     } else {
      this.abrirConfirmacaoDialog(event);
     }
  }

  abrirConfirmacaoDialog(data){

        const vaga = data.data;
        if(vaga.habilitada === 0){

          Swal.fire(
              'Ops!',
              'Área de atuação HABILITADA não pode ser Habilitada!',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Habilitar Área de atuação  ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Área de atuação :</b></label>'+
                  ' &nbsp ' + vaga.name +
                  '</div>',

            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Habilitar ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.habilitar(vaga.termId);
          }

        })
        }
  }

  abrirDeleteDialog(data) {

        const vaga = data.data;
        const tituloVaga = '';

        if(vaga.habilitada === 1){

          Swal.fire(
              'Ops!',
              'Área de atuação DESABILITADA não pode ser Desabilitada! ',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Desabilitar Área de atuação ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Área de atuação :</b></label>'+
                  ' &nbsp ' + vaga.name+
                  '</div>',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Desabilitar ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.desabilitar(vaga.termId);
          }

        })
        }
  }

  habilitar(codigoVaga) {

      this.areaAtuacaoServices.habilitarAreaAtuacao(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Área de atuação foi habilitado com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-area-atuacao']);
          });

        })
      });
  }

  desabilitar(codigoVaga) {

      this.areaAtuacaoServices.desabilitarAreaAtuacao(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Área de atuação foi desabilitada com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-area-atuacao']);
          });

        })
      });

  }

ngOnInit(): void {
  }

}
