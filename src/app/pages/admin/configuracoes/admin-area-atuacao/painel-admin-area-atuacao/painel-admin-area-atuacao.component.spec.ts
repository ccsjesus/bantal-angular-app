import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelAdminAreaAtuacaoComponent } from './painel-admin-area-atuacao.component';

describe('PainelAdminAreaAtuacaoComponent', () => {
  let component: PainelAdminAreaAtuacaoComponent;
  let fixture: ComponentFixture<PainelAdminAreaAtuacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainelAdminAreaAtuacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelAdminAreaAtuacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
