import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';


@Component({
  template: `
   <div title="{{value}}" [style]="styleFromParent" class="tamanho-status text-center visible" [innerHtml]="renderValue">
   </div>
   <div class="text-center">
    <span>{{value}}</span>
   </div>

`,
  styleUrls: ['./painel-admin-area-atuacao.component.scss']
})
export class StatusRenderComponent implements ViewCell, OnInit {

  renderValue: string;
  styleFromParent;

  @Input() value: any;
  @Input() rowData: any;

   constructor(private _detectChanges: ChangeDetectorRef) {}

  ngOnInit() {
    setTimeout(() => {

        if(this.value === 0){
          this.value = 'HABILITADA';
          this.renderValue = '<i class="nb-checkmark-circle"></i>';
          this.styleFromParent = { color: 'green', height: '100%', width: '100%'};
        } else if(this.value === 1) {
          this.value = 'DESABILITADA';
          this.renderValue = '<i class="nb-alert"></i>';
          this.styleFromParent = { color: '#F4A460', height: '100%', width: '100%'};
        }

        this._detectChanges.detectChanges();
      }, 100);

    }

  }

