import { AdminAreaAtuacaoComponent } from './admin-area-atuacao/admin-area-atuacao.component';
import { ConfiguracoesComponent } from './configuracoes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: ConfiguracoesComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracoesAdminRoutingModule { }
