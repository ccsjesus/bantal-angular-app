import { StatusRenderComponent } from './status.render.component';
import { LocalDataSource } from 'ng2-smart-table';
import { DadosAdministrativo } from './../../../models/dados-administrativo.model';
import { FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { TipoVagaServices } from './../../../../shared/services/tipo.vaga.services';
import { Component, OnInit, Injector } from '@angular/core';

@Component({
  selector: 'app-vagas-admin',
  templateUrl: './vagas.admin.component.html',
  styleUrls: ['./vagas.admin.component.scss']
})
export class VagasAdminComponent implements OnInit {
settings = {
  //hideSubHeader: true,
  actions: {
    custom: [
      {
        name: 'editAction',
        title: '<i class="icon ion-checkmark-circled" title="Habilitar"></i>'
      },
      {
        name: 'deleteAction',
        title: '<i class="icon ion-close-circled" style="color:red" title="Desabilitar"></i>'
      },

    ],
    add: false,
    edit: false,
    delete: false
  },
  columns: {
      id: {
        title: 'Identificação',
        type: 'number',
      },
      nome: {
        title: 'Tipo Vaga',
        type: 'string'
      },
      habilitada: {
        title: 'Status',
        type: 'custom',
        renderComponent: StatusRenderComponent,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private tipoVagaServices: TipoVagaServices,
              protected router: Router,
               protected injector: Injector,
               private fb: FormBuilder) {

    //super(injector, new DadosAdministrativo(), null, DadosAdministrativo.fromJson);
    tipoVagaServices.getTipoVaga().subscribe(data => {
      this.source.load(data);
    });
  }
   customFunction(event): void {
     if(event.action === 'deleteAction') {
      this.abrirDeleteDialog(event);
     } else {
      this.abrirConfirmacaoDialog(event);
     }
  }

  abrirConfirmacaoDialog(data){

        const vaga = data.data;
        const tituloVaga = '';

        if(vaga.habilitada === 0){

          Swal.fire(
              'Ops!',
              'Tipo Vaga HABILITADAS não podem ser Habilitadas!',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Habilitar Tipo de Vaga ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Tipo da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.nome +
                  '</div>',

            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Habilitar ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.habilitar(vaga.id);
          }

        })
        }
  }

  abrirDeleteDialog(data) {

        const vaga = data.data;
        const tituloVaga = '';

        if(vaga.habilitada === 1){

          Swal.fire(
              'Ops!',
              'Tipo Vagas DESABILITADAS não podem ser Desabilitadas! ',
              'error'
            ).then((resultado) => {
            });
        } else {

            Swal.fire({
            title: 'Desabilitar Tipo Vaga ?',
            html: '<div class="row" style="align-items: center;justify-content: center;">'+
                  //'<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                  '</div>'+
                  '<div class="row">'+
                  ' <label><b>Tipo da Vaga:</b></label>'+
                  ' &nbsp ' + vaga.nome+
                  '</div>',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: 'Desabilitar ',
            cancelButtonColor: '#d33',
            cancelButtonText:' Não, tem algo errado ',
            cancelButtonAriaLabel: 'Thumbs down'

        }).then((result) => {
          if (result.value) {
            this.desabilitar(vaga.id);
          }

        })
        }
  }

  habilitar(codigoVaga) {

      this.tipoVagaServices.habilitarTipoVaga(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Tipo Vaga foi habilitado com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-vagas']);
          });

        })
      });
  }

  desabilitar(codigoVaga) {

      this.tipoVagaServices.desabilitarTipoVaga(codigoVaga).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Tipo Vaga foi desabilitada com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-vagas']);
          });

        })
      });

  }

ngOnInit(): void {
  }

}
