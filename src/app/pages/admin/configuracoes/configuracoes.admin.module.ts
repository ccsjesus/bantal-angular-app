import { ConfiguracoesAdminRoutingModule } from './configuracoes-admin-routing.module';
import { VagasAdminComponent } from './vagas-admin/vagas.admin.component';
import { ConfiguracoesComponent } from './configuracoes.component';
import { SharedModule } from './../../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ConfiguracoesAdminRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule

  ],
  declarations: [
    ConfiguracoesComponent,
    VagasAdminComponent
  ],
})
export class ConfiguracoesAdminModule { }
