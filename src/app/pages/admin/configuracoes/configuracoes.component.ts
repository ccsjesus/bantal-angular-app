import Swal from 'sweetalert2';
import { NAO_HABILITADO, HABILITADO } from './../../constantes';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TipoVagaModel } from './../../models/tipo.vaga.model';
import { TipoVagaServices } from './../../../shared/services/tipo.vaga.services';
import { Component, OnInit, Injector } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';


@Component({
  selector: 'configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.scss']
})
export class ConfiguracoesComponent extends BaseResourceFormComponent<TipoVagaModel> implements OnInit {
    tipoVagaDisponiveis: TipoVagaModel[];

    tipoVagaDesabilitados: TipoVagaModel[];

    tipoVagaSelecionados: TipoVagaModel[];

    draggedTipoVaga: TipoVagaModel;

    formDadosTipoVaga: FormGroup;


    constructor(private tipoVagaServices: TipoVagaServices,
               protected injector: Injector,
               private fb: FormBuilder) {
      super(injector, new TipoVagaModel(), null, TipoVagaModel.fromJson)
    }

    ngOnInit() {
      this.tipoVagaSelecionados = [];
      this.tipoVagaServices.getTipoVaga().subscribe(data => {
        this.tipoVagaDisponiveis = data;
      });

      this.tipoVagaServices.getTipoVagaDesabilitadas().subscribe(data => {
        this.tipoVagaDesabilitados = data;
      });

      this.formDadosTipoVaga = this.fb.group({
        nome: ['', ],
        valor: ['', ],
        habilitada: ['', ]
    });
    }

    dragStart(event,car: TipoVagaModel) {
        this.draggedTipoVaga = car;
    }

    dragEnd(event) {
        this.draggedTipoVaga = null;
    }

    findIndex(car: TipoVagaModel) {
        let index = -1;
        for(let i = 0; i < this.tipoVagaDisponiveis.length; i++) {
            if (car.nome === this.tipoVagaDisponiveis[i].nome) {
                index = i;
                break;
            }
        }
        return index;
    }

    drop(event: CdkDragDrop<string[]>) {

      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        transferArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }
    }

    incluirTipoVaga() {

      const resource: TipoVagaModel = new TipoVagaModel()
      resource.nome = this.formDadosTipoVaga.controls.nome.value;
      resource.valor = this.formDadosTipoVaga.controls.nome.value;
      resource.habilitada = NAO_HABILITADO;


      this.tipoVagaServices.incluirTipoVaga(resource).subscribe(data => {
        Swal.fire(
          'Ótimo!',
          'Tipo Vaga foi incluída com sucesso!',
          'success'
        ).then((resultado) => {
          //this.source.load(resultado.listaVagas);
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['./admin-vagas']);
          });

        })
      });

  }

  ngOnDestroy(): void {}

  protected buildResourceForm() {}

  protected setCurrentAction() {

  }
  protected onLoadConfigurations(): void {

  }

}
