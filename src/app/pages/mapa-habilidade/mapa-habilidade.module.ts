import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapaHabilidadeComponent } from './mapa-habilidade.component';


@NgModule({
  declarations: [MapaHabilidadeComponent],
  imports: [
    CommonModule
  ]
})
export class MapaHabilidadeModule { }
