import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NbStepperComponent } from '@nebular/theme';

@Component({
  selector: 'vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.scss']
})
export class VagasComponent implements OnInit {
  firstForm: FormGroup;
  secondForm: FormGroup;
  thirdForm: FormGroup;
  @ViewChild('stepper', {static: true}) stepper: NbStepperComponent;

  cargos = [
    {
    "title":"Vendedor",
    "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "img":"Stock/pexels-4.jpeg"
    },
    {
      "title":"Advogado",
      "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "img":"Stock/pexels-4.jpeg"
    },
    {
      "title":"Gerente",
      "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "img":"Stock/pexels-4.jpeg"
    }
    ,
    {
      "title":"Engenheiro",
      "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "img":"Stock/pexels-4.jpeg"
    }
  ];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.firstForm = this.fb.group({
      cargo: ['', Validators.required],
      cargos: this.buildCargos()
    });


    this.secondForm = this.fb.group({
      candidato: ['', Validators.required],
    });

    this.thirdForm = this.fb.group({
      thirdCtrl: ['', Validators.required],
    });

  }

  buildCargos(){
    const values = this.cargos.map(v => new FormControl(false));
    return this.fb.array(values);

  }

  onFirstSubmit() {

    if (this.firstForm.valid) {
      this.stepper.next();
    }
    this.firstForm.markAsDirty();
  }

  onSecondSubmit() {
    if (this.secondForm.valid) {
      this.stepper.next();
    }
    this.secondForm.markAsDirty();
  }

  onThirdSubmit() {
    this.thirdForm.markAsDirty();
  }

}
