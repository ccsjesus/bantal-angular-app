import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";

import { VagasFormComponent } from './vagas-form/vagas-form.component';
import { VagasComponent } from './vagas.component';
import { RouterModule } from '@angular/router';
import { VagasRoutingModule } from './vaga-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { IMaskModule } from "angular-imask";

@NgModule({
  imports: [    
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    VagasRoutingModule,
    IMaskModule
  ],
  declarations: [
    VagasFormComponent,
    VagasComponent    
  ],
  providers: [],
})

export class VagasModule {
}
