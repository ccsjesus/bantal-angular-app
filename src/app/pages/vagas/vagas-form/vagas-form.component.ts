import { UsuarioRequest } from './../../models/usuario.pass';
import { DadosServices } from './../../../shared/services/dados.service';
import { noImage } from './../../constantes/bantal_b64';
import { TipoVagaModel } from './../../models/tipo.vaga.model';
import { AreaAtuacaoModel } from './../../models/area.atuacao.model';
import { TipoVagaServices } from './../../../shared/services/tipo.vaga.services';
import { AreaAtuacaoServices } from './../../../shared/services/area.atuacao.services';
import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
import { Validators, FormGroup } from '@angular/forms';
import { EstadoServices } from '../../../shared/services/estado.services';
import { TaxonomiaServices } from '../../../shared/services/taxonomia.services';
import { Taxonomia } from '../../../shared/models/taxonomia.model';
import { VagaServices } from '../../../shared/services/vaga.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { PublicacaoVaga } from '../../models/publicacao.vaga.model';
import { Cidade } from '../../../shared/models/cidades.model';
import { EstadosBr } from '../../../shared/models/estadosbr.model';
import { empty, Observable } from 'rxjs';
import { Data } from '../../../shared/models/data.model';
import { map, switchMap, tap } from 'rxjs/operators';
import { SalarioServices } from '../../../shared/services/salario.services';
import { FuncaoCargoServices } from '../../../shared/services/funcao-cargo.services';
import { Profissao } from '../../models/profissao.model';


@Component({
  selector: 'app-vagas-form',
  templateUrl: './vagas-form.component.html',
  styleUrls: ['./vagas-form.component.scss']
})
export class VagasFormComponent extends BaseResourceFormComponent<PublicacaoVaga> implements OnInit, OnDestroy {

  categories: Array<any>;
  cidades: Cidade[];
  areaAtuacao: AreaAtuacaoModel[];
  // cidades$: Observable<Cidade[]>;
  estados: EstadosBr[];
  cidadeServices: Array<Cidade>;
  estados$: Observable<EstadosBr[]>;
  areaAtuacaoServices$: Observable<AreaAtuacaoModel[]>;
  profissoes$: Observable<Profissao[]>;
  tipovagaServices$: Observable<TipoVagaModel[]>;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  tituloVaga: String = "Anunciar Vaga";
  novaPublicacao: boolean = true;
  storage: any = null;
  foto: any = null;
  confidencial: boolean = false;
  semImagem: any = noImage;
  logomarca:any;
  dadosEmpresa: UsuarioRequest;
  profissoes: Profissao[];


  optionsSalario = [];

  imaskConfig = {
    mask: Number,
    scale: 2,
    thousandsSeparator: '',
    padFractionalZeros: true,
    normalizeZeros: true,
    radix: ','
  };

  ptBR = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: [
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  }

  constructor(
    protected tipoVagaServices: TipoVagaServices,
    protected areaAtuacaoServices: AreaAtuacaoServices,
    protected funcaoCargoServices: FuncaoCargoServices,
    protected injector: Injector,
    private estadoService: EstadoServices,
    protected taxonomiaServices: TaxonomiaServices,
    protected vagasSevices: VagaServices,
    protected router: Router,
    private data: Data,
    protected salarioServices: SalarioServices,
    protected dadosServices: DadosServices
  ) {
    super(injector, new PublicacaoVaga(), null, PublicacaoVaga.fromJson)
  }

  ngOnInit() {
    super.ngOnInit();
    this.loadEstados();
    this.loadStorage();
    this.loadAreaAtuacao();
    this.loadTipoVaga();
    this.registrarChangeEstado();
    this.loadTodosSalarios();
    this.loadDadosEmpresa();
  }

  handleChange(e){
    this.confidencial = e.checked;
    this.resourceForm.controls.confidencial.setValue(this.confidencial);

  }

  private loadTodosSalarios() {
    this.salarioServices.getTodosSalarios().subscribe(valor => {
      valor.forEach(hab => {
        this.optionsSalario.push({label: hab.name, value: hab.name})

      });
    });
  }

  get typeOptions(): Array<any> {
    return [];
  }

  ngOnDestroy(): void {
    this.data.storage = null;
  }

  protected buildResourceForm() {
    if (this.data.storage && this.data.storage.vaga) {
        this.storage = this.data.storage.vaga;
    }
    this.resourceForm = this.formBuilder.group({
      idVaga: [null],
      postContent: [null, [Validators.required, Validators.minLength(2)]],
      titulo: [null],
      estado: [null, [Validators.required]],
      cidade: [null, [Validators.required]],
      salario: [null, [Validators.required]],
      CNPJ: [true, [Validators.required]],
      idAreaAtuacao: [null, [Validators.required]],
      idProfissao: [null, [Validators.required]],
      dataExpiracao: [null, [Validators.required]],
      idTipoVaga: [null, [Validators.required]],
      //email: [null, [Validators.required]],
      nomeTipoVaga: [null],
      localizacao: [null],
      foto: [null],
      user: [],
      confidencial: []
    });
    this.resourceForm.controls.cidade.setValue(-1);
    this.resourceForm.controls.salario.setValue(-1);

  }

  private loadStorage() {


    if (this.storage) {
      this.novaPublicacao = false;
      this.resourceForm.controls.idVaga.setValue(this.storage.id);
      this.tituloVaga = this.storage.id + '-' + this.storage.titulo;
      this.foto = this.storage.foto;
      this.resourceForm.controls.foto.setValue(this.logomarca);
      this.resourceForm.controls.salario.setValue(this.storage.salario);
      this.resourceForm.controls.dataExpiracao.setValue(this.storage.dataLimite);
      this.resourceForm.controls.foto.setValue(this.storage.logomarca);

      // this.resourceForm.get('estado').setValue(storage.estado);
      this.resourceForm.patchValue({
        titulo: this.storage.titulo,
        postContent: this.storage.descricao
      })
    }

  }

  private loadEstados() {
    this.estados$ = this.estadoService.getEstadosBr();
    this.resourceForm.controls.estado.setValue(-1);

    this.registrarChangeEstado();

    if (this.storage) {
      const ref = this.storage.localizacao.split(' - ')[1];
      this.estados$.subscribe(valor => {
        this.estados = valor;
        this.resourceForm.controls.estado.setValue(ref);
        this.resourceForm.controls.estado.markAsDirty();

      });
    } else {
      this.estados$.subscribe(valor => this.estados = valor);
    }
  }

  private loadAreaAtuacao() {
    this.areaAtuacaoServices$ = this.areaAtuacaoServices.getTodasAreaAtuacao();
    this.resourceForm.controls.idAreaAtuacao.setValue(-1);

    this.registrarChangeAreaAtuacao();

    if (this.storage) {
      const ref = this.storage.areaAtuacaoModel.termId + ',' + this.storage.areaAtuacaoModel.slug;
      this.areaAtuacaoServices$.subscribe(valor => {
        this.areaAtuacao = valor;
        this.resourceForm.controls.idAreaAtuacao.setValue(ref);
         this.resourceForm.controls.idAreaAtuacao.markAsDirty();
      });
    } else {
      this.areaAtuacaoServices$.subscribe(valor => this.areaAtuacao = valor);
    }
  }

  private registrarChangeAreaAtuacao() { 
    this.resourceForm.get('idAreaAtuacao').valueChanges
      .pipe(
        map(area => this.areaAtuacao.filter(a => a.termId === area.split(',')[0])), 
        map(areas => areas.length > 0 ? Number(areas[0].termId) : empty()),
        switchMap((idAreaAtuacao: number) => this.funcaoCargoServices.recuperarProfissoesPorIdAreaAtuacao(idAreaAtuacao))
      ).subscribe(profissoes => {
        this.profissoes = profissoes;

        if (this.storage) {
          this.resourceForm.controls.profissao.markAsDirty();
        }
      });
    
  }

  registrarChangeEstado() {
    this.resourceForm.get('estado').valueChanges
      .pipe(
        map(estado => this.estados.filter(e => e.sigla === estado)),
        map(estados => estados.length > 0 ? estados[0].id : empty()),
        switchMap((idEstado: number) => this.estadoService.getCidades(idEstado))
      ).subscribe(cidades => {
        this.cidades = cidades;

        if (this.storage) {
          const ref = this.storage.localizacao.split(' - ')[0];
          this.resourceForm.controls.cidade.setValue(ref);
          this.resourceForm.controls.cidade.markAsDirty();
        }
      });
  }

  private loadDadosEmpresa() {

    this.dadosServices.obterDadosUsuario().subscribe(valor => {
      const foto = valor.foto;
      if(foto === null){
        this.resourceForm.controls.foto.setValue(this.semImagem);
      } else {
        this.resourceForm.controls.foto.setValue(foto);
      }
    });
  }

  private loadTipoVaga() {
    this.tipovagaServices$ = this.tipoVagaServices.getTipoVagaHabilitadas();
    this.resourceForm.controls.idTipoVaga.setValue(-1);

    if (this.storage) {
      const refTipoVaga = this.storage.tipoVagaModel.id + ',' + this.storage.tipoVagaModel.valor;
      this.tipovagaServices$.subscribe(valor => {
        this.resourceForm.controls.idTipoVaga.setValue(refTipoVaga);
        this.resourceForm.controls.idTipoVaga.markAsDirty();
      });
    }

  }

  protected creationPageTitle(): string {
    return "";
  }

  protected editionPageTitle(): string {
    const resourceName = "";
    return " " + resourceName;
  }

  protected onLoadConfigurations(): void {

  }

  protected setCurrentAction() {
    this.currentAction = "create";
  }

  publicarVaga() {

    if (this.resourceForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else if (this.novaPublicacao) {

      const resource: PublicacaoVaga = this.jsonDataToResourceFn(this.resourceForm.value);

      this.vagasSevices.publicarVaga(resource).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Sua vaga foi publicada com sucesso!',
          'success'
        ).then((resultado) => {
          this.router.navigate(['./recrutamento']);
        })

      })
    }
  }

  updatePublicacao() {
    const estadoAtual = this.resourceForm.value.estado;

    if (estadoAtual !== this.storage.estado) {
      this.resourceForm.value.status = 'INVALID';
    }

    if (this.resourceForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {
      const resource: PublicacaoVaga = this.jsonDataToResourceFn(this.resourceForm.value);

      this.vagasSevices.atualizarVaga(resource).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Sua vaga foi atualizada com sucesso!',
          'success'
        ).then((resultado) => {
          this.router.navigate(['./recrutamento']);
        })

      })
    }
  }

  abrirConfirmacaoDialog(){
    if (this.resourceForm.status === 'INVALID') {

      Swal.fire({
        type: 'error',
        title: 'Informações não preenchidas...',
        text: 'Preencha as informações do formulário'
      });
    } else {
        const split = this.resourceForm.value.idTipoVaga.split(',');
        const splitAreaAtuacao = this.resourceForm.value.idAreaAtuacao.split(',');
        this.resourceForm.controls.idTipoVaga.setValue(split[0]);
        this.resourceForm.controls.nomeTipoVaga.setValue(split[1]);

        this.resourceForm.controls.idAreaAtuacao.setValue(splitAreaAtuacao[0]);
        this.resourceForm.controls.localizacao.setValue(this.resourceForm.value.cidade + ' - ' + this.resourceForm.value.estado);
        const estadoAtual = this.estadoService.obterEstadoFromSigla(this.resourceForm.value.estado);

        var tituloVagaPublicacao = "";
        if (this.novaPublicacao) {
          tituloVagaPublicacao = 'Vamos publicar esta vaga?';
        } else {
          tituloVagaPublicacao = 'Vamos atualizar esta vaga?';
        }

        var confidencialParam = this.resourceForm.value.confidencial;
        var foto = this.resourceForm.value.foto;
        if(confidencialParam){
          foto = this.semImagem;
        }
        this.resourceForm.controls.foto.setValue(foto);


        Swal.fire({
          title: tituloVagaPublicacao,
          html: '<div class="row" style="align-items: center;justify-content: center;">'+
                '<img src="'+ 'data:image/jpg;base64,' + foto +'" style="width:7rem; margin-top: 1em; margin-bottom: 1em; margin-right: 1em; -webkit-border-radius: 50%; -moz-border-radius: 50%;border-radius: 50%; -webkit-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); -moz-box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2); box-shadow: 0 0 0 3px #fff, 0 0 0 4px #999, 0 2px 5px 4px rgba(0,0,0,.2);">'+
                '</div>'+
                '<div class="row">'+
                ' <label><b>Título da Vaga:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.titulo +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Estado:</b></label>'+
                ' &nbsp ' + estadoAtual +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Cidade:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.cidade +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Tipo da Vaga:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.nomeTipoVaga +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Área de Atuação:</b></label>'+
                ' &nbsp ' + splitAreaAtuacao[1] +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Descrição:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.postContent +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Data Limite:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.dataExpiracao +
                '</div>'+
                '<div class="row">'+
                ' <label><b>Salário:</b></label>'+
                ' &nbsp ' + this.resourceForm.value.salario +
                '</div>',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText: 'Publicar ',
          cancelButtonColor: '#d33',
          cancelButtonText:' Não, tem algo errado ',
          cancelButtonAriaLabel: 'Thumbs down'

      }).then((result) => {
        if (result.value) {
          if (this.novaPublicacao) {
            this.publicarVaga();
          } else {
            this.updatePublicacao();
          }
        }
      })
    }
  }

  voltar() {
    this.router.navigate(['./recrutamento']);
  }

  urltoFile(dataurl, filename){
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
  }
}

