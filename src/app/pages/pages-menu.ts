import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_EMPRESA: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'home',
    link: '/empresa',
  },
  {
    title: 'Anunciar Vaga',
    icon: 'people-outline',
    link: '/vaga',
  },
  {
    title: 'Recrutamento',
    icon: 'edit-outline',
    link: '/recrutamento'

  },
  {
    title: 'Sobre a Empresa',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Dados',
        icon: 'file-text-outline',
        link: '/dados',
      },
      {
        title: 'Planos',
        icon: 'file-text-outline',
        link: '/planos-empresa',
      },
      {
        title: 'Suporte',
        icon: 'question-mark-circle-outline',
        link: '/suporte',
      }
    ],
  },
];

export const MENU_ITEMS_CANDIDATO: NbMenuItem[] = [
{
    title: 'Inicio',
    icon: 'home',
    link: '/selecao',
  },
  {
    title: 'Seleções / Vagas',
    icon: 'edit-outline',
    link: '/selecao'

  },
  {
    title: 'Curriculo',
    icon: 'people-outline',
    link: '/curriculo',
  },
  {
    title: 'Informações',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Dados',
        icon: 'file-text-outline',
        link: '/informacoes',
      },
      /*{
        title: 'Planos',
        icon: 'file-text-outline',
        link: '/planos',
      },*/
      {
        title: 'Suporte',
        icon: 'question-mark-circle-outline',
        link: '/suporte-candidato',
      }
    ],
  },
];

export const MENU_ITEMS_ADMINISTRADOR: NbMenuItem[] = [
{
    title: 'Inicio',
    icon: 'home',
    link: '/admin',
  },
  {
    title: 'Configurações',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Tipo Vagas',
        icon: 'file-text-outline',
        link: '/admin-vagas',
      },
      {
        title: 'Área de Atuação',
        icon: 'file-text-outline',
        link: '/admin-area-atuacao',
      }
    ],
  },
];
