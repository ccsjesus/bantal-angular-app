  import { FormBuilder, FormGroup } from '@angular/forms';
  import { Router } from '@angular/router';
  import { MensagemModel } from './../../models/mensagem';
  import { MensagemFromRecruiterService } from './../../../shared/services/mensagem-from-recruiter.services';
  import Swal from 'sweetalert2';
  import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
  import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';


  @Component({
    selector: 'app-message-recrutador',
    templateUrl: './message-recrutador.component.html',
    styleUrls: ['./message-recrutador.component.css']
  })
  export class MessagesRecrutadorComponent extends BaseResourceFormComponent<MensagemModel> implements OnInit, OnDestroy {

    text: string;
    nomeProcessoSeletivo = '';
    usuario: string;
    formDadosMensagem: FormGroup;
    listaMensagem: MensagemModel[];
    codigoVaga: string;
    possuiMensagem: boolean = false;
    textoCarregando: string = 'Carregando novas mensagens!';
    exibirLoading: Boolean = true;
    textoAguarde: string = 'Aguarde!';
    qtdMsgs: number = 0;


    constructor(protected router: Router,
                protected injector: Injector,
                private fb: FormBuilder,
                protected msgFromRecruiter: MensagemFromRecruiterService
      ) {
      super(injector, new MensagemModel(), null, MensagemModel.fromJson);

      let objSessao = sessionStorage.getItem('dadosSala');
      let objListaMsg = sessionStorage.getItem('listaMsg');

      if(objSessao){
        var dadosSala = JSON.parse(objSessao);
        this.nomeProcessoSeletivo = dadosSala.codigoselecao;
        this.usuario = localStorage.getItem('nome');

        this.codigoVaga = dadosSala.codigoselecao.split(" ")[0];
        let msgModel = new MensagemModel();
        msgModel.idVaga = this.codigoVaga;
        msgModel.codigoAutorMensagem = dadosSala.idSala;
        this.msgFromRecruiter.getMensagensCandidatoEmpresa(msgModel).subscribe(
          dados => {
            if(dados.length > 0) {
              this.possuiMensagem = true;
              this.listaMensagem = dados;
              this.atualizarStatusMsg(JSON.parse(objListaMsg));

            } else {
              this.exibirLoading = false;
              this.textoCarregando = 'Não há mensagens disponíveis!';
              this.textoAguarde = 'Em breve voce receberá alguma notificação.';
            }
            this.qtdMsgs = dados.length;
          }
        );
      }
    }

  ngOnInit(): void {
      this.formDadosMensagem = this.fb.group({
        mensagem: [''],
      });
  }

  atualizarStatusMsg(listaMsg){

      this.msgFromRecruiter.atualizarSitucaoMsgLida(listaMsg).subscribe(dados => {
      })
    }

    ngOnDestroy(): void {}

    protected buildResourceForm() {}

    protected setCurrentAction() {

    }
    protected onLoadConfigurations(): void {

    }

    enviarMensagem(): void {
      let objSessao = sessionStorage.getItem('dadosSala');
      var dadosSala = JSON.parse(objSessao);
      
      const resource: MensagemModel = new MensagemModel();
      resource.mensagem = this.formDadosMensagem.controls.mensagem.value;
      resource.tipoMensagem = 2;
      resource.idVaga = this.codigoVaga;
      if (dadosSala){
        resource.codigoReferenciaMensagem = dadosSala.idSala;
      }
      // resource.codigoAutorMensagem =
      this.msgFromRecruiter.inserirMensagem(resource).subscribe(res => {
        Swal.fire(
          'Ótimo!',
          'Sua Mensagem foi enviada com sucesso !',
          'success'
        ).then((resultado) => {
            this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate(['/messagem-empresa']);
            });
        },
        (error) => {
          console.log('Erro')
        })
      })
      }
    }
