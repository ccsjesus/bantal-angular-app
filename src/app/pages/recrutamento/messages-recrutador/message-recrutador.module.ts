import { MessagesRecrutadorRoutingModule } from './message-recrutador.routing.module';
import { MessagesRecrutadorComponent } from './message-recrutador.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule,
    MessagesRecrutadorRoutingModule,
    ReactiveFormsModule,
    FormsModule

  ],
  declarations: [
    MessagesRecrutadorComponent
  ],
})
export class MessagesRecrutadorModule { }
