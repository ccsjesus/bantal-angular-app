import { MessagesRecrutadorComponent } from './message-recrutador.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: MessagesRecrutadorComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MessagesRecrutadorRoutingModule { }
