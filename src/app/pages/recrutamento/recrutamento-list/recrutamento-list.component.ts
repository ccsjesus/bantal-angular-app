      import { Component, OnInit } from '@angular/core';
      import { Router } from '@angular/router';
      import { VagaServices } from '../../../shared/services/vaga.service';
      import { Vaga } from '../../models/vaga.model';
      import { MessageService } from '../../../shared/services/message';
      import { Data } from '../../../shared/models/data.model';
      import Swal from 'sweetalert2';
      import { map, catchError, switchMap } from 'rxjs/operators';
      import { MensagemFromRecruiterService } from './../../../shared/services/mensagem-from-recruiter.services';
      import { forkJoin, fromEvent, of } from "rxjs";


      @Component({
        selector: 'app-recrutamento-list',
        templateUrl: './recrutamento-list.component.html',
        styleUrls: ['./recrutamento-list.component.scss']
      })
      export class RecrutamentoListComponent implements OnInit {

        dados: Vaga[] = [{ "vagas": [] }];

        cols: any[];

        possuiVaga: Boolean = false;

        textoCarregando: string = 'Carregando suas informações!'

        textoAguarde: string = 'Aguarde...'

        exibirLoading: Boolean = true;

        mapVagas: any = [];

        constructor(private vagasSevices: VagaServices,
                    private messageService: MessageService,
                    private router: Router,
                    private data: Data,
                    protected msgFromRecruiter: MensagemFromRecruiterService) { }

        // ngOnInit() {
        //   this.vagasSevices.getCandidatos().subscribe(valor => this.dados = valor,
        //     error => this.messageService.add({ severity: 'info', summary: 'Usuario Selecionado', detail: 'Vin: ' + error }));

        //   this.cols = [
        //     { field: 'codigo', header: 'Nome da vaga' },
        //     { field: 'titulo' },
        //     { field: 'qtd_inscritos', header: 'Inscritos' },
        //     { field: 'situacao' }
        //   ];
        // }

        carregarBadgeMsgNaoLida(listaVagas, chaveVaga) {
          setTimeout( ()=> {
            listaVagas.forEach(element => {
              if(element.cdVaga === chaveVaga){
                return element.mensagens.length;
              }
            });
          }, 1000 )
        }

        ngOnInit(): void {

          this.cols = [
            { field: 'nome', header: 'Nome da vaga' },
            { field: 'inscritos', header: 'Inscritos' },
            { field: 'mensagens', header: 'Mensagens dos Candidatos' },
            { field: 'candidatos', header: 'Visualizar Candidatos' },
            { field: 'situacao', header: 'Situação da Vaga' },
            { field: 'opcoes', header: 'Opções' }
          ];

          let listaCdVagas = [];
          let listaVagas;
         this.vagasSevices.getCandidatos().subscribe(valor => {
            if(valor !== undefined) {
              this.possuiVaga = true;
              this.dados = valor;
            } else {
              this.exibirLoading = false;
              this.textoCarregando = 'Não vagas publicadas !';
              this.textoAguarde = 'Realize o cadastro de novas vagas.';
            }
          });

        }

        selecionarCandidatos(user: any, titulo: string, codigo: string) {

          if (user.length == 0) {
            Swal.fire({
              type: 'error',
              title: 'Não há candidatos inscritos',
              text: 'Não há candidatos inscritos para a vaga ' + codigo + ' - ' + titulo
            });
          } else {
            const usuario = {
              "user": user,
              "titulo": codigo + ' - ' + titulo
            }



            this.data.storage = usuario;

            this.router.navigate(['./recrutamento/inscritos']);
          }
        }

        async exibirDadosVaga(titulo: string, codigo: string) {


          this.vagasSevices.getDetalheVaga(codigo).subscribe(
            (res) => {
              const vaga = { "vaga": res };

              this.data.storage = vaga;
              this.router.navigate(['./vaga']);

            }
          );
        }

        obterString(val) {
          return (val + "").replace(/<[^>]*>?/gm, '');;
        }

      }
