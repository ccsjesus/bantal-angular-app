import { Component, OnInit, Input, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Candidato } from '../../models/candidato.model';
import { Vaga } from '../../models/vaga.model';
import { Data } from '../../../shared/models/data.model';
import { of as observableOf, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { RelatorioServices } from '../../../shared/services/relatorio.services';
import { MessageService } from '../../../shared/services/message';
import { ToastrService, GlobalConfig } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { MensagemFromRecruiterService } from './../../../shared/services/mensagem-from-recruiter.services';


@Component({
  selector: 'app-inscritos-table',
  templateUrl: './inscritos-table.component.html',
  styleUrls: ['./inscritos-table.component.scss']
})

export class InscritosTableComponent implements OnInit {

  @Input() parentForm: FormGroup;

  selectedValues: string[] = [];

  dados: Vaga[];

  titulo: String;

  cols: any[];

  msgs: string;

  @Input() candidatos: any;

  options: GlobalConfig;

@ViewChildren('nbAction') actionIcon:QueryList<any>;

vagas: Vaga[];

myObj: any;

mapListaMsg: any = [];



  constructor(private toastr: ToastrService,
              private messageService: MessageService,
              private data: Data,
              private route: Router,
              private relatorioServices: RelatorioServices,
              protected msgFromRecruiter: MensagemFromRecruiterService) {

    this.options = this.toastr.toastrConfig;
  }



  ngOnInit() {
    this.myObj = this.data.storage;
    this.getMsgNaoLidasPorUser();
    if (this.myObj === undefined) {
      this.route.navigate(['./recrutamento']);
    }

    this.getDados().subscribe(valor => this.dados = valor,
      error => this.messageService.add({ severity: 'info', summary: 'Usuario Selecionado', detail: 'Vin: ' + error }));

    this.getTitulo().subscribe(valor => this.titulo = valor,
      error => this.messageService.add({ severity: 'info', summary: 'Usuario Selecionado', detail: 'Vin: ' + error }));


    this.cols = [
      { field: 'candidato', header: 'Nome da vaga' },
      { field: 'cpf' }
    ];
  }

  getMsgNaoLidasPorUser(){

    var cdVaga = parseInt(this.data.storage.titulo.split(' - ')[0]);
    const listaIdUsers = this.data.storage.user.map(users => users.idUsuario);
    let map = new Map();
    map.set(cdVaga, listaIdUsers);
    this.msgFromRecruiter.getMensagemNaoLidaUsuarios(map).subscribe(valor => {
      this.carregarBadgeMsgNaoLida(valor);
    });
  }

  armazenarListaMensagens(tituloVaga){
    var chaveVaga = parseInt(tituloVaga);
    let lista = this.mapListaMsg.get(chaveVaga);

    sessionStorage.setItem('listaMsg', JSON.stringify(lista));
  }

  carregarBadgeMsgNaoLida(listaVagas) {
    setTimeout( ()=> {
      var instancia = this;
      this.actionIcon.toArray().forEach(function(value, key) {
        var chaveVaga = parseInt(value.title.split('__')[1]);
        instancia.mapListaMsg = new Map(Object.keys(listaVagas).map(key => [parseInt(key), listaVagas[key]]));
        let lista = instancia.mapListaMsg.get(chaveVaga);
        value.badgeText = lista.length === undefined ? 0 : lista.length;
      });
    }, 1000 )

  }

  getDados(): Observable<Candidato[]> {
    if(this.data.storage === undefined ){
      return observableOf(undefined);
    }
    return observableOf(this.data.storage.user);
  }

  getTitulo(): Observable<string> {
    if(this.data.storage === undefined ){
      return observableOf(undefined);
    }
    return observableOf(this.data.storage.titulo);
  }

  onRowSelect(event) {
    if (this.selectedValues.length === 0) {
      var result = [];
      result.push(event.data);

      if (result.length == 0) {
        Swal.fire({
          type: 'error',
          title: 'Não foi possível gerar seu relatório...',
          text: 'Selecione um registro para gerar o relatório'
        });
      } else {
        this.relatorioServices.gerarRelatorio(result).subscribe(res => {

          Swal.fire(
            'Ótimo!',
            'Seu relatório foi gerado com sucesso click para continuar!',
            'success'

          ).then((result) => {
            const fileURL = URL.createObjectURL(res);
            window.open(fileURL, '_blank');
          })
        });
      }

    }
  }

  gerarPDF() {
    if (this.selectedValues.length == 0) {
      Swal.fire({
        type: 'error',
        title: 'Não foi possível gerar seu relatório...',
        text: 'Selecione um registro para gerar o relatório'
      });
    } else {
      this.relatorioServices.gerarRelatorio(this.selectedValues).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Seu relatório foi gerado com sucesso click para continuar!',
          'success'

        ).then((result) => {
          this.selectedValues = [];
          var file = new Blob([res], {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL, '_blank');
        })
      });
    }
  }

  gerarPDFPorIdUsuario(idUsuario) {

      let array = [idUsuario];
      this.relatorioServices.gerarRelatorio(array).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Seu relatório foi gerado com sucesso click para continuar!',
          'success'

        ).then((result) => {
          this.selectedValues = [];
          var file = new Blob([res], {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL, '_blank');
        })
      });
  }

  redirecionarPainelMensagensEmpresa(cdVagaTituloSelecao: string, idUsuario: string){
    let dadosSala = this.obterDadosSalaChat(cdVagaTituloSelecao, idUsuario);
    sessionStorage.setItem('dadosSala', JSON.stringify(dadosSala));
    this.armazenarListaMensagens(idUsuario);

    this.route.navigate(['./messagem-empresa']);
    //this.router.navigate(['/mensagem-candidato']);
  }

  obterDadosSalaChat(cdVagaTituloSelecao, idUsuario){
    let dadosSala = {
      codigoselecao: cdVagaTituloSelecao,
      idSala: idUsuario
    }
    return dadosSala;
  }
  formataCPF(cpf){
    cpf = cpf.replace(/[^\d]/g, "");

    return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
  }

}

