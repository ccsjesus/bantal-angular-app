import { MessagesRecrutadorModule } from './messages-recrutador/message-recrutador.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RecrutamentoRoutingModule } from './recrutamento-routing.module';
import { RouterModule } from '@angular/router';
import { RecrutamentoComponent } from './recrutamento.component';
import { SharedModule } from '../../shared/shared.module';
import { InscritosComponent } from './inscritos/inscritos.component';
import { RecrutamentoListComponent } from './recrutamento-list/recrutamento-list.component';
import { InscritosTableComponent } from './inscritos-table/inscritos-table.component';
import { FeedbackComponent } from './feedback/feedback.component';

@NgModule({
  imports: [
    RouterModule,
    RecrutamentoRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    MessagesRecrutadorModule
  ],
  declarations: [ RecrutamentoComponent,
                  InscritosTableComponent,
                  InscritosComponent,
                  RecrutamentoListComponent, FeedbackComponent
                ],
})
export class RecrutamentoModule { }
