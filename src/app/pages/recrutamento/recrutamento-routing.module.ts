import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecrutamentoComponent } from './recrutamento.component';
import { InscritosComponent } from './inscritos/inscritos.component';

const routes: Routes = [
  { path: '', component: RecrutamentoComponent, },
  { path: 'inscritos', component: InscritosComponent }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class RecrutamentoRoutingModule { }
