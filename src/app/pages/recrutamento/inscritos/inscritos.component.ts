import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Vaga } from '../../models/vaga.model';

@Component({
  selector: 'app-inscritos',
  templateUrl: './inscritos.component.html',
  styleUrls: ['./inscritos.component.scss']
})
export class InscritosComponent implements OnInit {

  @Input() parentForm: FormGroup;

  @Input() candidatos: Array<Vaga>;

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
