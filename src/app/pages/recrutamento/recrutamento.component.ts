import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { VagaServices } from '../../shared/services/vaga.service';
import { of as observableOf,Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Vaga } from '../models/vaga.model';
import { MessageService } from '../../shared/services/message';
import { Router } from '@angular/router';

@Component({
  selector: 'recrutamento',
  templateUrl: './recrutamento.component.html',
  styleUrls: ['./recrutamento.component.scss']
})
export class RecrutamentoComponent implements OnInit {


  user: any = {};
  errors: string[] = [];
  messages: string[] = [];
  submitted: boolean = false;
  showMessages: any;
  protected formBuilder: FormBuilder;
  resourceForm: FormGroup;
  dados: any =
    {
      "qtdVagasAbertas": "0",
      "totalInscritos" : "0",
      "vagas": []

    };
  inscricoesVaga: any;
  vagasAbertas: any;

  constructor(protected vagaServices: VagaServices, 
    private toastr: ToastrService, 
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit(): void {

    this.vagaServices.getCandidatos().subscribe(valor => {
     this.dados = valor;
     this.loadChartInscritoVagas();
     this.loadChartVagasAberrtas();
    },
      error => this.messageService.add({severity:'info', summary:'Usuario Selecionado', detail:'Vin: ' + error}));
  }

  protected loadChartInscritoVagas(): void {
    const value = this.dados.totalInscritos;
    this.inscricoesVaga = {
                labels: [ 'Abril', 'Maio', 'Junho'],
                datasets: [
                    {
                        label: 'Inscritos na Vaga',
                        data: [0, 0, value],
                        backgroundColor: '#42A5F5',
                        borderColor: '#4bc0c0'
                    }
                ]
            }
  }

  protected loadChartVagasAberrtas(): void {
    const value = this.dados.qtdVagasAbertas;
    this.vagasAbertas = {
                    labels: [ 'Abril', 'Maio', 'Junho'],
                    datasets: [
                        {
                            label: 'Vagas Abertas',
                            data: [0, 0, value],
                            backgroundColor: '#42A5F5',
                            borderColor: '#4bc0c0'
                        }
                    ]
                }
  }

  visualizarBancoDeTalentos() {
    this.router.navigate(['./banco-talentos']);
  }

  protected buildResourceForm(): void {

  }

  protected onLoadConfigurations(): void {

  }

}
