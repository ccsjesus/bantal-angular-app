import { VagaServices } from './../../../shared/services/vaga.service';
import { Vaga } from './../../models/posts';
import { Component, OnInit, Injector } from '@angular/core';
import { BaseResourceFormComponent } from '../../../shared/components/base-resource-form/base-resource-form.component';
import { DadosServices } from '../../../shared/services/dados.service';


@Component({
  selector: 'app-identificacao-empresa',
  templateUrl: './identificacao-empresa.component.html',
  styleUrls: ['./identificacao-empresa.component.scss']
})
export class IdentificacaoEmpresaComponent extends BaseResourceFormComponent<Vaga> implements OnInit {

  linkPlanos = '/planos-empresa';

  nomePlano: string = '';

  slug: string = '';

  nomeUsuario: string = '';

  textoEmpresa: string = '';

  possuiVaga: Boolean = false;
  textoCarregando: string = 'Carregando suas informações!'
  textoAguarde: string = 'Aguarde!'
  exibirLoading: Boolean = true;

  constructor(protected injector: Injector,
    protected vagasServices: VagaServices,
    protected dadosServices: DadosServices) {

    super(injector, new Vaga(), null, Vaga.fromJson);

	}

  ngOnInit() {
    this.dadosServices.obterDadosUsuario().subscribe((users) => {
      this.nomeUsuario = users.displayName;
      if(users.planoUsuario) {
        this.nomePlano = users.planoUsuario.nomePlano;
        this.slug = users.planoUsuario.slug;
      } else {
        this.nomePlano = 'Plano Inicial';
      }
      this.exibirLoading = false;
      this.possuiVaga = true;
    });//this.user = users);
  }

  protected setCurrentAction() {

  }
  protected buildResourceForm(): void {

  }
  protected onLoadConfigurations(): void {

  }

}
