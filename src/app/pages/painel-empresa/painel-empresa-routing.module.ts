import { IdentificacaoEmpresaComponent } from './identificacao-empresa/identificacao-empresa.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: IdentificacaoEmpresaComponent, },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PainelEmpresaRoutingModule { }
