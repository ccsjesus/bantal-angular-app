import { IdentificacaoEmpresaComponent } from './identificacao-empresa/identificacao-empresa.component';
import { PainelEmpresaRoutingModule } from './painel-empresa-routing.module';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import {ToastModule} from 'primeng/toast';
import {TabViewModule} from 'primeng/tabview';
import {CodeHighlighterModule} from 'primeng/codehighlighter';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    PainelEmpresaRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    CodeHighlighterModule,
    TabViewModule,
    ToastModule
  ],
  declarations: [
   IdentificacaoEmpresaComponent
  ],
})
export class PainelEmpresaModule { }
