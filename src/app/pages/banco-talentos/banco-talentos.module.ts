import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { BancoTalentosRoutingModule } from './banco-talentos-routing.module';
import { DashboardAreaAtuacaoComponent } from './dashboard-area-atuacao/dashboard-area-atuacao.component';
import { ListaProfissoesPorAreaAtuacaoComponent } from './lista-profissoes-por-area-atuacao/lista-profissoes-por-area-atuacao.component';
import { ListaCurriculosPorProfissaoComponent } from './lista-curriculos-por-profissao/lista-curriculos-por-profissao.component';

@NgModule({
  declarations: [DashboardAreaAtuacaoComponent, ListaProfissoesPorAreaAtuacaoComponent, ListaCurriculosPorProfissaoComponent],
  imports: [
    SharedModule,
    CommonModule,
    BancoTalentosRoutingModule 
  ]
})
export class BancoTalentosModule { }
