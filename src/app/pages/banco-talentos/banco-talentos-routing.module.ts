import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardAreaAtuacaoComponent } from './dashboard-area-atuacao/dashboard-area-atuacao.component';
import { ListaProfissoesPorAreaAtuacaoComponent } from './lista-profissoes-por-area-atuacao/lista-profissoes-por-area-atuacao.component';
import { ListaCurriculosPorProfissaoComponent } from './lista-curriculos-por-profissao/lista-curriculos-por-profissao.component';


const routes: Routes = [
  { path: '', component: DashboardAreaAtuacaoComponent,
  },
  {
    path: 'profissoes-por-area-atuacao/:codigo', component: ListaProfissoesPorAreaAtuacaoComponent,
  },
  {
    path: 'curriculos-por-profissao/:codigo', component: ListaCurriculosPorProfissaoComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BancoTalentosRoutingModule { }
