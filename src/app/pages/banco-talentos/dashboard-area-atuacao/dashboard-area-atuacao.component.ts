import { Component, OnInit } from '@angular/core';
import { AreaAtuacaoServices } from '../../../shared/services/area.atuacao.services';
import { AreaAtuacaoDTO } from '../../models/area-atuacao-dto';
import { AreaAtuacaoModel } from '../../models/area.atuacao.model';

@Component({
  selector: 'dashboard-area-atuacao',
  templateUrl: './dashboard-area-atuacao.component.html',
  styleUrls: ['./dashboard-area-atuacao.component.css']
})
export class DashboardAreaAtuacaoComponent implements OnInit {

  listaAreaAtuacao: AreaAtuacaoDTO[];

  constructor(
    private areaAtuacaoServices: AreaAtuacaoServices
  ) { }

  ngOnInit(): void {
    this.loadAreasAtuacao();
  }

  private loadAreasAtuacao() {
    this.areaAtuacaoServices.obterTodasAreasAtuacaoAgrupadaPorCurriculo().subscribe(data => {
      this.listaAreaAtuacao = data;
    });
  }

}
