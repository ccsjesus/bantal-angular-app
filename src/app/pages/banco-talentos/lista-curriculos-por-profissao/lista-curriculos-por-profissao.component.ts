import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { CurriculoServices } from '../../../shared/services/curriculo.services';
import { Curriculo } from '../../models/curriculo.model';
import { error } from 'console';
import { RelatorioServices } from '../../../shared/services/relatorio.services';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import * as XLSX from 'xlsx';

@Component({
  selector: 'lista-curriculos-por-profissao',
  templateUrl: './lista-curriculos-por-profissao.component.html',
  styleUrls: ['./lista-curriculos-por-profissao.component.css']
})
export class ListaCurriculosPorProfissaoComponent implements OnInit {

  curriculos: Curriculo[];

  selectedCurriculos: any[] = [];

  cols: any[];

  mapListaMsg: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private title: Title,
    private curriculoServices: CurriculoServices,
    private relatorioServices: RelatorioServices,
    private location: Location,
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Currículos por Profissões");
    const codigoProfissao = this.route.snapshot.params['codigo'];
    this.carregarProfissoesPorAreaAtuacao(codigoProfissao);


    this.cols = [
      { field: 'candidato', header: 'Nome da vaga' },
      { field: 'cpf' }
    ];

  }

  carregarProfissoesPorAreaAtuacao(codigo: number) {
    this.curriculoServices.recuperarCurriculosPorIdProfissao(codigo).subscribe(curriculo => {
      this.curriculos = curriculo;
    },
   error => {
    console.log('>>>> NAO ENCONTRADO.');
   })
    ;
  }


  goBack(): void {
    this.location.back();
  }

  redirecionarPainelMensagensEmpresa(cdVagaTituloSelecao: string, nome: string){
    let dadosSala = this.obterDadosSalaChat(cdVagaTituloSelecao, nome);
    sessionStorage.setItem('dadosSala', JSON.stringify(dadosSala));
    this.armazenarListaMensagens(nome);

    this.router.navigate(['./messagem-empresa']);

  }



  obterDadosSalaChat(cdVagaTituloSelecao, nome){
    let dadosSala = {
      codigoselecao: cdVagaTituloSelecao,
      idSala: nome
    }
    return dadosSala;
  }

  armazenarListaMensagens(tituloVaga){
    var chaveVaga = parseInt(tituloVaga);
    let lista = this.mapListaMsg.get(chaveVaga);

    sessionStorage.setItem('listaMsg', JSON.stringify(lista));
  }

onRowSelect(event) {
    if (this.selectedCurriculos.length === 0) {
      var result = [];
      result.push(event.data);

      if (result.length == 0) {
        Swal.fire({
          type: 'error',
          title: 'Não foi possível gerar seu relatório...',
          text: 'Selecione um registro para gerar o relatório'
        });
      } else {
        this.relatorioServices.gerarRelatorio(result).subscribe(res => {

          Swal.fire(
            'Ótimo!',
            'Seu relatório foi gerado com sucesso click para continuar!',
            'success'

          ).then((result) => {
            const fileURL = URL.createObjectURL(res);
            window.open(fileURL, '_blank');
          })
        });
      }

    }

  }

  gerarPDF() {
    if (this.selectedCurriculos.length == 0) {
      Swal.fire({
        type: 'error',
        title: 'Não foi possível gerar seu relatório...',
        text: 'Selecione um registro para gerar o relatório'
      });
    } else {
      this.relatorioServices.gerarRelatorio(this.selectedCurriculos).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Seu relatório foi gerado com sucesso click para continuar!',
          'success'

        ).then((result) => {
          this.selectedCurriculos = [];
          var file = new Blob([res], {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL, '_blank');
        })
      });
    }
  }

  gerarPDFPorIdUsuario(nome) {

      let array = [nome];
      this.relatorioServices.gerarRelatorio(array).subscribe(res => {

        Swal.fire(
          'Ótimo!',
          'Seu relatório foi gerado com sucesso click para continuar!',
          'success'

        ).then((result) => {
          this.selectedCurriculos = [];
          var file = new Blob([res], {type: 'application/pdf'});
          const fileURL = URL.createObjectURL(file);
          window.open(fileURL, '_blank');
        })
      });
  }

  exportToExcel() {
    const dataToExport = this.curriculos; // Replace with the data you want to export
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, 'candidatos');
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const url: string = window.URL.createObjectURL(data);
    const link: HTMLAnchorElement = document.createElement('a');
    link.href = url;
    link.download = fileName + '.xlsx';
    link.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(url);
    }, 100);
  }


}
