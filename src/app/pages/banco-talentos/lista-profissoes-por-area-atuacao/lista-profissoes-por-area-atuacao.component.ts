import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { FuncaoCargoServices } from '../../../shared/services/funcao-cargo.services';
import { Profissao } from '../../models/profissao.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'lista-profissoes-por-area-atuacao',
  templateUrl: './lista-profissoes-por-area-atuacao.component.html',
  styleUrls: ['./lista-profissoes-por-area-atuacao.component.css']
})
export class ListaProfissoesPorAreaAtuacaoComponent implements OnInit {

  profissoes: Profissao[];

  constructor(
    private route: ActivatedRoute,
    private title: Title,
    private router: Router,
    private location: Location,
    private funcaoCargoServices: FuncaoCargoServices
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Profissões por área de atuação");
    const codigoAreaAtuacao = this.route.snapshot.params['codigo'];
    this.carregarProfissoesPorAreaAtuacao(codigoAreaAtuacao);
  }

  carregarProfissoesPorAreaAtuacao(codigo: number) {
    this.funcaoCargoServices.recuperarProfissoesPorIdAreaAtuacao(codigo).subscribe(profissao => {
      this.profissoes = profissao;
      if (this.profissoes.length === 0) {
        Swal.fire({
          title: 'Aviso',
          text: 'Não há currículos disponíveis para esta área de atuação no momento.',
          confirmButtonText: 'OK'
        }).then(() => {
          this.router.navigate(['/banco-talentos']);
        });
      }
    });
  }

  goBack(): void {
    this.location.back();
  }
}
