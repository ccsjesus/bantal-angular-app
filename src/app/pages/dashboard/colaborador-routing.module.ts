import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ColaboradorFormComponent } from './colaborador-form/colaborador-form.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'novo', component: ColaboradorFormComponent },
  { path: ':id/edit', component: ColaboradorFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColaboradorRoutingModule {
}
