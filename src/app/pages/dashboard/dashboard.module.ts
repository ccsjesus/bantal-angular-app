import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { ColaboradorFormComponent } from './colaborador-form/colaborador-form.component';
import { ColaboradorDetalheComponent } from './colaborador-detalhe/colaborador-detalhe.component';
import { ColaboradorRoutingModule } from './colaborador-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    RouterModule,
    ColaboradorRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [    
    DashboardComponent,    
    ColaboradorFormComponent,
    ColaboradorDetalheComponent
  ],
})
export class DashboardModule { }
