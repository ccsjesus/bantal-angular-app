import { AdminComponent } from './admin/admin.component';
import { DashboardAdminModule } from './admin/dashboard/dashboard.admin.module';
import { AdminRoutingModule } from './admin/admin-routing.module';
import { AuthGuardAdministrador } from './../guards/auth-admin.guard';
import { SuporteModule } from './suporte/suporte.module';
import { SuporteRoutingModule } from './suporte/suporte-routing.module';
import { MessagesFromRecrutadorModule } from './candidato/messages/message-from-recrutador.module';
import { PainelEmpresaModule } from './painel-empresa/painel-empresa.module';
import { PlanosEmpresaModule } from './planos-empresa/planos-empresa.module';
import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { LoginModule } from './login/login.module';
import { PagesRoutingModule } from './pages-routing.module';
import { LoginService } from './login/login.service';
import { UserService } from '../@core/mock/users.service';
import { CarService } from '../shared/services/carservices';
import { VagasModule } from './vagas/vagas.module';
import { RecrutamentoModule } from './recrutamento/recrutamento.module';
import { SharedModule } from '../shared/shared.module';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DadosModule } from './dados/dados.module';
import { AuthGuardCandidato } from '../guards/auth-candidato.guard';
import { AuthGuardEmpresa } from '../guards/auth-empresa.guard';

import { CandidatoRoutingModule } from './candidato/candidato-routing.module';
import { CandidatoComponent } from './candidato/candidato.component';
import { SelecaoModule } from './candidato/selecao/selecao.module';
import { PainelEmpresaComponent } from './painel-empresa/painel-empresa.component';
import { SuporteComponent } from './suporte/suporte.component';
import { BancoTalentosModule } from './banco-talentos/banco-talentos.module';


@NgModule({
  imports: [
    AdminRoutingModule,
    PagesRoutingModule,
    CandidatoRoutingModule,
    ThemeModule,
    NbMenuModule,
    LoginModule,
    VagasModule,
    RecrutamentoModule,
    SharedModule,
    MessagesModule,
    MessageModule,
    DadosModule,
    SelecaoModule,
    PlanosEmpresaModule,
    PainelEmpresaModule,
    MessagesFromRecrutadorModule,
    SuporteModule,
    DashboardAdminModule,
    BancoTalentosModule,
  ],
  declarations: [
    PagesComponent,
    CandidatoComponent,
    PainelEmpresaComponent,
    AdminComponent
  ],
  providers: [ LoginService, AuthGuardEmpresa, AuthGuardCandidato, AuthGuardAdministrador, UserService, CarService],
})
export class PagesModule {
}
