import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Expiracao extends BaseResourceModel {
    constructor(private valor?: number,
                private unidade?: string) {
      super()
    }

  static fromJson(jsonData: any): Expiracao {
    return Object.assign(new Expiracao(), jsonData);
  }
}
