import { BaseResourceModel } from '../../shared/models/base-resource.model';

// "displayName": "EMPRESA",
//     "userActivationKey": "1564747561:$P$Bp6iENPeDAnttlN0YEEdjb0Y.AUmrO0",
//     "userEmail": "EMPRESA",
//     "userLogin": null,
//     "userNicename": "empresa",
//     "userPass": "$P$B4DxunJs9hO7n3DBbyVibWUUBO9A1m0",
//     "userRegistered": "2019-08-02T15:06:00.000+0000",
//     "userStatus": 0,
//     "userUrl": "",
//     "id": 16,
//     "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJkYXRlIjoiMjAxOS0wOS0wN1QxMDoyMToxNi40MTIiLCJzdWIiOiJFTVBSRVNBIn0._iFKZeTtZnrcGN6NCtnKtLaGoyqvUpUqQZDUF4cJx0rLxajvIsl_Qs3U9F3Xr0OIMBCAiFcYSfItG9AX0xysxQ",
//     "cpf": "025.414.715-13",
//     "cellphone": "",
//     "neighborhood": "CENTRO",
//     "endereco_number": "137",
//     "phone": "79981191742",
//     "country": "BR",
//     "last_name": "SANTOS",
//     "address_1": "RUA ANTONIO TEODORO DE JESUS",
//     "city": "ROSÁRIO DO CATETE",
//     "state": "SE",
//     "postcode": "49760-000",
//     "first_name": "MARCUS",
//     "description": "",
//     "last_active": "1567728000"


export class Usuario extends BaseResourceModel {
    constructor(public email?: string, 
                public senha?: string, 
                public cpf?: string, 
                public phone?: string, 
                public pis?: string, 
                public address_1?: string,
                public token?: string) {
      super()
    }

  static fromJson(jsonData: any): Usuario {
    return Object.assign(new Usuario(), jsonData);
  }
}
