import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Expiracao } from './expiracao.model';

export class RequisicaoPreAprovado extends BaseResourceModel {
    constructor(public carrega?: string,
                public nome?: string,
                public detalhes?: string,
                public percentualExtraPagamento?: number,
                public valorMaximoPercentualPagamento?: number,
                public valorMaximoTotal?: number,
                public valorMaximoPercentualPeriodo?: number,
                public valorPagamentoMensal?: number,
                public pagamentoMaximoPercentualPeriodo?: number,
                public periodo?: string,
                public dataInicial?: string,
                public dataFinal?: string,
                public taxaAssociacao?: number,
                public tentativaPeriodoDuracao?: number,
                public expiracao?: Expiracao,
                public diaDoAno?: number,
                public diaDoMes?: number,
                public diaDaSemana?: number,
                public cancelarURL?: string) {
      super()
    }

  static fromJson(jsonData: any): RequisicaoPreAprovado {
    return Object.assign(new RequisicaoPreAprovado(), jsonData);
  }
}
