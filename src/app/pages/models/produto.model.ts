import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Produto extends BaseResourceModel {
    constructor(public idProduto?: string,
                public descricao?: string,
                public quantidade?: number,
                public peso?: number,
                public preco?: number) {
      super()
    }

  static fromJson(jsonData: any): Produto {
    return Object.assign(new Produto(), jsonData);
  }
}
