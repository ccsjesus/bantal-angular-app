import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class PlanoUsuario extends BaseResourceModel {
    constructor(public referenciaPlano?: string,
                public nomePlano?: string,
                public codigoTransacao?: string,
                public slug?: string,
                public aderido?: boolean) {
      super()
    }

  static fromJson(jsonData: any): PlanoUsuario {
    return Object.assign(new PlanoUsuario(), jsonData);
  }
}
