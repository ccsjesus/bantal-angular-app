import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class LoginResponseModel extends BaseResourceModel {
    constructor(public accessToken?: string, 
                public expiresIn?: string,
                public refreshToken?: string) {
      super()
    }

  static fromJson(jsonData: any): LoginResponseModel {
    return Object.assign(new LoginResponseModel(), jsonData);
  }
}
