import { Plano } from './plano.model';
import { BaseResourceModel } from '../../shared/models/base-resource.model';


export class Perfil extends BaseResourceModel {
    constructor(public id?: number,
                public userId?: string,
                public nome?: string,
                public displayNome?: string,
                public email?: string,
                public perfil?: string,
                public identificacao?: string,
                public foto?: any,
                public plano?: Plano[]){
        super()
    }
    static fromJson(jsonData: any): Perfil {
        return Object.assign(new Perfil(), jsonData);
    }
}
