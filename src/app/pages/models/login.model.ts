import { BaseResourceModel } from '../../shared/models/base-resource.model';


export class LoginRequest extends BaseResourceModel {
    constructor(public identificacao?: string,
                public senha?: string,) {
      super()
    }

  static fromJson(jsonData: any): LoginRequest {
    return Object.assign(new LoginRequest(), jsonData);
  }
}
