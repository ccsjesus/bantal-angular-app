import { PlanoUsuario } from './plano-usuario.model';
import { BaseResourceModel } from '../../shared/models/base-resource.model';


export class UsuarioRequest extends BaseResourceModel {
  constructor(public email?: string,
              public sobreEmpresa?: string,
              public objetivo?: string,
              public sigla?: string,
              public senha?: string,
              public displayName?: string,
              public tokens? : Array<any>,
              public userEmail?: string,
              public newSenha?: string,
              public first_name?: string,
              public last_name?: string,
              public userPass?: string,
              public funcaoSistema?: string,
              public curriculoCandidatoValido?: Boolean,
              public planoUsuario?: PlanoUsuario,
              public imagemPerfil?: string,
              public foto?: any,
              public imagemPerfilInformacoes?: string) {
    super()
  }


  static fromJson(jsonData: any): UsuarioRequest {
    return Object.assign(new UsuarioRequest(), jsonData);
  }
}
