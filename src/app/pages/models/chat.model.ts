import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class ChatModel extends BaseResourceModel {
    constructor(public nome?: string,
                public apresentacao?: string,
                public nacionalidade?: string,
                public estadoCivil?: string,

                public qtdFilhos?: string,
                public idade?: string,
                public endereco?: string) {
      super();
    }

  static fromJson(jsonData: any): ChatModel {
    return Object.assign(new ChatModel(), jsonData);
}
}
