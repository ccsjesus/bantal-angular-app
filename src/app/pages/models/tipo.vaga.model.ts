import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class TipoVagaModel extends BaseResourceModel {
  constructor(public nome?: string,
              public valor?: string,
              public habilitada?: number) {
    super()
  }

static fromJson(jsonData: any): TipoVagaModel {
  return Object.assign(new TipoVagaModel(), jsonData);
}
}
