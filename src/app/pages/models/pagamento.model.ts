import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Remetente } from './remetente.model';
import { Remessa } from './remessa.model';
import { Produto } from './produto.model';

export class Pagamento extends BaseResourceModel {
    constructor(public idPagamento?: string,
                public moeda?: string,
                public precoExtra?: number,
                public referencia?: string,
                public remetente?: Remetente,
                public remessa?: Remessa,
                public produtos?: Array<Produto>,
                public cartaoCredito?: boolean) {
      super()
    }

  static fromJson(jsonData: any): Pagamento {
    return Object.assign(new Pagamento(), jsonData);
  }
}
