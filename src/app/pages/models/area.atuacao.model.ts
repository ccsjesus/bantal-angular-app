import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class AreaAtuacaoModel extends BaseResourceModel {
  constructor(public termId?: string, 
              public name?: string, 
              public slug?: string) {
    super()
  }

static fromJson(jsonData: any): AreaAtuacaoModel {
  return Object.assign(new AreaAtuacaoModel(), jsonData);
}
}
