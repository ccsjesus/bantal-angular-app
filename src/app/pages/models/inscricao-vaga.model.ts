import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class InscricaoVaga extends BaseResourceModel {
    constructor(public cdUsuario?:number,
                public nomeCandidato?:string,
                public codigoVaga?:string,
                public comentarioInscricao?:string) {
      super()
    }

  static fromJson(jsonData: any): InscricaoVaga {
    return Object.assign(new InscricaoVaga(), jsonData);
  }
}