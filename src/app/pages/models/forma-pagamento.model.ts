import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Remetente } from './remetente.model';
import { Remessa } from './remessa.model';
import { Produto } from './produto.model';

export class FormaPagamento extends BaseResourceModel {
    constructor(public code?: string,
                public codeId?: string,
                public type?: string,
                public typeId?: string) {
      super()
    }

  static fromJson(jsonData: any): FormaPagamento {
    return Object.assign(new FormaPagamento(), jsonData);
  }
}
