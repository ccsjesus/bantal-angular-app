import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class StatusPagamento extends BaseResourceModel {
    constructor(public status?: string,
                public statusId?: number) {
      super()
    }

  static fromJson(jsonData: any): StatusPagamento {
    return Object.assign(new StatusPagamento(), jsonData);
  }
}
