import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Vaga extends BaseResourceModel {
    constructor(public cdVaga?: string,
                public titulo?: string,
                public localizacao?: string,
                public tipoVaga?: string,
                public area?: string,
                public descricao?: string,
                public email?: string,
                public dataLimite?: string,
                public vagas?: any,
                public qtdVagasAbertas?: string,
                public totalInscritos?: string,
                public usuario?: any,
                public situacao?: string,
                public  salario?: string,
                public  idAreaAtuacao?: string,
                public  nomeAreaAtuacao?: string,
                public  slugAreaAtuacao?: string,
                public  estado?: string,
                public  cidade?: string) {
      super()
    }

  static fromJson(jsonData: any): Vaga {
    return Object.assign(new Vaga(), jsonData);
  }
}
