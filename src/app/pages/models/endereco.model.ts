import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Endereco extends BaseResourceModel {
    constructor(public codigoPostal?: string,
                public pais?: number,
                public estado?: string,
                public cidade?: string,
                public complemento?: string,
                public distrito?: string,
                public numero?: string,
                public rua?: string) {
      super()
    }

  static fromJson(jsonData: any): Endereco {
    return Object.assign(new Endereco(), jsonData);
  }
}
