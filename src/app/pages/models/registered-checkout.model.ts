import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class RegisteredCheckout extends BaseResourceModel {
    constructor(public checkoutCode?: string,
                public redirectURL?: string) {
      super()
    }

  static fromJson(jsonData: any): RegisteredCheckout {
    return Object.assign(new RegisteredCheckout(), jsonData);
  }
}
