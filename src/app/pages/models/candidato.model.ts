import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Candidato extends BaseResourceModel {
    constructor(public nome?: string,
                public email?: string,
                public senha?: string,
                public identificacao?: string,
                public cnpj?: string,
                public telefone?: string,
                public pis?: string,
                public contaBancaria?: string,
                public endereco?: string,
                public user?: any,
                public titulo?: string ) {
      super()
    }

  static fromJson(jsonData: any): Candidato {
    return Object.assign(new Candidato(), jsonData);
  }
}
