import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class CNH extends BaseResourceModel {
    constructor(public name?: string, 
                public value?: string, 
                public selected?: Boolean ) {
      super()
    }

  static fromJson(jsonData: any): CNH {
    return Object.assign(new CNH(), jsonData);
  }
}
