import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class RegisteredPreApproval extends BaseResourceModel {
    constructor(public preApprovalCode?: string,
                public redirectURL?: string,
                public codeReference?: string) {
      super()
    }

  static fromJson(jsonData: any): RegisteredPreApproval {
    return Object.assign(new RegisteredPreApproval(), jsonData);
  }
}
