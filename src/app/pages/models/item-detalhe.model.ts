import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class ItemDetalhe extends BaseResourceModel {
    constructor(public amount?: string,
                public description?: string,
                public quantity?: number,
                public shippingCost?: string,
                public weight?: number) {
      super()
    }

  static fromJson(jsonData: any): ItemDetalhe {
    return Object.assign(new ItemDetalhe(), jsonData);
  }
}
