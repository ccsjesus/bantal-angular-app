import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class IdiomaModel extends BaseResourceModel {
    constructor(public idioma?: string,
                public leitura?: string,
                public escrita?: string,
                public conversacao?: string) {
      super();
    }

  static fromJson(jsonData: any): IdiomaModel {
    return Object.assign(new IdiomaModel(), jsonData);
}
}
