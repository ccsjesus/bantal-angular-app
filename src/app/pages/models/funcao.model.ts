import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class FuncaoModel extends BaseResourceModel {
    constructor(public nome?: string,
                public valor?: string) {
      super();
    }

  static fromJson(jsonData: any): FuncaoModel {
    return Object.assign(new FuncaoModel(), jsonData);
}
}
