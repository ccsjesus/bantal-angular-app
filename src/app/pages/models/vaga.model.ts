import { MensagemModel } from './mensagem';
import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Vaga extends BaseResourceModel {
    constructor(public cdVaga?: string,
                public titulo?: string,
                public localizacao?: string,
                public tipoVaga?: string,
                public area?: string,
                public descricao?: string,
                public email?: string,
                public dataLimite?: string,
                public vagas?: any[],
                public qtdVagasAbertas?: string,
                public totalInscritos?: string,
                public usuario?: any,
                public situacao?: string,
                public foto?: string,
                public mensagens?: Array<MensagemModel>) {
      super()
    }

  static fromJson(jsonData: any): Vaga {
    return Object.assign(new Vaga(), jsonData);
  }
}
