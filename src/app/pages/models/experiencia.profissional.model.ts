import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class ExperienciaProfissionalModel extends BaseResourceModel {
  constructor(public empregador?: string,
              public nomeTrabalho?: string,
              public profissao?: string,
              public dataInicio?: string,
              public dataTermino?: string,
              public notas?: string,
              public experiencia?: number,
              public desAtribuicao?: string ) {
    super()
  }

static fromJson(jsonData: any): ExperienciaProfissionalModel {
  return Object.assign(new ExperienciaProfissionalModel(), jsonData);
}
}
