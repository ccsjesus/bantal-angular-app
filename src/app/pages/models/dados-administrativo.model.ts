import { InscricaoVaga } from './inscricao-vaga.model';
import { Curriculo } from './curriculo.model';
import { Vaga } from './vaga.model';
import { Perfil } from './perfil.model';
import { BaseResourceModel } from "../../shared/models/base-resource.model";

export class DadosAdministrativo extends BaseResourceModel {
    constructor(public listaPerfil?: Perfil[],
                public listaVagas?: Vaga[],
                public listaCurriculos?: Curriculo[],
                public listaInscricaoVagas?: InscricaoVaga[]){
        super();
    }
    static fromJson(jsonData: any): DadosAdministrativo {
        return Object.assign(new DadosAdministrativo(), jsonData);
    }
}
