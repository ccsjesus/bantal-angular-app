import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Colaborador extends BaseResourceModel {
    constructor(public nome?: string, 
                public email?: string, 
                public senha?: string, 
                public cpf?: string, 
                public cnpj?: string,
                public telefone?: string, 
                public pis?: string, 
                public contaBancaria?: string,
                public endereco?: string) {
      super()
    }

  static fromJson(jsonData: any): Colaborador {
    return Object.assign(new Colaborador(), jsonData);
  }
}
