import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Endereco } from './endereco.model';

export class Remessa extends BaseResourceModel {
    constructor(public idRemessa?: string,
                public custo?: number,
                public endereco?: Endereco) {
      super()
    }

  static fromJson(jsonData: any): Remessa {
    return Object.assign(new Remessa(), jsonData);
  }
}
