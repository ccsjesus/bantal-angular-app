import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { ItemDetalhe } from './item-detalhe.model';
import { FormaPagamento } from './forma-pagamento.model';
import { StatusPagamento } from './status-pagamento.model';

export class DetalheTransacao extends BaseResourceModel {
    constructor(public cancellationSource?: string,
                public code?: string,
                public date?: string,
                public items?: ItemDetalhe[],
                public paymentMethod?: FormaPagamento,
                public status?: StatusPagamento) {
      super()
    }

  static fromJson(jsonData: any): DetalheTransacao {
    return Object.assign(new DetalheTransacao(), jsonData);
  }
}
