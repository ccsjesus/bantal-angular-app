import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class AreaAtuacaoDTO extends BaseResourceModel {
  constructor(public idAreaAtuacao?: number, 
              public nome?: string, 
              public foto?: string, 
              public quantidade?: number) {
    super()
  }

static fromJson(jsonData: any): AreaAtuacaoDTO {
  return Object.assign(new AreaAtuacaoDTO(), jsonData);
}
}
