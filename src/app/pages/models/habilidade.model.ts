import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class HabilidadeModel extends BaseResourceModel {
  constructor(public termId?: string, 
              public name?: string, 
              public slug?: string) {
    super()
  }

static fromJson(jsonData: any): HabilidadeModel {
  return Object.assign(new HabilidadeModel(), jsonData);
}
}
