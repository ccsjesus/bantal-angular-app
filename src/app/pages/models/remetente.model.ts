import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Telefone } from './telefone.model';

export class Remetente extends BaseResourceModel {
    constructor(public idRemetente?: string,
                public nome?: string,
                public cpf?: string,
                public email?: string,
                public telefone?: Telefone) {
      super()
    }

  static fromJson(jsonData: any): Remetente {
    return Object.assign(new Remetente(), jsonData);
  }
}
