import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Profissao extends BaseResourceModel {
    constructor(public idProfissao?: number,
                public nome?: string,
                public idAreaAtuacao?: number,
                public quantidade?: number,
                public habilitada?: boolean) {
      super()
    }

  static fromJson(jsonData: any): Profissao {
    return Object.assign(new Profissao(), jsonData);
  }
}
