import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class CursosModel extends BaseResourceModel {
    constructor(public instituicao?: string,
                public nomeCurso?: string,
                public anoInicio?: number,
                public anoFim?: number,
                public cargaHoraria?: number) {
      super();
    }

  static fromJson(jsonData: any): CursosModel {
    return Object.assign(new CursosModel(), jsonData);
}
}
