import { BaseResourceModel } from '../../shared/models/base-resource.model';


export class MessagesModel extends BaseResourceModel {
    constructor(public key?: string,
                public message?: string,) {
      super()
    }

  static fromJson(jsonData: any): MessagesModel {
    return Object.assign(new MessagesModel(), jsonData);
  }
}
