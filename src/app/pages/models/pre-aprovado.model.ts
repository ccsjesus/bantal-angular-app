import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { Remetente } from './remetente.model';
import { Remessa } from './remessa.model';
import { Produto } from './produto.model';
import { RequisicaoPreAprovado } from './requisicao-pre-aprovado.model';

export class PreAprovado extends BaseResourceModel {
    constructor(public redirecionaURL?: string,
                public notificacaoURL?: string,
                public moeda?: string,
                public precoExtra?: number,
                public referencia?: string,
                public remetente?: Remetente,
                public remessa?: Remessa,
                public requisicao?: RequisicaoPreAprovado,
                public cartaoCredito?: boolean) {
      super()
    }

  static fromJson(jsonData: any): PreAprovado {
    return Object.assign(new PreAprovado(), jsonData);
  }
}
