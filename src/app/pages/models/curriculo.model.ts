import { InformaticaModel } from './informatica-model.model';
import { IdiomaModel } from './idioma.model';
import { BaseResourceModel } from '../../shared/models/base-resource.model';
import { CNH } from './cnh.model';
import { FormacaoAcademicaModel } from './formacao.academica.model';
import { ExperienciaProfissionalModel } from './experiencia.profissional.model';
import { AreaAtuacaoModel } from './area.atuacao.model';
import { FuncaoModel } from './funcao.model';

 export class Curriculo extends BaseResourceModel {
    constructor(public nome?: string,
                public apresentacao?: string,
                public nacionalidade?: string,
                public estadoCivil?: string,

                public filhos?: string,
                public idade?: string,
                public endereco?: string,

                public telefone?: string,
                public celular?: string,
                public telefoneRecados?: string,

                public email?: string,
                public cnh?: Array<CNH>,
                public identificacao?: string,

                public rg?: string,
                public formacao?: string,
                public formacaoAcademica?: Array<FormacaoAcademicaModel>,
                public experienciaProfissional?: Array<ExperienciaProfissionalModel>,
                public areaAtuacao?:  Array<AreaAtuacaoModel>,
                public funcaoCargo?: FuncaoModel,

                public nivel?: FuncaoModel,
                public pretensaoSalarial?: string,
                public qualificacao?: string,
                public objetivoProfissional?: string,
                public foto?: string,
                public idiomas?:  Array<IdiomaModel>,
                public informatica?:  Array<InformaticaModel>) {
      super();
    }

  static fromJson(jsonData: any): Curriculo {
    return Object.assign(new Curriculo(), jsonData);
}
}
