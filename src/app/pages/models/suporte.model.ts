import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class SuporteModel extends BaseResourceModel {
    constructor(public nome?: string,
                public destinatario?: string,
                public telefone?: string,
                public assunto?: string,
                public mensagem?: string) {
      super();
    }

  static fromJson(jsonData: any): SuporteModel {
    return Object.assign(new SuporteModel(), jsonData);
}
}
