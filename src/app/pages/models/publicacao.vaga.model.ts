import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class PublicacaoVaga extends BaseResourceModel {
    constructor(public idVaga?: string,
                public postContent?: string,
                public titulo?: string,
                public descricao?: string,
                public localizacao?: string,
                public salario?: string,
                public CNPJ?: string,
                public idAreaAtuacao?: string,
                public idProfissao?: string,
                public dataExpiracao?: string,
                public idTaxonomia?: string,
                public idTipoVaga?: string,
                public foto?: any,
                public logomarca?: any,
                public confidencial?: string) {
      super()
    }

  static fromJson(jsonData: any): PublicacaoVaga {
    return Object.assign(new PublicacaoVaga(), jsonData);
  }
}
