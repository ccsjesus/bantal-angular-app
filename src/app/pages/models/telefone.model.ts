import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Telefone extends BaseResourceModel {
    constructor(public idTelefone?: string,
                public codigoArea?: string,
                public numero?: string) {
      super()
    }

  static fromJson(jsonData: any): Telefone {
    return Object.assign(new Telefone(), jsonData);
  }
}
