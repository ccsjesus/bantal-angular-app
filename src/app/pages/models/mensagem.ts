import { BaseResourceModel } from '../../shared/models/base-resource.model';


export class MensagemModel extends BaseResourceModel {
    constructor(public mensagem?: string,
                public tipoMensagem?: number,
                public idVaga?: string,
                public dataMensagem?: string,
                public codigoAutorMensagem?: string,
                public nomeAutorMensagem?: string,
                public codigoReferenciaMensagem?: string) {
      super()
    }

  static fromJson(jsonData: any): MensagemModel {
    return Object.assign(new MensagemModel(), jsonData);
  }
}
