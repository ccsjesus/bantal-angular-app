import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class FormacaoAcademicaModel extends BaseResourceModel {
    constructor(public universidade?: string, 
                public qualificacao?: string, 
                public dataInicio?: string,
                public dataFim?: string,
                public nota?: string ) {
      super()
    }

  static fromJson(jsonData: any): FormacaoAcademicaModel {
    return Object.assign(new FormacaoAcademicaModel(), jsonData);
  }
}
