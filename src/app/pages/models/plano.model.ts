import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class Plano extends BaseResourceModel {
    constructor(public cdPlano?: string,
                public nome?: string,
                public slug?: string,
                public descTempo?: string,
                public valor?: number,
                public vantagens?: Plano[],
                public aderido?: boolean,
                public duracao?: number) {
      super()
    }

  static fromJson(jsonData: any): Plano {
    return Object.assign(new Plano(), jsonData);
  }
}
