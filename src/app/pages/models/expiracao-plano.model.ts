import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class ExpiracaoPlanoModel extends BaseResourceModel {
    constructor(public dataPlano?: string,
                public expiracaoPlano?: string,
                public diasExpirar?: number,
                public exibirAlertaExpiracao?: boolean) {
      super()
    }
6
  static fromJson(jsonData: any): ExpiracaoPlanoModel {
    return Object.assign(new ExpiracaoPlanoModel(), jsonData);
  }
}
