import { BaseResourceModel } from '../../shared/models/base-resource.model';

export class ParametroModel extends BaseResourceModel {
  constructor(public termId?: string,
              public name?: string,
              public slug?: string) {
    super()
  }

static fromJson(jsonData: any): ParametroModel {
  return Object.assign(new ParametroModel(), jsonData);
}
}
