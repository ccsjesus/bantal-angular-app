import { BaseResourceModel } from '../../shared/models/base-resource.model';

 export class InformaticaModel extends BaseResourceModel {
    constructor(public tecnologia?: string,
                public nivel?: string) {
      super();
    }

  static fromJson(jsonData: any): InformaticaModel {
    return Object.assign(new InformaticaModel(), jsonData);
}
}
