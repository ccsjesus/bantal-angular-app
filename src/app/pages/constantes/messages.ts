export var messages = [
    {
        key:"TOKEN_EXPIRED",
        message:"Token Expirado, por gentileza realize um novo login! "
     },

     {
        key:"USER_NOT_FOUND",
        message:"Usuário não encontrado, por gentileza realize o cadastro na plataforma Bantal ! "
     },

     {
        key:"AUTHENTICATION_FAILED",
        message:"Falha na autenticação, por gentileza realize um novo login! "
     },
     {
        key:"Unauthorized",
        message:"Usuário ou senha inválidos, por gentileza revise suas credenciais ! "
     },
     {
        key:"USER_OR_PASSWORD_INVALID",
        message:"Usuário ou senha inválidos, por gentileza revise suas credenciais ! "
     },
     {
        key:"Forbidden",
        message:"Usuário sem permissão, por gentileza entre em contato com a Bantal ! "
     },
     {
         key:"PLAN_NOT_FOUND",
         message:"Não foi encontrado plano no seu perfil, faça já a aquisição do seu Plano."
     },
     {
         key:"PLAN_EXPIRED",
         message: "Seu plano expirou, faça uma renovação para poder se inscrever nas vagas."
     },
     {
         key: "CONNECTION_DATABASE_ERROR",
         message:  "Não foi possível concluir a transação,por gentileza tente novamente."
     }


 ];
 