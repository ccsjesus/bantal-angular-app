export var planosEmpresa: any = [
    {
      cdPlano: "001",
      nome: "Plano Inicial",
      slug: "inicial",
      descTempo: "Acesso gratuito por 2 meses",
      valor: "0.00",
      vantagens:[
      ],
      aderido: true
    },
    {
      cdPlano: "002",
      nome: "Plano Ouro",
      slug: "ouro",
      descTempo: "Acesso gratuito por 6 meses",
      valor: "469.00",
      vantagens:[
      ],
      aderido: true
    },
    {
      cdPlano: "003",
      nome: "Plano Estrela",
      slug: "estrela",
      descTempo: "Acesso gratuito por 1 ano",
      valor: "954.00",
      vantagens:[

      ],
      aderido: true
    },

  ];
