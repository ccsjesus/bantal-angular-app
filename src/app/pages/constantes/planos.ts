export var planos: any = [
    {
      cdPlano: "001",
      nome: "Plano Inicial",
      slug: "inicial",
      descTempo: "Acesso gratuito por 2 meses",
      valor: "0.00",
      vantagens:[
        "Acesso, com login e senha, para atualizar o Currículo, quantas vezes desejar, e gerar pdf para impressão, por tempo ilimitado.",
        "Acesso ao Painel Resumo de Vagas disponíveis por empresa, durante 2 meses.",
        "Acesso para candidatar-se diretamente nas vagas disponibilizadas pelas empresas que anunciam na Bantal, durante 2 meses."
      ],
      aderido: true
    },
    {
      cdPlano: "002",
      nome: "Plano Ouro",
      slug: "ouro",
      descTempo: "Acesso gratuito por 6 meses",
      valor: "34.90",
      vantagens:[
        "Acesso, com login e senha, para atualizar o Currículo, quantas vezes desejar, e gerar pdf para impressão, por tempo ilimitado.",
        "Acesso ao Painel Resumo de Vagas disponíveis por empresa, durante 2 meses.",
        "Acesso para candidatar-se diretamente nas vagas disponibilizadas pelas empresas que anunciam na Bantal, durante 2 meses.",
        "Acesso ao Chat com as Empresas"
      ],
      aderido: true
    },
    {
      cdPlano: "003",
      nome: "Plano Estrela",
      slug: "estrela",
      descTempo: "Acesso gratuito por 1 ano",
      valor: "54.90",
      vantagens:[
        "Acesso, com login e senha, para atualizar o Currículo, quantas vezes desejar, e gerar pdf para impressão, por tempo ilimitado.",
        "Acesso ao Painel Resumo de Vagas disponíveis por empresa, durante 2 meses.",
        "Acesso para candidatar-se diretamente nas vagas disponibilizadas pelas empresas que anunciam na Bantal, durante 2 meses.",
        "Acesso ao Chat com as Empresas",
        "Orientação profissional para preenchimento de currículo, entrevistas, entre outros."
      ],
      aderido: true
    },

  ];
