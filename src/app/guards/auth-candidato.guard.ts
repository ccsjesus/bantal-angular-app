import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { LoginService } from '../pages/login/login.service';

@Injectable()
export class AuthGuardCandidato implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) { }

  // tslint:disable-next-line:no-trailing-whitespace

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {

      if( !this.loginService.isCandidato() || !this.loginService.isLoggedIn()){
        this.router.navigate(['/auth/login']);
      }

    return true;
  }
}
