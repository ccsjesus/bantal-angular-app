import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { LoginService } from '../pages/login/login.service';
import Swal from 'sweetalert2';

@Injectable()
export class AuthGuardEmpresa implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) { }

  // tslint:disable-next-line:no-trailing-whitespace

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {

      if( (!this.loginService.isEmpresa()) || !this.loginService.isLoggedIn()){
        this.router.navigate(['/auth/login']);
        //this.exibirAlertaPerfil();
        return false;
      }

    return true;
  }

  exibirAlertaPerfil(){
    Swal.fire(
      'Ops!',
      'O perfil do usuário não está cadastrado como EMPRESA !',
      'error'
    ).then((resultado) => {

    })
  }
}
