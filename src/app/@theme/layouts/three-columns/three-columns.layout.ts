import { Component } from '@angular/core';

@Component({
  selector: 'ngx-three-columns-layout',
  templateUrl: './three-columns.layout.html',
  styleUrls: ['./three-columns.layout.scss'],
})
export class ThreeColumnsLayoutComponent {}
