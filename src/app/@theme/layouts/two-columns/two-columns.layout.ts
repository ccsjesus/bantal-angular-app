import { Component } from '@angular/core';

@Component({
  selector: 'ngx-two-columns-layout',
  templateUrl: './two-columns.layout.html',
  styleUrls: ['./two-columns.layout.scss'],
})
export class TwoColumnsLayoutComponent {}
