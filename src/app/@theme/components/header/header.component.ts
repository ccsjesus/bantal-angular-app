import { UsuarioRequest } from './../../../pages/models/usuario.pass';
import { DadosServices } from './../../../shared/services/dados.service';
import { FUNCAO_CANDIDATO, FUNCAO_ADMINISTRATOR, FUNCAO_EMPRESA, ROLE_EMPLOYER, ROLE_CANDIDATE, ROLE_ADMIN } from './../../../pages/constantes';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LoginService } from '../../../pages/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: UsuarioRequest;
  urlHome = ''
  imagemPerfil: any;
  imgCarregando: any = '../../../../assets/images/perfil.png';
  displayNameCarregando = 'Carregando...';
  hasCarregado: boolean = false;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  // userMenu = [{ title: 'Profile'}, { title: 'Log out'}];
  userMenu = [ { title: 'Sair'}];

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private breakpointService: NbMediaBreakpointsService,
    private loginServices: LoginService,
    private route: Router,
    protected dadosUsuario: DadosServices) {
  }
  ngOnInit() {
    this.menuService.onItemClick().subscribe((event) => {
      this.onContecxtItemSelection(event.item.title);
    });

    this.currentTheme = this.themeService.currentTheme;

    this.dadosUsuario.obterDadosUsuario()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: UsuarioRequest) => {
          if(users.foto === null){
            this.imagemPerfil = '../../../../assets/images/perfil.png';
          } else {
            this.imagemPerfil = 'data:image/jpg;base64,' + users.foto;
          }
          this.user = users;
          this.hasCarregado = true;
        });

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  navigateHome() {
    //this.menuService.navigateHome();
    var dados = JSON.parse(localStorage.getItem('dados'));

    let acesso = localStorage.getItem('funcaoSistema');

    if( acesso === FUNCAO_CANDIDATO || acesso === ROLE_CANDIDATE){
      this.urlHome = '/selecao';
    } else if(acesso === FUNCAO_EMPRESA || acesso === ROLE_EMPLOYER) {
      this.urlHome = '/empresa';
    } else if (acesso === FUNCAO_ADMINISTRATOR || acesso === ROLE_ADMIN) {
      this.urlHome = '/admin';
    }
    window.location.href = this.urlHome;
    return false;
  }

  onContecxtItemSelection(title) {
    if (title === 'Sair') {
      this.loginServices.logout();
    }
  }
}
