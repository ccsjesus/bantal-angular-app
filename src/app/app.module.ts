import { NbMenuModule, NbMenuService, NbSidebarService } from '@nebular/theme';
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './guards/auth.guard';
import { LoginModule } from './pages/login/login.module';
import { PagesModule } from './pages/pages.module';
import { LoginService } from './pages/login/login.service';
import { SharedModule } from './shared/shared.module';
import { LoadingInterceptor } from './shared/interceptor/loading.interceptor';
import { LoadingService } from './shared/components/loading/loading.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Data } from './shared/models/data.model';
import { RelatorioServices } from './shared/services/relatorio.services';

import { EstadoServices } from './shared/services/estado.services';
import { TaxonomiaServices } from './shared/services/taxonomia.services';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {NgxMaskModule, IConfig} from 'ngx-mask'
import { DadosServices } from './shared/services/dados.service';
import { MessageService } from './shared/services/message';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;




@NgModule({
  declarations: [AppComponent],
  imports: [
    ReactiveFormsModule,
    LoginModule,
    PagesModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbMenuModule.forRoot(),
    SweetAlert2Module,
    NgxMaskModule.forRoot(options)
  ],
  bootstrap: [AppComponent],
  providers: [MessageService, NgxSpinnerService, LoadingService, LoginService, AuthGuard, NbMenuService, NbSidebarService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    },
    Data,
    RelatorioServices,
    EstadoServices,
    TaxonomiaServices,
    DadosServices
  ],
  
})
export class AppModule {
}
