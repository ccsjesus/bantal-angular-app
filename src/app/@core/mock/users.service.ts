import { IMG_USUARIO_NAO_CADASTRADO } from './../../pages/constantes';
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Contacts, RecentUsers, UserData, User } from '../data/users';
import { Usuario } from '../../pages/models/usuario.model';

@Injectable()
export class UserService {

  private time: Date = new Date;

  imgUser64 = IMG_USUARIO_NAO_CADASTRADO;

  private users: User =
    {
      name: localStorage.getItem('nome'),
      picture: 'assets/images/nick.png'
    };


  private users_system = {
    usuario: { email: 'adm@adm.com.br', senha: '123456' }
  };

  private types = {
    mobile: 'mobile',
    home: 'home',
    work: 'work',
  };

  private recentUsers: RecentUsers[]  = [

  ];

  private usersistema: Usuario  =
    { email: this.users_system.usuario.email, senha: this.users_system.usuario.senha};

  getUsers(): Observable<User> {
    this.users = new User();
    this.users.name = localStorage.getItem('nome');
    if(localStorage.getItem('imgPerfil')){
      this.users.picture = localStorage.getItem('imgPerfil') ;
    } else {
      this.users.picture = 'assets/images/perfil.png' ;
    }

    return observableOf(this.users);
  }

  getRecentUsers(): Observable<RecentUsers[]> {
    return observableOf(this.recentUsers);
  }

  getUsersSistema(): Observable<Usuario> {
    return observableOf(this.usersistema);
  }
}
