import { ParametroModel } from './../../../pages/models/parametro.model';
import { CNHServices } from './../../services/cnh.services';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormArray } from "@angular/forms";
import * as _ from "lodash";
import { Observable } from 'rxjs';
import { CNH } from '../../../pages/models/cnh.model';

@Component({
  selector: 'app-nb-checkbox',
  templateUrl: './nb-group-checkbox.component.html',
  styleUrls: ['./nb-group-checkbox.component.scss']
})
export class GroupNbCheckboxComponent implements OnInit {

  @Input('parentFormGroup')
  public parentFormGroup: FormGroup;
  personForm: FormGroup;
  selectedHobbiesNames: [string];


  constructor(
    protected cnhServices: CNHServices
  ) { }

  @Input('colecao')
  myhobbies: ParametroModel[];

  ngOnInit() {
    this.loadCNH();
    this.parentFormGroup.controls.cnh.valueChanges.subscribe(valor => {

      if(valor && valor.length > 0 && !this.myhobbies){
        this.myhobbies = valor;
        this.createFormInputs();
      }

    });

  }

  private loadCNH() {
    this.cnhServices.getTodasCategoriasCNH().subscribe(valor => {
      const cnh = this.parentFormGroup.controls.cnh.value;
      valor.forEach(element => {
        cnh.forEach(elementCNH => {
          if(elementCNH.value === element.value){
            element.selected = elementCNH.selected;
          }
        });
      });
      this.parentFormGroup.controls.cnh.setValue(valor);
    });
  }

  createFormInputs() {
    this.parentFormGroup.addControl(
      'hobbies', this.createHobbies(this.myhobbies)
      );
    this.getSelectedHobbies();
  }

  createHobbies(hobbiesInputs) {
    const arr = hobbiesInputs.map(hobby => {
      return new FormControl(hobby);
    });
    return new FormArray(arr);
  }

  checkboxValue: boolean = false;

  checkValue(event: any, hobby: any, pos) {
    this.checkboxValue = event.target.checked;
    this.parentFormGroup.controls.hobbies["controls"][pos].value.selected = this.checkboxValue;
    this.parentFormGroup.controls.cnh.setValue = this.parentFormGroup.controls.hobbies.value;
  }

  getSelectedHobbies() {
    this.selectedHobbiesNames = _.map(
      this.parentFormGroup.controls.hobbies["controls"],
      (hobby, i) => {
        return hobby.value && this.myhobbies[i].name;
      }
    );
    this.getSelectedHobbiesName();
  }

  getSelectedHobbiesName() {
    this.selectedHobbiesNames = _.filter(
      this.selectedHobbiesNames,
      function(hobby) {
        if (hobby !== false) {
          return hobby;
        }
      }
    );
  }


}
