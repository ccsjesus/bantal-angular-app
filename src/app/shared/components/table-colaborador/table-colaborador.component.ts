import { Component, Input, OnInit } from '@angular/core';
import { Colaborador } from '../../../pages/models/colaborador.model';
import { MessageService } from '../../services/message';
import { FormGroup } from '@angular/forms';
import { CarService } from '../../services/carservices';

@Component({
  selector: 'app-table-colaborador',
  templateUrl: './table-colaborador.component.html',
  styleUrls: ['./table-colaborador.component.scss']
})
export class TableColaboradorComponent implements OnInit {

  dados: Colaborador[];

  cols: any[];


  constructor(private carService: CarService, private messageService: MessageService) { }

  ngOnInit() {
      this.carService.getCars().subscribe(valor => this.dados = valor.data,
                                          error => this.messageService.add({severity:'info', summary:'Usuario Selecionado', detail:'Vin: ' + error}));

      this.cols = [
          { field: 'cpf', header: 'CPF' },
          { field: 'nome', header: 'Nome' },
          { field: 'telefone', header: 'Telefone' },
          { field: 'email', header: 'Email' }
      ];
  }

  selecionar(user: Colaborador) {
  }

  onRowSelect(event) {
      this.messageService.add({severity:'info', summary:'Car Selected', detail:'Vin: ' + event.data.vin});
  }

  onRowUnselect(event) {
      this.messageService.add({severity:'info', summary:'Car Unselected', detail:'Vin: ' + event.data.vin});
  }
}
