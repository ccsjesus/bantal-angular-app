import { FUNCAO_ADMINISTRATOR, ROLE_ADMIN, ROLE_CANDIDATE, ROLE_EMPLOYER } from './../../../pages/constantes';
import { OnInit, AfterContentChecked, Injector, Directive } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { BaseResourceModel } from '../../models/base-resource.model';
import { BaseResourceService } from '../../services/base-resource.service';
import { NbToastrService } from '@nebular/theme';
import { FUNCAO_CANDIDATO, FUNCAO_EMPRESA} from '../../../pages/constantes';



@Directive()
export abstract class BaseResourceFormComponent<T extends BaseResourceModel> implements OnInit, AfterContentChecked{

  currentAction: string = "";
  resourceForm: FormGroup;
  pageTitle: string;
  serverErrorMessages: string[] = null;
  submittingForm: boolean = false;

  protected route: ActivatedRoute;
  protected router: Router;
  protected formBuilder: FormBuilder;
  protected toastrService: NbToastrService;

  constructor(
    protected injector: Injector,
    public resource: T,
    protected resourceService: BaseResourceService<T>,
    protected jsonDataToResourceFn: (jsonData) => T
  ) {
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.formBuilder = this.injector.get(FormBuilder);
    this.toastrService = this.injector.get(NbToastrService);
    // this.loadingService = this.injector.get(LoadingService);
  }

  ngOnInit() {
    this.onLoadConfigurations();
    this.setCurrentAction();
    this.buildResourceForm();
    this.loadResource();
  }

  ngAfterContentChecked(){
    this.setPageTitle();
  }

  submitForm(){
    this.submittingForm = true;

    if(this.currentAction == "new") {
      this.createResource();
    } else if(this.currentAction == "auth")  {
      this.signLogin();
    } else {
      this.updateResource();
    }
  }


  // PRIVATE METHODS

  protected abstract setCurrentAction();
  //  {

  //   if(this.route.snapshot.url[0]) {
  //     if (this.route.snapshot.url[0].path == 'auth') {
  //       this.currentAction = "auth"
  //     } else {
  //       this.currentAction = ""
  //     }
  //   } else {
  //     this.currentAction = ""
  //   }
  // }

  protected loadResource() {

  }


  protected setPageTitle() {
    if (this.currentAction == 'new'){
      this.pageTitle = this.creationPageTitle();
    } else {
      this.pageTitle = this.editionPageTitle();
    }
  }

  protected creationPageTitle(): string{
    return "Novo"
  }

  protected editionPageTitle(): string{
    return "Edição"
  }


  protected createResource(){
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.resourceService.create(resource)
      .subscribe(
        resource => this.actionsForSuccess(resource),
        error => this.actionsForError(error)
      )
  }

  protected signLogin(){
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.resourceService.realizarLogin(resource)
         .subscribe(
        resource => this.actionsForSuccess(resource),
        error => this.actionsForError(error)
      )
  }

  protected updateResource(){
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.resourceService.update(resource)
      .subscribe(
        //resource => this.actionsForSuccess(resource),
        error => this.actionsForError(error)
      )
  }


  protected actionsForSuccess(resource: T){

    let funcaoSistema = localStorage.getItem('funcaoSistema');

    if(funcaoSistema === FUNCAO_CANDIDATO || funcaoSistema === ROLE_CANDIDATE){

      this.router.navigateByUrl('/candidato');
    } else if(funcaoSistema === FUNCAO_EMPRESA || funcaoSistema === ROLE_EMPLOYER){
      this.router.navigate(['/empresa']);
    } else if(funcaoSistema === FUNCAO_ADMINISTRATOR || funcaoSistema === ROLE_ADMIN){
      this.router.navigate(['/admin']);
    }

  }


  protected actionsForError(error){

    if(error.error !== undefined) {
      this.toastrService.danger(error.error.text);
    } else {
      this.toastrService.danger('Não foi possível se conectar com o servidor!');
    }

    this.submittingForm = false;

    if(error.status === 422)
      this.serverErrorMessages = JSON.parse(error._body).errors;
    else
      this.serverErrorMessages = ["Falha na comunicação com o servidor. Por favor, tente mais tarde."]
  }


  protected abstract buildResourceForm(): void;

  protected abstract onLoadConfigurations(): void;
}
