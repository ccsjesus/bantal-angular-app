import { Component, OnInit } from '@angular/core';
import { Vaga } from '../../../pages/models/vaga.model';
import { VagaServices } from '../../services/vaga.service';
import { MessageService } from '../../services/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-vagas',
  templateUrl: './table-vagas.component.html',
  styleUrls: ['./table-vagas.component.scss']
})
export class TableVagasComponent implements OnInit {

  dados: Vaga[];

  cols: any[];

  constructor(private service: VagaServices, private messageService: MessageService, private router: Router) { }

  ngOnInit() {
      // this.service.getCars().subscribe(valor => this.dados = valor.data,
      //                                     error => this.messageService.add({severity:'info', summary:'Usuario Selecionado', detail:'Vin: ' + error}));

      // this.cols = [
      //     { field: 'codigo', header: 'Nome da vaga' },
      //     { field: 'titulo' },
      //     { field: 'qtd_inscritos', header: 'Inscritos' }//,
      //     //{ field: 'data_limite', header: 'Encerramento da Vaga' }
      // ];
  }

  selecionarCandidatos(user: Vaga) {

    this.router.navigate(['/recrutamento/incritos']).then( (e) => {
        if (e) {
          
        } else {
          
        }
      });
  }

  onRowSelect(event) {
      this.messageService.add({severity:'info', summary:'Car Selected', detail:'Vin: ' + event.data.vin});
  }

  onRowUnselect(event) {
      this.messageService.add({severity:'info', summary:'Car Unselected', detail:'Vin: ' + event.data.vin});
  }
}
