import { Component, OnInit, Input, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingService } from './loading.service';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { LoginService } from '../../../pages/login/login.service';


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  loadingSubscription: Subscription;

  constructor(private loginService: LoginService, private loadingService: LoadingService, private spinner: NgxSpinnerService) {
    this.loadingSubscription = this.loadingService.loadingStatus.subscribe((value) => {
      if(value){
        this.spinner.show();
      } else {
        this.spinner.hide();
      }
    },
    (err) => {
      Swal.fire({
        type: 'error',
        title: 'Não foi possível obter as informações',
        text: 'Entre em contato com o Administrador Bantal'
      }).then((resultado) => {
        this.loginService.logout();
      });
    }
    );
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}
