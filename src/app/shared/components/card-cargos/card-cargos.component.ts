import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'card-cargos',
  templateUrl: './card-cargos.component.html',
  styleUrls: ['./card-cargos.component.scss']
})
export class CardCargosComponent implements OnInit {

  cargos = [ "Vendedor", "Advogado","Gerente","Engenheiro" ];

  @Input('parentForm')
  public parentForm: FormGroup;

  public title: ElementRef;

  constructor() {
  }

  ngOnInit() {

  }

  updateCargo(i) {
    this.parentForm.get('cargo').setValue(i);

    let valueSubmit = Object.assign({}, this.parentForm.value);
    valueSubmit = Object.assign(valueSubmit, {
      cargos : valueSubmit.cargos
      .map((v, i) =>v ? this.cargos[i] : null)
      .filter(v => v !== null)
    });

  }


}
