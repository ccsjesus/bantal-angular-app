import { noImage,confidencialImg } from './../../../pages/constantes/user';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ImageService } from '../../services/image.services';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import {user64} from '../../../pages/constantes/user';

class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';

  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-image-upload-empresa',
  templateUrl: 'image-upload-empresa.component.html',
  styleUrls: ['image-upload-empresa.component.scss']
})
export class ImageUploadEmpresaComponent implements OnInit, AfterViewInit {

  selectedFile: ImageSnippet;

  @Input('imgText')
  public imgText: string;

  @Input('parentForm')
  public parentForm: FormGroup;

  temFoto: boolean = false;

  nome: string = '';

  public mainImage = 'https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif';
  public showedImage = '';
  public novaImage;
  img = new Image();

  constructor(private imageService: ImageService, private elementRef: ElementRef){}

  ngAfterViewInit(): void {
    this.imageService.fakeUpload().subscribe(
      (res) => {
        this.img.src = this.mainImage;
      },
      (err) => {
        this.onError();
      })

     this.img.onload = () => {
       if(this.parentForm.controls.foto.value){
        this.showedImage = 'data:image/jpg;base64,' + this.parentForm.controls.foto.value;
       } else {
         this.showedImage = noImage;
       }
      this.temFoto = true;
    }
  }

  ngOnInit(): void {

    if(this.imgText === undefined){
      this.nome = 'Alterar Imagem';
    } else {
      this.nome = this.imgText;
    }

    this.parentForm.controls.foto.valueChanges.subscribe(valor => {
      this.imageService.fakeUpload().subscribe(
        (res) => {
          this.img.src = this.mainImage;
        },
        (err) => {
          this.onError();
        })

       this.img.onload = () => {
        this.showedImage = this.novaImage;
        this.temFoto = true;
      }
    });


  }

  private onSuccess(imagem) {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
    this.parentForm.controls.foto.setValue(imagem);

  }

  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    this.selectedFile = new ImageSnippet(file.name, file);

    reader.addEventListener('load', (event: any) => {

      this.selectedFile = new ImageSnippet(event.target.result, file);

      this.novaImage = this.selectedFile.src.toString();
      this.selectedFile.pending = true;

      let reader = new FileReader();
      reader.readAsDataURL(this.selectedFile.file);
      reader.onloadend = this._handleReaderLoaded.bind(this);
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };

    });
    reader.onloadend = this._handleReaderLoaded.bind(this);

    reader.readAsDataURL(file);
  }


  handleInputChange(files) {
    var file = files;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }

    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);

    this.parentForm.controls.foto.setValue(base64result);
    }
}
