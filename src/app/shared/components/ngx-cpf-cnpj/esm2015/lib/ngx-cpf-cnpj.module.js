/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CpfCnpjMaskDirective } from './cpf-cnpj-mask.directive';
import { CpfCnpjValidatorDirective } from './cpf-cnpj-validator.directive';
export class NgxCpfCnpjModule {
}
NgxCpfCnpjModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective],
                imports: [],
                exports: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNwZi1jbnBqLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jcGYtY25wai8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtY3BmLWNucGoubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBUTNFLE1BQU0sT0FBTyxnQkFBZ0I7OztZQU41QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsb0JBQW9CLEVBQUUseUJBQXlCLENBQUM7Z0JBQy9ELE9BQU8sRUFBRSxFQUNSO2dCQUNELE9BQU8sRUFBRSxDQUFDLG9CQUFvQixFQUFFLHlCQUF5QixDQUFDO2FBQzNEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENwZkNucGpNYXNrRGlyZWN0aXZlIH0gZnJvbSAnLi9jcGYtY25wai1tYXNrLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBDcGZDbnBqVmFsaWRhdG9yRGlyZWN0aXZlIH0gZnJvbSAnLi9jcGYtY25wai12YWxpZGF0b3IuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbQ3BmQ25wak1hc2tEaXJlY3RpdmUsIENwZkNucGpWYWxpZGF0b3JEaXJlY3RpdmVdLFxuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGV4cG9ydHM6IFtDcGZDbnBqTWFza0RpcmVjdGl2ZSwgQ3BmQ25walZhbGlkYXRvckRpcmVjdGl2ZV1cbn0pXG5leHBvcnQgY2xhc3MgTmd4Q3BmQ25wak1vZHVsZSB7IH1cbiJdfQ==