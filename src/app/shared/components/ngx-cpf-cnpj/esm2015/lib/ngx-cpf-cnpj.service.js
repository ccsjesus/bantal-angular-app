/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class NgxCpfCnpjService {
    constructor() { }
    /**
     * @param {?} num
     * @return {?}
     */
    convertToCpfCnpj(num) {
        if (num) {
            num = num.toString();
            num = this.getDigitos(num);
            switch (num.length) {
                case 4:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 5:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 6:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 7:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 8:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 9:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 10:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{1})/, '$1.$2.$3-$4');
                    break;
                case 11:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
                    break;
                case 12:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})/, '$1.$2.$3/$4');
                    break;
                case 13:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                    break;
                case 14:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                    break;
            }
        }
        return num;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    getDigitos(value) {
        return value.replace(/\D/g, '');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    cpfIsValid(value) {
        // Só faz a validação se o texto não for vazio.
        // Se for vazio, validar campo requerido não é a responsabilidade desta validação
        if (value.length > 0) {
            /** @type {?} */
            let IsValid = false;
            /** @type {?} */
            const cpf = this.getDigitos(value);
            if (cpf.length === 11) { // Se digitou o CPF por completo
                // Se digitou o CPF por completo
                // Verificação dos CPF's que não respeitam a regra de validação mas não são válidos
                /** @type {?} */
                const cpf_invalidos = ['00000000000', '11111111111', '22222222222', '33333333333',
                    '44444444444', '55555555555', '66666666666', '77777777777',
                    '88888888888', '99999999999'];
                for (let i = 0; i < 10; i++) {
                    if (cpf === cpf_invalidos[i]) {
                        return IsValid;
                    }
                }
                // Calculando o Primeiro Dígito Verificador
                /** @type {?} */
                let soma = 0;
                for (let i = 0; i < 9; i++) {
                    soma = soma + (parseInt(cpf.charAt(i), 10) * (10 - i));
                }
                /** @type {?} */
                let dv = 0;
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cpf.charAt(9), 10) !== dv) {
                    return IsValid;
                }
                // Calculando o Segundo Dígito Verificador
                soma = 0; // Soma para o CPF "ABC.DEF.GHI-XZ": (A*11)+(B*10)+...+(H*4)+(I*3)+(X*2)
                for (let i = 0; i < 10; i++) {
                    soma = soma + (parseInt(cpf.charAt(i), 10) * (11 - i));
                }
                dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cpf.charAt(10), 10) !== dv) {
                    return IsValid;
                }
                IsValid = true;
            }
            return IsValid;
        }
        return true;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    cnpjIsValid(value) {
        // Só faz a validação se o texto não for vazio.
        // Se for vazio, validar campo requerido não é a responsabilidade desta validação
        if (value.length > 0) {
            /** @type {?} */
            let IsValid = false;
            /** @type {?} */
            const cnpj = this.getDigitos(value);
            if (cnpj.length === 14) { // Se digitou o CNPJ por completo
                // Verificação do CNPJ que não respeita a regra de validação mas não são válidos
                if (cnpj === '00000000000000') {
                    return IsValid;
                }
                // Calculando o Primeiro Dígito Verificador
                /** @type {?} */
                let soma = 0;
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*5)+(B*4)+(C*3)+(D*2)+(E*9)+(F*8)+...+(K*3)+(L*2)
                /** @type {?} */
                let mult = 5;
                for (let i = 0; i < 12; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    mult--; // decrementa o multiplicador
                    if (mult === 1) {
                        mult = 9;
                    } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                }
                /** @type {?} */
                let dv = 0;
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cnpj.charAt(12), 10) !== dv) {
                    return IsValid;
                }
                // Calculando o Segundo Dígito Verificador
                soma = 0; // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*6)+(B*5)+(C*4)+(D*3)+(E*2)+(F*9)+...+(K*4)+(L*3)+(X*2)
                mult = 6; // multiplicador
                for (let i = 0; i < 13; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    mult--; // decrementa o multiplicador
                    if (mult === 1) {
                        mult = 9;
                    } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                }
                dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cnpj.charAt(13), 10) !== dv) {
                    return IsValid;
                }
                IsValid = true;
            }
            return IsValid;
        }
        return true;
    }
}
NgxCpfCnpjService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgxCpfCnpjService.ctorParameters = () => [];
/** @nocollapse */ NgxCpfCnpjService.ngInjectableDef = i0.defineInjectable({ factory: function NgxCpfCnpjService_Factory() { return new NgxCpfCnpjService(); }, token: NgxCpfCnpjService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNwZi1jbnBqLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY3BmLWNucGovIiwic291cmNlcyI6WyJsaWIvbmd4LWNwZi1jbnBqLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxpQkFBaUI7SUFFNUIsZ0JBQWdCLENBQUM7Ozs7O0lBRWpCLGdCQUFnQixDQUFDLEdBQUc7UUFDbEIsSUFBSSxHQUFHLEVBQUU7WUFDUCxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3JCLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzNCLFFBQVEsR0FBRyxDQUFDLE1BQU0sRUFBRTtnQkFDbEIsS0FBSyxDQUFDO29CQUNKLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDM0MsTUFBTTtnQkFDUixLQUFLLENBQUM7b0JBQ0osR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUMzQyxNQUFNO2dCQUNSLEtBQUssQ0FBQztvQkFDSixHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQzNDLE1BQU07Z0JBQ1IsS0FBSyxDQUFDO29CQUNKLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLHFCQUFxQixFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUNyRCxNQUFNO2dCQUNSLEtBQUssQ0FBQztvQkFDSixHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRSxVQUFVLENBQUMsQ0FBQztvQkFDckQsTUFBTTtnQkFDUixLQUFLLENBQUM7b0JBQ0osR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQ3JELE1BQU07Z0JBQ1IsS0FBSyxFQUFFO29CQUNMLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLDRCQUE0QixFQUFFLGFBQWEsQ0FBQyxDQUFDO29CQUMvRCxNQUFNO2dCQUNSLEtBQUssRUFBRTtvQkFDTCxHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyw0QkFBNEIsRUFBRSxhQUFhLENBQUMsQ0FBQztvQkFDL0QsTUFBTTtnQkFDUixLQUFLLEVBQUU7b0JBQ0wsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsNEJBQTRCLEVBQUUsYUFBYSxDQUFDLENBQUM7b0JBQy9ELE1BQU07Z0JBQ1IsS0FBSyxFQUFFO29CQUNMLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLG1DQUFtQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7b0JBQ3pFLE1BQU07Z0JBQ1IsS0FBSyxFQUFFO29CQUNMLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLG1DQUFtQyxFQUFFLGdCQUFnQixDQUFDLENBQUM7b0JBQ3pFLE1BQU07YUFDVDtTQUNGO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsK0NBQStDO1FBQy9DLGlGQUFpRjtRQUNqRixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDaEIsT0FBTyxHQUFHLEtBQUs7O2tCQUNiLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUNsQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFLEVBQUUsZ0NBQWdDOzs7O3NCQUVqRCxhQUFhLEdBQUcsQ0FBQyxhQUFhLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxhQUFhO29CQUNqRixhQUFhLEVBQUUsYUFBYSxFQUFFLGFBQWEsRUFBRSxhQUFhO29CQUMxRCxhQUFhLEVBQUUsYUFBYSxDQUFDO2dCQUM3QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUMzQixJQUFJLEdBQUcsS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQUUsT0FBTyxPQUFPLENBQUM7cUJBQUU7aUJBQ2xEOzs7b0JBR0csSUFBSSxHQUFHLENBQUM7Z0JBQ1osS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDMUIsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hEOztvQkFDRyxFQUFFLEdBQUcsQ0FBQztnQkFDVixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDbkIsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsQ0FBQztpQkFDdkI7Z0JBRUQsSUFBSSxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBQUUsT0FBTyxPQUFPLENBQUM7aUJBQUU7Z0JBRTNELDBDQUEwQztnQkFDMUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLHdFQUF3RTtnQkFDbEYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDM0IsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hEO2dCQUNELEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxzRkFBc0Y7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNuQixFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxDQUFDO2lCQUN2QjtnQkFFRCxJQUFJLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFBRSxPQUFPLE9BQU8sQ0FBQztpQkFBRTtnQkFFNUQsT0FBTyxHQUFHLElBQUksQ0FBQzthQUNoQjtZQUNELE9BQU8sT0FBTyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsK0NBQStDO1FBQy9DLGlGQUFpRjtRQUNqRixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDaEIsT0FBTyxHQUFHLEtBQUs7O2tCQUNiLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUVuQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFLEVBQUUsaUNBQWlDO2dCQUN6RCxnRkFBZ0Y7Z0JBQ2hGLElBQUksSUFBSSxLQUFLLGdCQUFnQixFQUFFO29CQUFFLE9BQU8sT0FBTyxDQUFDO2lCQUFFOzs7b0JBRzlDLElBQUksR0FBRyxDQUFDOzs7b0JBQ1IsSUFBSSxHQUFHLENBQUM7Z0JBQ1osS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDM0IsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDdEQsSUFBSSxFQUFFLENBQUMsQ0FBQyw2QkFBNkI7b0JBQ3JDLElBQUksSUFBSSxLQUFLLENBQUMsRUFBRTt3QkFBRSxJQUFJLEdBQUcsQ0FBQyxDQUFDO3FCQUFFLENBQUMsd0VBQXdFO2lCQUN2Rzs7b0JBQ0csRUFBRSxHQUFHLENBQUM7Z0JBQ1YsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ25CLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLENBQUM7aUJBQ3ZCO2dCQUNELElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUFFLE9BQU8sT0FBTyxDQUFDO2lCQUFFO2dCQUU3RCwwQ0FBMEM7Z0JBQzFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxtR0FBbUc7Z0JBQzdHLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0I7Z0JBQzFCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQzNCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ3RELElBQUksRUFBRSxDQUFDLENBQUMsNkJBQTZCO29CQUNyQyxJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7d0JBQUUsSUFBSSxHQUFHLENBQUMsQ0FBQztxQkFBRSxDQUFDLHdFQUF3RTtpQkFDdkc7Z0JBQ0QsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLHNGQUFzRjtnQkFDOUYsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ25CLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLENBQUM7aUJBQ3ZCO2dCQUNELElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUFFLE9BQU8sT0FBTyxDQUFDO2lCQUFFO2dCQUU3RCxPQUFPLEdBQUcsSUFBSSxDQUFDO2FBQ2hCO1lBQ0QsT0FBTyxPQUFPLENBQUM7U0FDaEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7OztZQWhKRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE5neENwZkNucGpTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIGNvbnZlcnRUb0NwZkNucGoobnVtKSB7XG4gICAgaWYgKG51bSkge1xuICAgICAgbnVtID0gbnVtLnRvU3RyaW5nKCk7XG4gICAgICBudW0gPSB0aGlzLmdldERpZ2l0b3MobnVtKTtcbiAgICAgIHN3aXRjaCAobnVtLmxlbmd0aCkge1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgbnVtID0gbnVtLnJlcGxhY2UoLyhcXGQrKShcXGR7M30pLywgJyQxLiQyJyk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgNTpcbiAgICAgICAgICBudW0gPSBudW0ucmVwbGFjZSgvKFxcZCspKFxcZHszfSkvLCAnJDEuJDInKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSA2OlxuICAgICAgICAgIG51bSA9IG51bS5yZXBsYWNlKC8oXFxkKykoXFxkezN9KS8sICckMS4kMicpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDc6XG4gICAgICAgICAgbnVtID0gbnVtLnJlcGxhY2UoLyhcXGQrKShcXGR7M30pKFxcZHszfSkvLCAnJDEuJDIuJDMnKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSA4OlxuICAgICAgICAgIG51bSA9IG51bS5yZXBsYWNlKC8oXFxkKykoXFxkezN9KShcXGR7M30pLywgJyQxLiQyLiQzJyk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgOTpcbiAgICAgICAgICBudW0gPSBudW0ucmVwbGFjZSgvKFxcZCspKFxcZHszfSkoXFxkezN9KS8sICckMS4kMi4kMycpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDEwOlxuICAgICAgICAgIG51bSA9IG51bS5yZXBsYWNlKC8oXFxkKykoXFxkezN9KShcXGR7M30pKFxcZHsxfSkvLCAnJDEuJDIuJDMtJDQnKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAxMTpcbiAgICAgICAgICBudW0gPSBudW0ucmVwbGFjZSgvKFxcZCspKFxcZHszfSkoXFxkezN9KShcXGR7Mn0pLywgJyQxLiQyLiQzLSQ0Jyk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgMTI6XG4gICAgICAgICAgbnVtID0gbnVtLnJlcGxhY2UoLyhcXGQrKShcXGR7M30pKFxcZHszfSkoXFxkezR9KS8sICckMS4kMi4kMy8kNCcpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDEzOlxuICAgICAgICAgIG51bSA9IG51bS5yZXBsYWNlKC8oXFxkKykoXFxkezN9KShcXGR7M30pKFxcZHs0fSkoXFxkezJ9KS8sICckMS4kMi4kMy8kNC0kNScpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIDE0OlxuICAgICAgICAgIG51bSA9IG51bS5yZXBsYWNlKC8oXFxkKykoXFxkezN9KShcXGR7M30pKFxcZHs0fSkoXFxkezJ9KS8sICckMS4kMi4kMy8kNC0kNScpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVtO1xuICB9XG5cbiAgZ2V0RGlnaXRvcyh2YWx1ZSkge1xuICAgIHJldHVybiB2YWx1ZS5yZXBsYWNlKC9cXEQvZywgJycpO1xuICB9XG5cbiAgY3BmSXNWYWxpZCh2YWx1ZSk6IGJvb2xlYW4ge1xuICAgIC8vIFPDsyBmYXogYSB2YWxpZGHDp8OjbyBzZSBvIHRleHRvIG7Do28gZm9yIHZhemlvLlxuICAgIC8vIFNlIGZvciB2YXppbywgdmFsaWRhciBjYW1wbyByZXF1ZXJpZG8gbsOjbyDDqSBhIHJlc3BvbnNhYmlsaWRhZGUgZGVzdGEgdmFsaWRhw6fDo29cbiAgICBpZiAodmFsdWUubGVuZ3RoID4gMCkge1xuICAgICAgbGV0IElzVmFsaWQgPSBmYWxzZTtcbiAgICAgIGNvbnN0IGNwZiA9IHRoaXMuZ2V0RGlnaXRvcyh2YWx1ZSk7IC8vIE9idMOpbSBvIHRleHRvIGRpZ2l0YWRvIG5vIGNhbXBvLCByZW1vdmUgdHVkbyBtZW5vcyBvcyBkaWdpdG9zXG4gICAgICBpZiAoY3BmLmxlbmd0aCA9PT0gMTEpIHsgLy8gU2UgZGlnaXRvdSBvIENQRiBwb3IgY29tcGxldG9cbiAgICAgICAgLy8gVmVyaWZpY2HDp8OjbyBkb3MgQ1BGJ3MgcXVlIG7Do28gcmVzcGVpdGFtIGEgcmVncmEgZGUgdmFsaWRhw6fDo28gbWFzIG7Do28gc8OjbyB2w6FsaWRvc1xuICAgICAgICBjb25zdCBjcGZfaW52YWxpZG9zID0gWycwMDAwMDAwMDAwMCcsICcxMTExMTExMTExMScsICcyMjIyMjIyMjIyMicsICczMzMzMzMzMzMzMycsXG4gICAgICAgICc0NDQ0NDQ0NDQ0NCcsICc1NTU1NTU1NTU1NScsICc2NjY2NjY2NjY2NicsICc3Nzc3Nzc3Nzc3NycsXG4gICAgICAgICc4ODg4ODg4ODg4OCcsICc5OTk5OTk5OTk5OSddO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDEwOyBpKyspIHtcbiAgICAgICAgICBpZiAoY3BmID09PSBjcGZfaW52YWxpZG9zW2ldKSB7IHJldHVybiBJc1ZhbGlkOyB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDYWxjdWxhbmRvIG8gUHJpbWVpcm8gRMOtZ2l0byBWZXJpZmljYWRvclxuICAgICAgICBsZXQgc29tYSA9IDA7IC8vIFNvbWEgcGFyYSBvIENQRiBcIkFCQy5ERUYuR0hJLVhaXCI6IChBKjEwKSsoQio5KSsuLi4rKEgqMykrKEkqMilcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCA5OyBpKyspIHtcbiAgICAgICAgICBzb21hID0gc29tYSArIChwYXJzZUludChjcGYuY2hhckF0KGkpLCAxMCkgKiAoMTAgLSBpKSk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGR2ID0gMDsgLy8gUHJpbWVpcm8gZMOtZ2l0byB2ZXJpZmljYWRvciAoc2Vyw6EgemVybyBzZSBvIHJlc3RvIGRhIGRpdmlzw6NvIGRlIHNvbWEgcG9yIDExIGZvciA8IDIpXG4gICAgICAgIGlmICgoc29tYSAlIDExKSA+IDEpIHtcbiAgICAgICAgICBkdiA9IDExIC0gKHNvbWEgJSAxMSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocGFyc2VJbnQoY3BmLmNoYXJBdCg5KSwgMTApICE9PSBkdikgeyByZXR1cm4gSXNWYWxpZDsgfVxuXG4gICAgICAgIC8vIENhbGN1bGFuZG8gbyBTZWd1bmRvIETDrWdpdG8gVmVyaWZpY2Fkb3JcbiAgICAgICAgc29tYSA9IDA7IC8vIFNvbWEgcGFyYSBvIENQRiBcIkFCQy5ERUYuR0hJLVhaXCI6IChBKjExKSsoQioxMCkrLi4uKyhIKjQpKyhJKjMpKyhYKjIpXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgMTA7IGkrKykge1xuICAgICAgICAgIHNvbWEgPSBzb21hICsgKHBhcnNlSW50KGNwZi5jaGFyQXQoaSksIDEwKSAqICgxMSAtIGkpKTtcbiAgICAgICAgfVxuICAgICAgICBkdiA9IDA7IC8vIFNlZ3VuZG8gZMOtZ2l0byB2ZXJpZmljYWRvciAoc2Vyw6EgemVybyBzZSBvIHJlc3RvIGRhIGRpdmlzw6NvIGRlIHNvbWEgcG9yIDExIGZvciA8IDIpXG4gICAgICAgIGlmICgoc29tYSAlIDExKSA+IDEpIHtcbiAgICAgICAgICBkdiA9IDExIC0gKHNvbWEgJSAxMSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocGFyc2VJbnQoY3BmLmNoYXJBdCgxMCksIDEwKSAhPT0gZHYpIHsgcmV0dXJuIElzVmFsaWQ7IH1cblxuICAgICAgICBJc1ZhbGlkID0gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBJc1ZhbGlkO1xuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGNucGpJc1ZhbGlkKHZhbHVlKTogYm9vbGVhbiB7XG4gICAgLy8gU8OzIGZheiBhIHZhbGlkYcOnw6NvIHNlIG8gdGV4dG8gbsOjbyBmb3IgdmF6aW8uXG4gICAgLy8gU2UgZm9yIHZhemlvLCB2YWxpZGFyIGNhbXBvIHJlcXVlcmlkbyBuw6NvIMOpIGEgcmVzcG9uc2FiaWxpZGFkZSBkZXN0YSB2YWxpZGHDp8Ojb1xuICAgIGlmICh2YWx1ZS5sZW5ndGggPiAwKSB7XG4gICAgICBsZXQgSXNWYWxpZCA9IGZhbHNlO1xuICAgICAgY29uc3QgY25waiA9IHRoaXMuZ2V0RGlnaXRvcyh2YWx1ZSk7IC8vIE9idMOpbSBvIHRleHRvIGRpZ2l0YWRvIG5vIGNhbXBvLCByZW1vdmUgdHVkbyBtZW5vcyBvcyBkaWdpdG9zXG5cbiAgICAgIGlmIChjbnBqLmxlbmd0aCA9PT0gMTQpIHsgLy8gU2UgZGlnaXRvdSBvIENOUEogcG9yIGNvbXBsZXRvXG4gICAgICAgIC8vIFZlcmlmaWNhw6fDo28gZG8gQ05QSiBxdWUgbsOjbyByZXNwZWl0YSBhIHJlZ3JhIGRlIHZhbGlkYcOnw6NvIG1hcyBuw6NvIHPDo28gdsOhbGlkb3NcbiAgICAgICAgaWYgKGNucGogPT09ICcwMDAwMDAwMDAwMDAwMCcpIHsgcmV0dXJuIElzVmFsaWQ7IH1cblxuICAgICAgICAvLyBDYWxjdWxhbmRvIG8gUHJpbWVpcm8gRMOtZ2l0byBWZXJpZmljYWRvclxuICAgICAgICBsZXQgc29tYSA9IDA7IC8vIFNvbWEgcGFyYSBvIENOUEogXCJBQi5DREUuRkdIL0lKS0wtWFpcIjogKEEqNSkrKEIqNCkrKEMqMykrKEQqMikrKEUqOSkrKEYqOCkrLi4uKyhLKjMpKyhMKjIpXG4gICAgICAgIGxldCBtdWx0ID0gNTsgLy8gbXVsdGlwbGljYWRvclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDEyOyBpKyspIHtcbiAgICAgICAgICBzb21hID0gc29tYSArIChwYXJzZUludChjbnBqLmNoYXJBdChpKSwgMTApICogKG11bHQpKTtcbiAgICAgICAgICBtdWx0LS07IC8vIGRlY3JlbWVudGEgbyBtdWx0aXBsaWNhZG9yXG4gICAgICAgICAgaWYgKG11bHQgPT09IDEpIHsgbXVsdCA9IDk7IH0gLy8gdm9sdGEgbyBtdWx0aXBsaWNhZG9yIHBhcmEgbyB2YWxvciA5IChwYXJhIGRlY3JlbWVudGFyIGF0w6kgbyB2YWxvciAyKVxuICAgICAgICB9XG4gICAgICAgIGxldCBkdiA9IDA7IC8vIFByaW1laXJvIGTDrWdpdG8gdmVyaWZpY2Fkb3IgKHNlcsOhIHplcm8gc2UgbyByZXN0byBkYSBkaXZpc8OjbyBkZSBzb21hIHBvciAxMSBmb3IgPCAyKVxuICAgICAgICBpZiAoKHNvbWEgJSAxMSkgPiAxKSB7XG4gICAgICAgICAgZHYgPSAxMSAtIChzb21hICUgMTEpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChwYXJzZUludChjbnBqLmNoYXJBdCgxMiksIDEwKSAhPT0gZHYpIHsgcmV0dXJuIElzVmFsaWQ7IH1cblxuICAgICAgICAvLyBDYWxjdWxhbmRvIG8gU2VndW5kbyBEw61naXRvIFZlcmlmaWNhZG9yXG4gICAgICAgIHNvbWEgPSAwOyAvLyBTb21hIHBhcmEgbyBDTlBKIFwiQUIuQ0RFLkZHSC9JSktMLVhaXCI6IChBKjYpKyhCKjUpKyhDKjQpKyhEKjMpKyhFKjIpKyhGKjkpKy4uLisoSyo0KSsoTCozKSsoWCoyKVxuICAgICAgICBtdWx0ID0gNjsgLy8gbXVsdGlwbGljYWRvclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDEzOyBpKyspIHtcbiAgICAgICAgICBzb21hID0gc29tYSArIChwYXJzZUludChjbnBqLmNoYXJBdChpKSwgMTApICogKG11bHQpKTtcbiAgICAgICAgICBtdWx0LS07IC8vIGRlY3JlbWVudGEgbyBtdWx0aXBsaWNhZG9yXG4gICAgICAgICAgaWYgKG11bHQgPT09IDEpIHsgbXVsdCA9IDk7IH0gLy8gdm9sdGEgbyBtdWx0aXBsaWNhZG9yIHBhcmEgbyB2YWxvciA5IChwYXJhIGRlY3JlbWVudGFyIGF0w6kgbyB2YWxvciAyKVxuICAgICAgICB9XG4gICAgICAgIGR2ID0gMDsgLy8gU2VndW5kbyBkw61naXRvIHZlcmlmaWNhZG9yIChzZXLDoSB6ZXJvIHNlIG8gcmVzdG8gZGEgZGl2aXPDo28gZGUgc29tYSBwb3IgMTEgZm9yIDwgMilcbiAgICAgICAgaWYgKChzb21hICUgMTEpID4gMSkge1xuICAgICAgICAgIGR2ID0gMTEgLSAoc29tYSAlIDExKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocGFyc2VJbnQoY25wai5jaGFyQXQoMTMpLCAxMCkgIT09IGR2KSB7IHJldHVybiBJc1ZhbGlkOyB9XG5cbiAgICAgICAgSXNWYWxpZCA9IHRydWU7XG4gICAgICB9XG4gICAgICByZXR1cm4gSXNWYWxpZDtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn1cbiJdfQ==