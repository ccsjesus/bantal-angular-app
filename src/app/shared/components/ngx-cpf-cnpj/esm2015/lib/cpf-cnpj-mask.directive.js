/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener } from '@angular/core';
import { NgModel } from '@angular/forms';
import { NgxCpfCnpjService } from './ngx-cpf-cnpj.service';
export class CpfCnpjMaskDirective {
    /**
     * @param {?} model
     * @param {?} cpfCnpjService
     */
    constructor(model, cpfCnpjService) {
        this.model = model;
        this.cpfCnpjService = cpfCnpjService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onInput(event) {
        if (event.target.value.length <= 17) {
            this.model.valueAccessor.writeValue(this.cpfCnpjService.convertToCpfCnpj(event.target.value));
        }
    }
}
CpfCnpjMaskDirective.decorators = [
    { type: Directive, args: [{
                selector: '[nccCpfCnpjMask] [ngModel]',
                providers: [NgModel]
            },] }
];
/** @nocollapse */
CpfCnpjMaskDirective.ctorParameters = () => [
    { type: NgModel },
    { type: NgxCpfCnpjService }
];
CpfCnpjMaskDirective.propDecorators = {
    onInput: [{ type: HostListener, args: ['input', ['$event'],] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    CpfCnpjMaskDirective.prototype.model;
    /**
     * @type {?}
     * @private
     */
    CpfCnpjMaskDirective.prototype.cpfCnpjService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3BmLWNucGotbWFzay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY3BmLWNucGovIiwic291cmNlcyI6WyJsaWIvY3BmLWNucGotbWFzay5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUszRCxNQUFNLE9BQU8sb0JBQW9COzs7OztJQUMvQixZQUFvQixLQUFjLEVBQVUsY0FBaUM7UUFBekQsVUFBSyxHQUFMLEtBQUssQ0FBUztRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtJQUFJLENBQUM7Ozs7O0lBRS9DLE9BQU8sQ0FBQyxLQUFLO1FBQzlDLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDL0Y7SUFDSCxDQUFDOzs7WUFYRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsU0FBUyxFQUFFLENBQUMsT0FBTyxDQUFDO2FBQ3JCOzs7O1lBTFEsT0FBTztZQUNQLGlCQUFpQjs7O3NCQVF2QixZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7O0lBRnJCLHFDQUFzQjs7Ozs7SUFBRSw4Q0FBeUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdNb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE5neENwZkNucGpTZXJ2aWNlIH0gZnJvbSAnLi9uZ3gtY3BmLWNucGouc2VydmljZSc7XG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbbmNjQ3BmQ25wak1hc2tdIFtuZ01vZGVsXScsXG4gIHByb3ZpZGVyczogW05nTW9kZWxdXG59KVxuZXhwb3J0IGNsYXNzIENwZkNucGpNYXNrRGlyZWN0aXZlIHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2RlbDogTmdNb2RlbCwgcHJpdmF0ZSBjcGZDbnBqU2VydmljZTogTmd4Q3BmQ25walNlcnZpY2UpIHsgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2lucHV0JywgWyckZXZlbnQnXSkgb25JbnB1dChldmVudCkge1xuICAgIGlmIChldmVudC50YXJnZXQudmFsdWUubGVuZ3RoIDw9IDE3KSB7XG4gICAgICB0aGlzLm1vZGVsLnZhbHVlQWNjZXNzb3Iud3JpdGVWYWx1ZSh0aGlzLmNwZkNucGpTZXJ2aWNlLmNvbnZlcnRUb0NwZkNucGooZXZlbnQudGFyZ2V0LnZhbHVlKSk7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==