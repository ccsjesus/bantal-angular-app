/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive } from '@angular/core';
import { NG_VALIDATORS } from '@angular/forms';
import { NgxCpfCnpjService } from './ngx-cpf-cnpj.service';
export class CpfCnpjValidatorDirective {
    constructor() {
        this.cpfCnpjService = new NgxCpfCnpjService();
        this.validator = this.cpfCnpjValidator();
    }
    /**
     * @param {?} c
     * @return {?}
     */
    validate(c) {
        return this.validator(c);
    }
    /**
     * @return {?}
     */
    cpfCnpjValidator() {
        return (c) => {
            /** @type {?} */
            let value = (/** @type {?} */ (c.value));
            if (value != null) {
                value = this.cpfCnpjService.getDigitos(value);
                if (value.length === 11) {
                    if (this.cpfCnpjService.cpfIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
                else if (value.length === 14) {
                    if (this.cpfCnpjService.cnpjIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
            }
            return {
                cpfcnpjvalidator: {
                    valid: false
                }
            };
        };
    }
}
CpfCnpjValidatorDirective.decorators = [
    { type: Directive, args: [{
                selector: '[nccCpfCnpjValidator] [ngModel]',
                providers: [
                    {
                        provide: NG_VALIDATORS,
                        useExisting: CpfCnpjValidatorDirective,
                        multi: true
                    }
                ]
            },] }
];
/** @nocollapse */
CpfCnpjValidatorDirective.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    CpfCnpjValidatorDirective.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    CpfCnpjValidatorDirective.prototype.cpfCnpjService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3BmLWNucGotdmFsaWRhdG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jcGYtY25wai8iLCJzb3VyY2VzIjpbImxpYi9jcGYtY25wai12YWxpZGF0b3IuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxhQUFhLEVBQXVDLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFXM0QsTUFBTSxPQUFPLHlCQUF5QjtJQUlwQztRQURRLG1CQUFjLEdBQXNCLElBQUksaUJBQWlCLEVBQUUsQ0FBQztRQUVsRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzNDLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLENBQWM7UUFDckIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDZCxPQUFPLENBQUMsQ0FBYyxFQUFFLEVBQUU7O2dCQUNwQixLQUFLLEdBQVcsbUJBQVEsQ0FBQyxDQUFDLEtBQUssRUFBQTtZQUNuQyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7Z0JBQ2pCLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLEVBQUUsRUFBRTtvQkFDdkIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDekMsT0FBTyxJQUFJLENBQUM7cUJBQ2I7eUJBQU07d0JBQ0wsT0FBTzs0QkFDTCxnQkFBZ0IsRUFBRTtnQ0FDaEIsS0FBSyxFQUFFLEtBQUs7NkJBQ2I7eUJBQ0YsQ0FBQztxQkFDSDtpQkFDRjtxQkFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFO29CQUM5QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUMxQyxPQUFPLElBQUksQ0FBQztxQkFDYjt5QkFBTTt3QkFDTCxPQUFPOzRCQUNMLGdCQUFnQixFQUFFO2dDQUNoQixLQUFLLEVBQUUsS0FBSzs2QkFDYjt5QkFDRixDQUFDO3FCQUNIO2lCQUNGO2FBQ0Y7WUFFRCxPQUFPO2dCQUNMLGdCQUFnQixFQUFFO29CQUNoQixLQUFLLEVBQUUsS0FBSztpQkFDYjthQUNGLENBQUM7UUFDSixDQUFDLENBQUM7SUFDSixDQUFDOzs7WUF2REYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQ0FBaUM7Z0JBQzNDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsYUFBYTt3QkFDdEIsV0FBVyxFQUFFLHlCQUF5Qjt3QkFDdEMsS0FBSyxFQUFFLElBQUk7cUJBQ1o7aUJBQUM7YUFDTDs7Ozs7O0lBR0MsOENBQXVCOzs7OztJQUN2QixtREFBb0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5HX1ZBTElEQVRPUlMsIFZhbGlkYXRvciwgVmFsaWRhdG9yRm4sIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTmd4Q3BmQ25walNlcnZpY2UgfSBmcm9tICcuL25neC1jcGYtY25wai5zZXJ2aWNlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW25jY0NwZkNucGpWYWxpZGF0b3JdIFtuZ01vZGVsXScsXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTElEQVRPUlMsXG4gICAgICB1c2VFeGlzdGluZzogQ3BmQ25walZhbGlkYXRvckRpcmVjdGl2ZSxcbiAgICAgIG11bHRpOiB0cnVlXG4gICAgfV1cbn0pXG5leHBvcnQgY2xhc3MgQ3BmQ25walZhbGlkYXRvckRpcmVjdGl2ZSBpbXBsZW1lbnRzIFZhbGlkYXRvciAge1xuXG4gIHZhbGlkYXRvcjogVmFsaWRhdG9yRm47XG4gIHByaXZhdGUgY3BmQ25walNlcnZpY2U6IE5neENwZkNucGpTZXJ2aWNlID0gbmV3IE5neENwZkNucGpTZXJ2aWNlKCk7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudmFsaWRhdG9yID0gdGhpcy5jcGZDbnBqVmFsaWRhdG9yKCk7XG4gIH1cblxuICB2YWxpZGF0ZShjOiBGb3JtQ29udHJvbCkge1xuICAgIHJldHVybiB0aGlzLnZhbGlkYXRvcihjKTtcbiAgfVxuXG4gIGNwZkNucGpWYWxpZGF0b3IoKTogVmFsaWRhdG9yRm4ge1xuICAgIHJldHVybiAoYzogRm9ybUNvbnRyb2wpID0+IHtcbiAgICAgIGxldCB2YWx1ZTogc3RyaW5nID0gPHN0cmluZz5jLnZhbHVlO1xuICAgICAgaWYgKHZhbHVlICE9IG51bGwpIHtcbiAgICAgICAgdmFsdWUgPSB0aGlzLmNwZkNucGpTZXJ2aWNlLmdldERpZ2l0b3ModmFsdWUpO1xuICAgICAgICBpZiAodmFsdWUubGVuZ3RoID09PSAxMSkge1xuICAgICAgICAgIGlmICh0aGlzLmNwZkNucGpTZXJ2aWNlLmNwZklzVmFsaWQodmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgY3BmY25wanZhbGlkYXRvcjoge1xuICAgICAgICAgICAgICAgIHZhbGlkOiBmYWxzZVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5sZW5ndGggPT09IDE0KSB7XG4gICAgICAgICAgaWYgKHRoaXMuY3BmQ25walNlcnZpY2UuY25waklzVmFsaWQodmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgY3BmY25wanZhbGlkYXRvcjoge1xuICAgICAgICAgICAgICAgIHZhbGlkOiBmYWxzZVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBjcGZjbnBqdmFsaWRhdG9yOiB7XG4gICAgICAgICAgdmFsaWQ6IGZhbHNlXG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfTtcbiAgfVxufVxuIl19