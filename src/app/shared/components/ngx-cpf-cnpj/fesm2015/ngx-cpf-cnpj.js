import { Injectable, Directive, HostListener, NgModule, defineInjectable } from '@angular/core';
import { NgModel, NG_VALIDATORS } from '@angular/forms';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgxCpfCnpjService {
    constructor() { }
    /**
     * @param {?} num
     * @return {?}
     */
    convertToCpfCnpj(num) {
        if (num) {
            num = num.toString();
            num = this.getDigitos(num);
            switch (num.length) {
                case 4:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 5:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 6:
                    num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                    break;
                case 7:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 8:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 9:
                    num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                    break;
                case 10:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{1})/, '$1.$2.$3-$4');
                    break;
                case 11:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
                    break;
                case 12:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})/, '$1.$2.$3/$4');
                    break;
                case 13:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                    break;
                case 14:
                    num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                    break;
            }
        }
        return num;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    getDigitos(value) {
        return value.replace(/\D/g, '');
    }
    /**
     * @param {?} value
     * @return {?}
     */
    cpfIsValid(value) {
        // Só faz a validação se o texto não for vazio.
        // Se for vazio, validar campo requerido não é a responsabilidade desta validação
        if (value.length > 0) {
            /** @type {?} */
            let IsValid = false;
            /** @type {?} */
            const cpf = this.getDigitos(value);
            if (cpf.length === 11) { // Se digitou o CPF por completo
                // Se digitou o CPF por completo
                // Verificação dos CPF's que não respeitam a regra de validação mas não são válidos
                /** @type {?} */
                const cpf_invalidos = ['00000000000', '11111111111', '22222222222', '33333333333',
                    '44444444444', '55555555555', '66666666666', '77777777777',
                    '88888888888', '99999999999'];
                for (let i = 0; i < 10; i++) {
                    if (cpf === cpf_invalidos[i]) {
                        return IsValid;
                    }
                }
                // Calculando o Primeiro Dígito Verificador
                /** @type {?} */
                let soma = 0;
                for (let i = 0; i < 9; i++) {
                    soma = soma + (parseInt(cpf.charAt(i), 10) * (10 - i));
                }
                /** @type {?} */
                let dv = 0;
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cpf.charAt(9), 10) !== dv) {
                    return IsValid;
                }
                // Calculando o Segundo Dígito Verificador
                soma = 0; // Soma para o CPF "ABC.DEF.GHI-XZ": (A*11)+(B*10)+...+(H*4)+(I*3)+(X*2)
                for (let i = 0; i < 10; i++) {
                    soma = soma + (parseInt(cpf.charAt(i), 10) * (11 - i));
                }
                dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cpf.charAt(10), 10) !== dv) {
                    return IsValid;
                }
                IsValid = true;
            }
            return IsValid;
        }
        return true;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    cnpjIsValid(value) {
        // Só faz a validação se o texto não for vazio.
        // Se for vazio, validar campo requerido não é a responsabilidade desta validação
        if (value.length > 0) {
            /** @type {?} */
            let IsValid = false;
            /** @type {?} */
            const cnpj = this.getDigitos(value);
            if (cnpj.length === 14) { // Se digitou o CNPJ por completo
                // Verificação do CNPJ que não respeita a regra de validação mas não são válidos
                if (cnpj === '00000000000000') {
                    return IsValid;
                }
                // Calculando o Primeiro Dígito Verificador
                /** @type {?} */
                let soma = 0;
                // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*5)+(B*4)+(C*3)+(D*2)+(E*9)+(F*8)+...+(K*3)+(L*2)
                /** @type {?} */
                let mult = 5;
                for (let i = 0; i < 12; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    mult--; // decrementa o multiplicador
                    if (mult === 1) {
                        mult = 9;
                    } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                }
                /** @type {?} */
                let dv = 0;
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cnpj.charAt(12), 10) !== dv) {
                    return IsValid;
                }
                // Calculando o Segundo Dígito Verificador
                soma = 0; // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*6)+(B*5)+(C*4)+(D*3)+(E*2)+(F*9)+...+(K*4)+(L*3)+(X*2)
                mult = 6; // multiplicador
                for (let i = 0; i < 13; i++) {
                    soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                    mult--; // decrementa o multiplicador
                    if (mult === 1) {
                        mult = 9;
                    } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                }
                dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                if ((soma % 11) > 1) {
                    dv = 11 - (soma % 11);
                }
                if (parseInt(cnpj.charAt(13), 10) !== dv) {
                    return IsValid;
                }
                IsValid = true;
            }
            return IsValid;
        }
        return true;
    }
}
NgxCpfCnpjService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgxCpfCnpjService.ctorParameters = () => [];
/** @nocollapse */ NgxCpfCnpjService.ngInjectableDef = defineInjectable({ factory: function NgxCpfCnpjService_Factory() { return new NgxCpfCnpjService(); }, token: NgxCpfCnpjService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CpfCnpjMaskDirective {
    /**
     * @param {?} model
     * @param {?} cpfCnpjService
     */
    constructor(model, cpfCnpjService) {
        this.model = model;
        this.cpfCnpjService = cpfCnpjService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onInput(event) {
        if (event.target.value.length <= 17) {
            this.model.valueAccessor.writeValue(this.cpfCnpjService.convertToCpfCnpj(event.target.value));
        }
    }
}
CpfCnpjMaskDirective.decorators = [
    { type: Directive, args: [{
                selector: '[nccCpfCnpjMask] [ngModel]',
                providers: [NgModel]
            },] }
];
/** @nocollapse */
CpfCnpjMaskDirective.ctorParameters = () => [
    { type: NgModel },
    { type: NgxCpfCnpjService }
];
CpfCnpjMaskDirective.propDecorators = {
    onInput: [{ type: HostListener, args: ['input', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CpfCnpjValidatorDirective {
    constructor() {
        this.cpfCnpjService = new NgxCpfCnpjService();
        this.validator = this.cpfCnpjValidator();
    }
    /**
     * @param {?} c
     * @return {?}
     */
    validate(c) {
        return this.validator(c);
    }
    /**
     * @return {?}
     */
    cpfCnpjValidator() {
        return (c) => {
            /** @type {?} */
            let value = (/** @type {?} */ (c.value));
            if (value != null) {
                value = this.cpfCnpjService.getDigitos(value);
                if (value.length === 11) {
                    if (this.cpfCnpjService.cpfIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
                else if (value.length === 14) {
                    if (this.cpfCnpjService.cnpjIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
            }
            return {
                cpfcnpjvalidator: {
                    valid: false
                }
            };
        };
    }
}
CpfCnpjValidatorDirective.decorators = [
    { type: Directive, args: [{
                selector: '[nccCpfCnpjValidator] [ngModel]',
                providers: [
                    {
                        provide: NG_VALIDATORS,
                        useExisting: CpfCnpjValidatorDirective,
                        multi: true
                    }
                ]
            },] }
];
/** @nocollapse */
CpfCnpjValidatorDirective.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgxCpfCnpjModule {
}
NgxCpfCnpjModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective],
                imports: [],
                exports: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { NgxCpfCnpjService, NgxCpfCnpjModule, CpfCnpjMaskDirective as ɵa, CpfCnpjValidatorDirective as ɵb };

//# sourceMappingURL=ngx-cpf-cnpj.js.map