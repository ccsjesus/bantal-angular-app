import { Validator, ValidatorFn, FormControl, ValidationErrors } from '@angular/forms';
export declare class CpfCnpjValidatorDirective implements Validator {
    validator: ValidatorFn;
    private cpfCnpjService;
    constructor();
    validate(c: FormControl): ValidationErrors;
    cpfCnpjValidator(): ValidatorFn;
}
