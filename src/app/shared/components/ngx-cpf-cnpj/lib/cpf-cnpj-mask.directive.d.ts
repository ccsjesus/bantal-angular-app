import { NgModel } from '@angular/forms';
import { NgxCpfCnpjService } from './ngx-cpf-cnpj.service';
export declare class CpfCnpjMaskDirective {
    private model;
    private cpfCnpjService;
    constructor(model: NgModel, cpfCnpjService: NgxCpfCnpjService);
    onInput(event: any): void;
}
