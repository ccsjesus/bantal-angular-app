export declare class NgxCpfCnpjService {
    constructor();
    convertToCpfCnpj(num: any): any;
    getDigitos(value: any): any;
    cpfIsValid(value: any): boolean;
    cnpjIsValid(value: any): boolean;
}
