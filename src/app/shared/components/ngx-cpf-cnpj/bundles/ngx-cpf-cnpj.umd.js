(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms')) :
    typeof define === 'function' && define.amd ? define('ngx-cpf-cnpj', ['exports', '@angular/core', '@angular/forms'], factory) :
    (factory((global['ngx-cpf-cnpj'] = {}),global.ng.core,global.ng.forms));
}(this, (function (exports,i0,forms) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgxCpfCnpjService = /** @class */ (function () {
        function NgxCpfCnpjService() {
        }
        /**
         * @param {?} num
         * @return {?}
         */
        NgxCpfCnpjService.prototype.convertToCpfCnpj = /**
         * @param {?} num
         * @return {?}
         */
            function (num) {
                if (num) {
                    num = num.toString();
                    num = this.getDigitos(num);
                    switch (num.length) {
                        case 4:
                            num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                            break;
                        case 5:
                            num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                            break;
                        case 6:
                            num = num.replace(/(\d+)(\d{3})/, '$1.$2');
                            break;
                        case 7:
                            num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                            break;
                        case 8:
                            num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                            break;
                        case 9:
                            num = num.replace(/(\d+)(\d{3})(\d{3})/, '$1.$2.$3');
                            break;
                        case 10:
                            num = num.replace(/(\d+)(\d{3})(\d{3})(\d{1})/, '$1.$2.$3-$4');
                            break;
                        case 11:
                            num = num.replace(/(\d+)(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
                            break;
                        case 12:
                            num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})/, '$1.$2.$3/$4');
                            break;
                        case 13:
                            num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                            break;
                        case 14:
                            num = num.replace(/(\d+)(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
                            break;
                    }
                }
                return num;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        NgxCpfCnpjService.prototype.getDigitos = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                return value.replace(/\D/g, '');
            };
        /**
         * @param {?} value
         * @return {?}
         */
        NgxCpfCnpjService.prototype.cpfIsValid = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                // Só faz a validação se o texto não for vazio.
                // Se for vazio, validar campo requerido não é a responsabilidade desta validação
                if (value.length > 0) {
                    /** @type {?} */
                    var IsValid = false;
                    /** @type {?} */
                    var cpf = this.getDigitos(value);
                    if (cpf.length === 11) { // Se digitou o CPF por completo
                        // Se digitou o CPF por completo
                        // Verificação dos CPF's que não respeitam a regra de validação mas não são válidos
                        /** @type {?} */
                        var cpf_invalidos = ['00000000000', '11111111111', '22222222222', '33333333333',
                            '44444444444', '55555555555', '66666666666', '77777777777',
                            '88888888888', '99999999999'];
                        for (var i = 0; i < 10; i++) {
                            if (cpf === cpf_invalidos[i]) {
                                return IsValid;
                            }
                        }
                        // Calculando o Primeiro Dígito Verificador
                        /** @type {?} */
                        var soma = 0;
                        for (var i = 0; i < 9; i++) {
                            soma = soma + (parseInt(cpf.charAt(i), 10) * (10 - i));
                        }
                        /** @type {?} */
                        var dv = 0;
                        if ((soma % 11) > 1) {
                            dv = 11 - (soma % 11);
                        }
                        if (parseInt(cpf.charAt(9), 10) !== dv) {
                            return IsValid;
                        }
                        // Calculando o Segundo Dígito Verificador
                        soma = 0; // Soma para o CPF "ABC.DEF.GHI-XZ": (A*11)+(B*10)+...+(H*4)+(I*3)+(X*2)
                        for (var i = 0; i < 10; i++) {
                            soma = soma + (parseInt(cpf.charAt(i), 10) * (11 - i));
                        }
                        dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                        if ((soma % 11) > 1) {
                            dv = 11 - (soma % 11);
                        }
                        if (parseInt(cpf.charAt(10), 10) !== dv) {
                            return IsValid;
                        }
                        IsValid = true;
                    }
                    return IsValid;
                }
                return true;
            };
        /**
         * @param {?} value
         * @return {?}
         */
        NgxCpfCnpjService.prototype.cnpjIsValid = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                // Só faz a validação se o texto não for vazio.
                // Se for vazio, validar campo requerido não é a responsabilidade desta validação
                if (value.length > 0) {
                    /** @type {?} */
                    var IsValid = false;
                    /** @type {?} */
                    var cnpj = this.getDigitos(value);
                    if (cnpj.length === 14) { // Se digitou o CNPJ por completo
                        // Verificação do CNPJ que não respeita a regra de validação mas não são válidos
                        if (cnpj === '00000000000000') {
                            return IsValid;
                        }
                        // Calculando o Primeiro Dígito Verificador
                        /** @type {?} */
                        var soma = 0;
                        // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*5)+(B*4)+(C*3)+(D*2)+(E*9)+(F*8)+...+(K*3)+(L*2)
                        /** @type {?} */
                        var mult = 5;
                        for (var i = 0; i < 12; i++) {
                            soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                            mult--; // decrementa o multiplicador
                            if (mult === 1) {
                                mult = 9;
                            } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                        }
                        /** @type {?} */
                        var dv = 0;
                        if ((soma % 11) > 1) {
                            dv = 11 - (soma % 11);
                        }
                        if (parseInt(cnpj.charAt(12), 10) !== dv) {
                            return IsValid;
                        }
                        // Calculando o Segundo Dígito Verificador
                        soma = 0; // Soma para o CNPJ "AB.CDE.FGH/IJKL-XZ": (A*6)+(B*5)+(C*4)+(D*3)+(E*2)+(F*9)+...+(K*4)+(L*3)+(X*2)
                        mult = 6; // multiplicador
                        for (var i = 0; i < 13; i++) {
                            soma = soma + (parseInt(cnpj.charAt(i), 10) * (mult));
                            mult--; // decrementa o multiplicador
                            if (mult === 1) {
                                mult = 9;
                            } // volta o multiplicador para o valor 9 (para decrementar até o valor 2)
                        }
                        dv = 0; // Segundo dígito verificador (será zero se o resto da divisão de soma por 11 for < 2)
                        if ((soma % 11) > 1) {
                            dv = 11 - (soma % 11);
                        }
                        if (parseInt(cnpj.charAt(13), 10) !== dv) {
                            return IsValid;
                        }
                        IsValid = true;
                    }
                    return IsValid;
                }
                return true;
            };
        NgxCpfCnpjService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        NgxCpfCnpjService.ctorParameters = function () { return []; };
        /** @nocollapse */ NgxCpfCnpjService.ngInjectableDef = i0.defineInjectable({ factory: function NgxCpfCnpjService_Factory() { return new NgxCpfCnpjService(); }, token: NgxCpfCnpjService, providedIn: "root" });
        return NgxCpfCnpjService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CpfCnpjMaskDirective = /** @class */ (function () {
        function CpfCnpjMaskDirective(model, cpfCnpjService) {
            this.model = model;
            this.cpfCnpjService = cpfCnpjService;
        }
        /**
         * @param {?} event
         * @return {?}
         */
        CpfCnpjMaskDirective.prototype.onInput = /**
         * @param {?} event
         * @return {?}
         */
            function (event) {
                if (event.target.value.length <= 17) {
                    this.model.valueAccessor.writeValue(this.cpfCnpjService.convertToCpfCnpj(event.target.value));
                }
            };
        CpfCnpjMaskDirective.decorators = [
            { type: i0.Directive, args: [{
                        selector: '[nccCpfCnpjMask] [ngModel]',
                        providers: [forms.NgModel]
                    },] }
        ];
        /** @nocollapse */
        CpfCnpjMaskDirective.ctorParameters = function () {
            return [
                { type: forms.NgModel },
                { type: NgxCpfCnpjService }
            ];
        };
        CpfCnpjMaskDirective.propDecorators = {
            onInput: [{ type: i0.HostListener, args: ['input', ['$event'],] }]
        };
        return CpfCnpjMaskDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var CpfCnpjValidatorDirective = /** @class */ (function () {
        function CpfCnpjValidatorDirective() {
            this.cpfCnpjService = new NgxCpfCnpjService();
            this.validator = this.cpfCnpjValidator();
        }
        /**
         * @param {?} c
         * @return {?}
         */
        CpfCnpjValidatorDirective.prototype.validate = /**
         * @param {?} c
         * @return {?}
         */
            function (c) {
                return this.validator(c);
            };
        /**
         * @return {?}
         */
        CpfCnpjValidatorDirective.prototype.cpfCnpjValidator = /**
         * @return {?}
         */
            function () {
                var _this = this;
                return function (c) {
                    /** @type {?} */
                    var value = ( /** @type {?} */(c.value));
                    if (value != null) {
                        value = _this.cpfCnpjService.getDigitos(value);
                        if (value.length === 11) {
                            if (_this.cpfCnpjService.cpfIsValid(value)) {
                                return null;
                            }
                            else {
                                return {
                                    cpfcnpjvalidator: {
                                        valid: false
                                    }
                                };
                            }
                        }
                        else if (value.length === 14) {
                            if (_this.cpfCnpjService.cnpjIsValid(value)) {
                                return null;
                            }
                            else {
                                return {
                                    cpfcnpjvalidator: {
                                        valid: false
                                    }
                                };
                            }
                        }
                    }
                    return {
                        cpfcnpjvalidator: {
                            valid: false
                        }
                    };
                };
            };
        CpfCnpjValidatorDirective.decorators = [
            { type: i0.Directive, args: [{
                        selector: '[nccCpfCnpjValidator] [ngModel]',
                        providers: [
                            {
                                provide: forms.NG_VALIDATORS,
                                useExisting: CpfCnpjValidatorDirective,
                                multi: true
                            }
                        ]
                    },] }
        ];
        /** @nocollapse */
        CpfCnpjValidatorDirective.ctorParameters = function () { return []; };
        return CpfCnpjValidatorDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgxCpfCnpjModule = /** @class */ (function () {
        function NgxCpfCnpjModule() {
        }
        NgxCpfCnpjModule.decorators = [
            { type: i0.NgModule, args: [{
                        declarations: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective],
                        imports: [],
                        exports: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective]
                    },] }
        ];
        return NgxCpfCnpjModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.NgxCpfCnpjService = NgxCpfCnpjService;
    exports.NgxCpfCnpjModule = NgxCpfCnpjModule;
    exports.ɵa = CpfCnpjMaskDirective;
    exports.ɵb = CpfCnpjValidatorDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=ngx-cpf-cnpj.umd.js.map