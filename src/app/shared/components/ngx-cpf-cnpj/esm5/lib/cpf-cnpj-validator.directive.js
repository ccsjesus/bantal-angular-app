/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive } from '@angular/core';
import { NG_VALIDATORS } from '@angular/forms';
import { NgxCpfCnpjService } from './ngx-cpf-cnpj.service';
var CpfCnpjValidatorDirective = /** @class */ (function () {
    function CpfCnpjValidatorDirective() {
        this.cpfCnpjService = new NgxCpfCnpjService();
        this.validator = this.cpfCnpjValidator();
    }
    /**
     * @param {?} c
     * @return {?}
     */
    CpfCnpjValidatorDirective.prototype.validate = /**
     * @param {?} c
     * @return {?}
     */
    function (c) {
        return this.validator(c);
    };
    /**
     * @return {?}
     */
    CpfCnpjValidatorDirective.prototype.cpfCnpjValidator = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return function (c) {
            /** @type {?} */
            var value = (/** @type {?} */ (c.value));
            if (value != null) {
                value = _this.cpfCnpjService.getDigitos(value);
                if (value.length === 11) {
                    if (_this.cpfCnpjService.cpfIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
                else if (value.length === 14) {
                    if (_this.cpfCnpjService.cnpjIsValid(value)) {
                        return null;
                    }
                    else {
                        return {
                            cpfcnpjvalidator: {
                                valid: false
                            }
                        };
                    }
                }
            }
            return {
                cpfcnpjvalidator: {
                    valid: false
                }
            };
        };
    };
    CpfCnpjValidatorDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[nccCpfCnpjValidator] [ngModel]',
                    providers: [
                        {
                            provide: NG_VALIDATORS,
                            useExisting: CpfCnpjValidatorDirective,
                            multi: true
                        }
                    ]
                },] }
    ];
    /** @nocollapse */
    CpfCnpjValidatorDirective.ctorParameters = function () { return []; };
    return CpfCnpjValidatorDirective;
}());
export { CpfCnpjValidatorDirective };
if (false) {
    /** @type {?} */
    CpfCnpjValidatorDirective.prototype.validator;
    /**
     * @type {?}
     * @private
     */
    CpfCnpjValidatorDirective.prototype.cpfCnpjService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3BmLWNucGotdmFsaWRhdG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jcGYtY25wai8iLCJzb3VyY2VzIjpbImxpYi9jcGYtY25wai12YWxpZGF0b3IuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxhQUFhLEVBQXVDLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFM0Q7SUFhRTtRQURRLG1CQUFjLEdBQXNCLElBQUksaUJBQWlCLEVBQUUsQ0FBQztRQUVsRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzNDLENBQUM7Ozs7O0lBRUQsNENBQVE7Ozs7SUFBUixVQUFTLENBQWM7UUFDckIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCxvREFBZ0I7OztJQUFoQjtRQUFBLGlCQWtDQztRQWpDQyxPQUFPLFVBQUMsQ0FBYzs7Z0JBQ2hCLEtBQUssR0FBVyxtQkFBUSxDQUFDLENBQUMsS0FBSyxFQUFBO1lBQ25DLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDakIsS0FBSyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM5QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssRUFBRSxFQUFFO29CQUN2QixJQUFJLEtBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUN6QyxPQUFPLElBQUksQ0FBQztxQkFDYjt5QkFBTTt3QkFDTCxPQUFPOzRCQUNMLGdCQUFnQixFQUFFO2dDQUNoQixLQUFLLEVBQUUsS0FBSzs2QkFDYjt5QkFDRixDQUFDO3FCQUNIO2lCQUNGO3FCQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7b0JBQzlCLElBQUksS0FBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQzFDLE9BQU8sSUFBSSxDQUFDO3FCQUNiO3lCQUFNO3dCQUNMLE9BQU87NEJBQ0wsZ0JBQWdCLEVBQUU7Z0NBQ2hCLEtBQUssRUFBRSxLQUFLOzZCQUNiO3lCQUNGLENBQUM7cUJBQ0g7aUJBQ0Y7YUFDRjtZQUVELE9BQU87Z0JBQ0wsZ0JBQWdCLEVBQUU7b0JBQ2hCLEtBQUssRUFBRSxLQUFLO2lCQUNiO2FBQ0YsQ0FBQztRQUNKLENBQUMsQ0FBQztJQUNKLENBQUM7O2dCQXZERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGlDQUFpQztvQkFDM0MsU0FBUyxFQUFFO3dCQUNUOzRCQUNFLE9BQU8sRUFBRSxhQUFhOzRCQUN0QixXQUFXLEVBQUUseUJBQXlCOzRCQUN0QyxLQUFLLEVBQUUsSUFBSTt5QkFDWjtxQkFBQztpQkFDTDs7OztJQWdERCxnQ0FBQztDQUFBLEFBeERELElBd0RDO1NBL0NZLHlCQUF5Qjs7O0lBRXBDLDhDQUF1Qjs7Ozs7SUFDdkIsbURBQW9FIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOR19WQUxJREFUT1JTLCBWYWxpZGF0b3IsIFZhbGlkYXRvckZuLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE5neENwZkNucGpTZXJ2aWNlIH0gZnJvbSAnLi9uZ3gtY3BmLWNucGouc2VydmljZSc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tuY2NDcGZDbnBqVmFsaWRhdG9yXSBbbmdNb2RlbF0nLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxJREFUT1JTLFxuICAgICAgdXNlRXhpc3Rpbmc6IENwZkNucGpWYWxpZGF0b3JEaXJlY3RpdmUsXG4gICAgICBtdWx0aTogdHJ1ZVxuICAgIH1dXG59KVxuZXhwb3J0IGNsYXNzIENwZkNucGpWYWxpZGF0b3JEaXJlY3RpdmUgaW1wbGVtZW50cyBWYWxpZGF0b3IgIHtcblxuICB2YWxpZGF0b3I6IFZhbGlkYXRvckZuO1xuICBwcml2YXRlIGNwZkNucGpTZXJ2aWNlOiBOZ3hDcGZDbnBqU2VydmljZSA9IG5ldyBOZ3hDcGZDbnBqU2VydmljZSgpO1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnZhbGlkYXRvciA9IHRoaXMuY3BmQ25walZhbGlkYXRvcigpO1xuICB9XG5cbiAgdmFsaWRhdGUoYzogRm9ybUNvbnRyb2wpIHtcbiAgICByZXR1cm4gdGhpcy52YWxpZGF0b3IoYyk7XG4gIH1cblxuICBjcGZDbnBqVmFsaWRhdG9yKCk6IFZhbGlkYXRvckZuIHtcbiAgICByZXR1cm4gKGM6IEZvcm1Db250cm9sKSA9PiB7XG4gICAgICBsZXQgdmFsdWU6IHN0cmluZyA9IDxzdHJpbmc+Yy52YWx1ZTtcbiAgICAgIGlmICh2YWx1ZSAhPSBudWxsKSB7XG4gICAgICAgIHZhbHVlID0gdGhpcy5jcGZDbnBqU2VydmljZS5nZXREaWdpdG9zKHZhbHVlKTtcbiAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA9PT0gMTEpIHtcbiAgICAgICAgICBpZiAodGhpcy5jcGZDbnBqU2VydmljZS5jcGZJc1ZhbGlkKHZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIGNwZmNucGp2YWxpZGF0b3I6IHtcbiAgICAgICAgICAgICAgICB2YWxpZDogZmFsc2VcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAodmFsdWUubGVuZ3RoID09PSAxNCkge1xuICAgICAgICAgIGlmICh0aGlzLmNwZkNucGpTZXJ2aWNlLmNucGpJc1ZhbGlkKHZhbHVlKSkge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIGNwZmNucGp2YWxpZGF0b3I6IHtcbiAgICAgICAgICAgICAgICB2YWxpZDogZmFsc2VcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgY3BmY25wanZhbGlkYXRvcjoge1xuICAgICAgICAgIHZhbGlkOiBmYWxzZVxuICAgICAgICB9XG4gICAgICB9O1xuICAgIH07XG4gIH1cbn1cbiJdfQ==