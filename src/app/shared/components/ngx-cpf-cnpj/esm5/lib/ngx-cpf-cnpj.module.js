/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CpfCnpjMaskDirective } from './cpf-cnpj-mask.directive';
import { CpfCnpjValidatorDirective } from './cpf-cnpj-validator.directive';
var NgxCpfCnpjModule = /** @class */ (function () {
    function NgxCpfCnpjModule() {
    }
    NgxCpfCnpjModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective],
                    imports: [],
                    exports: [CpfCnpjMaskDirective, CpfCnpjValidatorDirective]
                },] }
    ];
    return NgxCpfCnpjModule;
}());
export { NgxCpfCnpjModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LWNwZi1jbnBqLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1jcGYtY25wai8iLCJzb3VyY2VzIjpbImxpYi9uZ3gtY3BmLWNucGoubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBRTNFO0lBQUE7SUFNZ0MsQ0FBQzs7Z0JBTmhDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSx5QkFBeUIsQ0FBQztvQkFDL0QsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFLENBQUMsb0JBQW9CLEVBQUUseUJBQXlCLENBQUM7aUJBQzNEOztJQUMrQix1QkFBQztDQUFBLEFBTmpDLElBTWlDO1NBQXBCLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDcGZDbnBqTWFza0RpcmVjdGl2ZSB9IGZyb20gJy4vY3BmLWNucGotbWFzay5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgQ3BmQ25walZhbGlkYXRvckRpcmVjdGl2ZSB9IGZyb20gJy4vY3BmLWNucGotdmFsaWRhdG9yLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0NwZkNucGpNYXNrRGlyZWN0aXZlLCBDcGZDbnBqVmFsaWRhdG9yRGlyZWN0aXZlXSxcbiAgaW1wb3J0czogW1xuICBdLFxuICBleHBvcnRzOiBbQ3BmQ25wak1hc2tEaXJlY3RpdmUsIENwZkNucGpWYWxpZGF0b3JEaXJlY3RpdmVdXG59KVxuZXhwb3J0IGNsYXNzIE5neENwZkNucGpNb2R1bGUgeyB9XG4iXX0=