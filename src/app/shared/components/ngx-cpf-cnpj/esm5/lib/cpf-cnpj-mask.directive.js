/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener } from '@angular/core';
import { NgModel } from '@angular/forms';
import { NgxCpfCnpjService } from './ngx-cpf-cnpj.service';
var CpfCnpjMaskDirective = /** @class */ (function () {
    function CpfCnpjMaskDirective(model, cpfCnpjService) {
        this.model = model;
        this.cpfCnpjService = cpfCnpjService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    CpfCnpjMaskDirective.prototype.onInput = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.target.value.length <= 17) {
            this.model.valueAccessor.writeValue(this.cpfCnpjService.convertToCpfCnpj(event.target.value));
        }
    };
    CpfCnpjMaskDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[nccCpfCnpjMask] [ngModel]',
                    providers: [NgModel]
                },] }
    ];
    /** @nocollapse */
    CpfCnpjMaskDirective.ctorParameters = function () { return [
        { type: NgModel },
        { type: NgxCpfCnpjService }
    ]; };
    CpfCnpjMaskDirective.propDecorators = {
        onInput: [{ type: HostListener, args: ['input', ['$event'],] }]
    };
    return CpfCnpjMaskDirective;
}());
export { CpfCnpjMaskDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CpfCnpjMaskDirective.prototype.model;
    /**
     * @type {?}
     * @private
     */
    CpfCnpjMaskDirective.prototype.cpfCnpjService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3BmLWNucGotbWFzay5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZ3gtY3BmLWNucGovIiwic291cmNlcyI6WyJsaWIvY3BmLWNucGotbWFzay5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3hELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN6QyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMzRDtJQUtFLDhCQUFvQixLQUFjLEVBQVUsY0FBaUM7UUFBekQsVUFBSyxHQUFMLEtBQUssQ0FBUztRQUFVLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtJQUFJLENBQUM7Ozs7O0lBRS9DLHNDQUFPOzs7O0lBQTFDLFVBQTJDLEtBQUs7UUFDOUMsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFFO1lBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUMvRjtJQUNILENBQUM7O2dCQVhGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsNEJBQTRCO29CQUN0QyxTQUFTLEVBQUUsQ0FBQyxPQUFPLENBQUM7aUJBQ3JCOzs7O2dCQUxRLE9BQU87Z0JBQ1AsaUJBQWlCOzs7MEJBUXZCLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBTW5DLDJCQUFDO0NBQUEsQUFiRCxJQWFDO1NBVFksb0JBQW9COzs7Ozs7SUFDbkIscUNBQXNCOzs7OztJQUFFLDhDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ01vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTmd4Q3BmQ25walNlcnZpY2UgfSBmcm9tICcuL25neC1jcGYtY25wai5zZXJ2aWNlJztcbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tuY2NDcGZDbnBqTWFza10gW25nTW9kZWxdJyxcbiAgcHJvdmlkZXJzOiBbTmdNb2RlbF1cbn0pXG5leHBvcnQgY2xhc3MgQ3BmQ25wak1hc2tEaXJlY3RpdmUge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIG1vZGVsOiBOZ01vZGVsLCBwcml2YXRlIGNwZkNucGpTZXJ2aWNlOiBOZ3hDcGZDbnBqU2VydmljZSkgeyB9XG5cbiAgQEhvc3RMaXN0ZW5lcignaW5wdXQnLCBbJyRldmVudCddKSBvbklucHV0KGV2ZW50KSB7XG4gICAgaWYgKGV2ZW50LnRhcmdldC52YWx1ZS5sZW5ndGggPD0gMTcpIHtcbiAgICAgIHRoaXMubW9kZWwudmFsdWVBY2Nlc3Nvci53cml0ZVZhbHVlKHRoaXMuY3BmQ25walNlcnZpY2UuY29udmVydFRvQ3BmQ25waihldmVudC50YXJnZXQudmFsdWUpKTtcbiAgICB9XG4gIH1cblxufVxuIl19