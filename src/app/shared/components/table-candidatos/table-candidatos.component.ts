import { Component, Input, OnInit } from '@angular/core';
import { Usuario } from '../../../pages/models/usuario.model';
import { CarService } from '../../services/carservices';
import { MessageService } from '../../services/message';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-table-candidatos',
  templateUrl: './table-candidatos.component.html',
  styleUrls: ['./table-candidatos.component.scss']
})
export class TableCandidatosComponent implements OnInit {

  usuarios: Usuario[];

  @Input('parentForm')
  public parentForm: FormGroup;

  @Input('parentFormSecundario')
  public parentFormSecundario: FormGroup;

  cols: any[];

  selectedCar1: Usuario;

  selectedCar2: Usuario;

  selectedCar3: Usuario;

  selectedCar4: Usuario;

  selectedCars1: Usuario[];

  selectedCars2: Usuario[];

  selectedCars3: Usuario[];

  constructor(private carService: CarService, private messageService: MessageService) { }

  ngOnInit() {
      this.carService.getCars().subscribe(valor => this.usuarios = valor.data,
                                          error => this.messageService.add({severity:'info', summary:'Usuario Selecionado', detail:'Vin: ' + error}));

      this.cols = [
          { field: 'cpf', header: 'CPF' },
          { field: 'nome', header: 'Nome' },
          { field: 'telefone', header: 'Telefone' },
          { field: 'email', header: 'Email' }
      ];
  }

  selecionarCandidato(user: Usuario) {
    this.parentFormSecundario.get('candidato').setValue(user);
  }

  onRowSelect(event) {
      this.messageService.add({severity:'info', summary:'Car Selected', detail:'Vin: ' + event.data.vin});
  }

  onRowUnselect(event) {
      this.messageService.add({severity:'info', summary:'Car Unselected', detail:'Vin: ' + event.data.vin});
  }
}
