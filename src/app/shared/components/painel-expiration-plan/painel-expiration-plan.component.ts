import { ExpiracaoPlanoModel } from './../../../pages/models/expiracao-plano.model';
import { DadosServices } from './../../services/dados.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'painel-expiration-plan',
  templateUrl: './painel-expiration-plan.component.html',
  styleUrls: ['./painel-expiration-plan.component.css']
})
export class PainelExpirationPlanComponent implements OnInit {

  exibirInformacao: boolean = false;
  textoCarregando: string = 'Informação sobre seu plano!';
  exibirLoading: Boolean = true;
  textoAguarde: string = 'Seu plano irá expirar em: ';
  diasExpirar: number;
  expiracaoPlano: ExpiracaoPlanoModel;

  constructor(protected dadosServices: DadosServices) { }

  ngOnInit(): void {
    this.dadosServices.obterExpiracaoPlano().subscribe((expiracao) => {
      this.expiracaoPlano = expiracao;
      this.exibirInformacao = expiracao.exibirAlertaExpiracao
      if(this.expiracaoPlano.diasExpirar == 0 ){
         this.textoAguarde = 'Seu plano irá expirar hoje! ';
      } else if(this.expiracaoPlano.diasExpirar <= -1){
         this.textoAguarde = 'Seu plano expirou, realize a renovação para continuar utilizando o sistema ';
      } else {
        this.textoAguarde = 'Seu plano irá expirar em: ' + this.expiracaoPlano.diasExpirar + ' dias !';
      }
    });
  }

}
