import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ImageService } from '../../services/image.services';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import {user64} from '../../../pages/constantes/user';

class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';

  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'image-upload-perfil',
  templateUrl: './image-upload-perfil.component.html',
  styleUrls: ['./image-upload-perfil.component.css']
})
export class ImageUploadPerfilComponent implements OnInit, AfterViewInit {

   selectedFile: ImageSnippet;

  @ViewChild('photo', { static: true }) img: ElementRef;


  @Input('parentForm')
  public parentForm: FormGroup;

  constructor(private imageService: ImageService){}

  ngAfterViewInit() {

    if(this.parentForm.controls.foto.value !== ''){
      this.img.nativeElement.src = this.parentForm.controls.foto.value;
    }

    this.parentForm.controls.foto.valueChanges.subscribe(valor => {

      let novoValor = valor + "";
      if( novoValor.startsWith("http")){
        this.img.nativeElement.src = valor;
      } else if(novoValor.startsWith("data")){
        this.img.nativeElement.src = valor;
      } else if(novoValor !== ""){
        this.img.nativeElement.src = 'data:image/jpg;base64,' +valor;
      } else {
        this.img.nativeElement.src = user64;
      }
    });
  }
  ngOnInit(): void {

  }

  private onSuccess(imagem) {
    Swal.fire(
      'Ótimo!',
      'Sua Foto foi publicada com sucesso!',
      'success'
    ).then((resultado) => {
      this.selectedFile.pending = false;
      this.selectedFile.status = 'ok';
      this.parentForm.controls.foto.setValue(imagem);
    })
  }

  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {


      //if (result.length * 2  > 2**21) reject('File exceeds the maximum size');

      this.selectedFile = new ImageSnippet(event.target.result, file);
      let img = this.selectedFile.src;
      const uploadImageData = new FormData();
      uploadImageData.append('imageFile', this.selectedFile.file, this.selectedFile.file.name);

      this.selectedFile.pending = true;
      this.imageService.uploadImagePerfil(uploadImageData).subscribe(
        (res) => {
          this.onSuccess(img);
        },
        (err) => {
          this.onError();
        })
    });

    reader.readAsDataURL(file);
  }
}
