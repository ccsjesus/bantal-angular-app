
 export class Taxonomia {
    constructor(public termId?: string,
                public name?: string,
                public slug?: string) {
      
    }

  static fromJson(jsonData: any): Taxonomia {
    return Object.assign(new Taxonomia(), jsonData);
}
}
