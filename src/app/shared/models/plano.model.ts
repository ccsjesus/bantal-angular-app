import { BaseResourceModel } from './base-resource.model';

export class PlanoModel extends BaseResourceModel {
    constructor(public cdPlano?: string,
            public nome?: string,
            public slug?: string,
            public valor?: string,
            public parcelas?: string,
            public desc_tempo?: string,
            public duracao?: string,
            public tempo_alerta?: string,
            public perfil?: string) {
        super()
    }

    static fromJson(jsonData: any): PlanoModel {
        return Object.assign(new PlanoModel(), jsonData);
    }

}
