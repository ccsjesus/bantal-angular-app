
 export class Estado {
    constructor(public sigla?: string,
                public nome?: string,
                public cidades?: Array<string>) {
      
    }

  static fromJson(jsonData: any): Estado {
    return Object.assign(new Estado(), jsonData);
}
}
