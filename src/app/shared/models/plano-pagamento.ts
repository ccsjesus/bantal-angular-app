import { BaseResourceModel } from './base-resource.model';

export class PlanoPagamento extends BaseResourceModel {
    constructor(public cdPlano?: string,
            public cdUsuario?: string,
            public cdTransacao?: string,
            public cdSolicitacaoTransacao?: string,
            public dataConfirmacaoPagamento?: string,
            public statusPagamento?: string) {
        super()
    }

    static fromJson(jsonData: any): PlanoPagamento {
        return Object.assign(new PlanoPagamento(), jsonData);
    }

}
