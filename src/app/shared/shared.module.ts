import { PlanoServices } from './services/plano.services';
import { TecnologiaServices } from './services/tecnologia.services';
import { ExperienciaProfissionalServices } from './services/experiencia-profissional.services';
import { TipoVagaServices } from './services/tipo.vaga.services';
import { CNHServices } from './services/cnh.services';
import { FuncaoCargoServices } from './services/funcao-cargo.services';
import { NivelCargoServices } from './services/nivel-cargo.services';
import { SuporteServices } from './services/suporte.services';
import { ImageUploadPerfilComponent } from './components/image-upload-perfil/image-upload-perfil.component';
import { MensagemCandidatoService } from './services/mensagem-candidato.services';
import { MensagemFromRecruiterService } from './services/mensagem-from-recruiter.services';
import { MercadopagoService } from './services/mercadopago.services';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { NbAccordionModule, NbChatModule, NbToastrModule, NbToastrService, NbCardModule, NbButtonModule, NbIconModule, NbActionsModule, NbSelectModule, NbStepperModule, NbInputModule, NbCheckboxModule, NbRadioModule, NbListModule, NbTabsetModule, NbAlertModule, NbTooltipModule, NbPopoverModule } from '@nebular/theme';
import { MessageService } from './services/message';
import { TableVagasComponent } from './components/table-vagas/table-vagas.component';
import { VagaServices } from './services/vaga.service';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { ThemeModule } from '../@theme/theme.module';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { TableColaboradorComponent } from './components/table-colaborador/table-colaborador.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AccordionModule } from 'primeng/accordion';
import { CardCargosComponent } from './components/card-cargos/card-cargos.component';
import { TableCandidatosComponent } from './components/table-candidatos/table-candidatos.component';
import { EditorModule } from 'primeng/editor';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { RatingComponent } from './components/rating/rating.component';
import { RelatoriosComponent } from './components/relatorios/relatorios.component';
import { LoadingComponent } from './components/loading/loading.component';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxSpinnerModule } from "ngx-spinner";
import { PaginateComponent } from './components/paginate/paginate.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { ServerErrorMessagesComponent } from './components/server-error-messages/server-error-messages.component';
import { ToastrModule } from 'ngx-toastr';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { InputMaskModule } from 'primeng/inputmask';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgBrazil } from 'ng-brazil';
import { TextMaskModule } from 'angular2-text-mask';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PopoverModule } from "ngx-smart-popover";
import { NumberDirective } from './diretivas/numbers-only.directive';
import { CpfCnpjDirective } from './diretivas/cpf-cnpj.directive';
import { CurriculoServices } from './services/curriculo.services';
import { GroupNbCheckboxComponent } from './components/group-checkbox/nb-group-checkbox.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { AreaAtuacaoServices } from './services/area.atuacao.services';
import { HabilidadeServices } from './services/habilidades.services';
import { SalarioServices } from './services/salario.services';
import { PanelModule } from 'primeng/panel';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ImageService } from './services/image.services';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import {CarouselModule} from 'primeng/carousel';
import { VagasCandidatoServices } from './services/vaga.candidato.service';
import { VagasEmpresaServices } from './services/vagas.empresa.service';
import { ImageUploadEmpresaComponent } from './components/image-upload-empresa/image-upload-empresa.component';
import { PagseguroService } from './services/pagseguro.services';
import { DadosServices } from './services/dados.service';
import { PainelExpirationPlanComponent } from './components/painel-expiration-plan/painel-expiration-plan.component';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ChartModule } from 'primeng/chart';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {CardModule} from 'primeng/card';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import {DataViewModule} from 'primeng/dataview';


export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  imports: [
    DataViewModule,
    NgxMaskModule.forRoot(),
    CardModule,
    DragDropModule,
    Ng2SmartTableModule,
    ChartModule,
    NbPopoverModule,
    InputSwitchModule,
    NbAccordionModule,
    SplitButtonModule,
    PanelModule,
    SelectButtonModule,
    NbAlertModule,
    ReactiveFormsModule,
    InputTextModule,
    NbButtonModule,
    NbCardModule,
    ThemeModule,
    TableModule,
    ButtonModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    InputTextareaModule,
    FormsModule,
    DropdownModule,
    ButtonModule,
    NbEvaIconsModule,
    ToastrModule.forRoot({
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      positionClass: 'toast-bottom-right',
    }),
    NbToastrModule.forRoot(),
    NbIconModule,
    EditorModule,
    CalendarModule,
    CheckboxModule,

    NbActionsModule,
    NbSelectModule,
    NbButtonModule,
    NbInputModule,
    NbCheckboxModule,
    NbRadioModule,
    PdfViewerModule,
    AccordionModule,
    NbListModule,
    NbTabsetModule,
    NgxLoadingModule.forRoot({}),
    NgxSpinnerModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    SweetAlert2Module,
    InputMaskModule,
    NbTooltipModule,
    NgBrazil,
    TextMaskModule,
    PopoverModule,
    CarouselModule,
    NbAlertModule,
    NbChatModule
  ],
  declarations: [
    GroupNbCheckboxComponent,
    PaginateComponent,
    TableColaboradorComponent,
    TableVagasComponent,
    PageHeaderComponent,
    CardCargosComponent,
    TableCandidatosComponent,
    RatingComponent,
    RelatoriosComponent,
    LoadingComponent,
    ServerErrorMessagesComponent,
    NumberDirective,
    CpfCnpjDirective,
    ImageUploadComponent,
    ImageUploadEmpresaComponent,
    PainelExpirationPlanComponent,
    ImageUploadPerfilComponent

  ],
  exports: [
    // shared
    DataViewModule,
    NgxMaskModule,
    CardModule,
    DragDropModule,
    Ng2SmartTableModule,
    ChartModule,
    NbPopoverModule,
    InputSwitchModule,
    NbAccordionModule,
    SplitButtonModule,
    PanelModule,
    SelectButtonModule,
    NbAlertModule,
    InputTextModule,
    NbButtonModule,
    NbCardModule,
    ThemeModule,
    TableModule,
    ButtonModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    InputTextareaModule,
    FormsModule,
    DropdownModule,
    ButtonModule,
    ToastrModule,
    NbToastrModule,
    NbIconModule,
    EditorModule,
    CalendarModule,
    CheckboxModule,
    NgxLoadingModule,

    NbActionsModule,
    NbSelectModule,
    NbButtonModule,
    NbInputModule,
    NbCheckboxModule,
    NbRadioModule,
    PdfViewerModule,
    AccordionModule,
    NbListModule,
    NbTabsetModule,
    NgxSpinnerModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    InputMaskModule,
    NbEvaIconsModule,
    NbTooltipModule,
    NgBrazil,
    TextMaskModule,
    NgbTooltipModule,
    PopoverModule,
    CarouselModule,
    NbAlertModule,
    NbChatModule,
    // shared components

    PageHeaderComponent,
    TableVagasComponent,
    TableColaboradorComponent,
    CardCargosComponent,
    TableCandidatosComponent,
    RatingComponent,
    RelatoriosComponent,
    LoadingComponent,
    PaginateComponent,
    ServerErrorMessagesComponent,
    NumberDirective,
    CpfCnpjDirective,
    GroupNbCheckboxComponent,
    ImageUploadComponent,
    ImageUploadEmpresaComponent,
    PainelExpirationPlanComponent,
    ImageUploadPerfilComponent

  ],

  providers: [
    MessageService,
    VagaServices,
    NbToastrService,
    CurriculoServices,
    AreaAtuacaoServices,
    HabilidadeServices,
    SalarioServices,
    ImageService,
    VagasCandidatoServices,
    VagasEmpresaServices,
    PagseguroService,
    DadosServices,
    DatePipe,
    MercadopagoService,
    MensagemFromRecruiterService,
    MensagemCandidatoService,
    SuporteServices,
    NivelCargoServices,
    FuncaoCargoServices,
    CNHServices,
    TipoVagaServices,
    ExperienciaProfissionalServices,
    TecnologiaServices,
    PlanoServices
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule { }
