import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { LoadingService } from '../components/loading/loading.service';
import { Router } from '@angular/router';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';
import {_throw} from "rxjs/observable/throw";
import { LoginService } from '../../pages/login/login.service';
import Swal from 'sweetalert2';
import {  MessagesModel } from "../../pages/models/messages.model";
import { messages } from "../../pages/constantes/messages";



@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  activeRequests: number = 0;
  listErrorsMessage: MessagesModel[] = Object.assign(new MessagesModel(), messages)
  defaultMessage = 'Entre em contato com o Administrador Bantal'
  

  constructor(private loadingScreenService: LoadingService, private router: Router, private loginService: LoginService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.loadingScreenService.startLoading();

    let token = localStorage.getItem('token');

    if(token){
      request = request.clone({
        setHeaders: {
          "Authorization": "Bearer " + token
        }
      });
    }

    return next.handle(request).pipe(
      finalize(() => {
          this.loadingScreenService.stopLoading();
      })
    ).catch(error => {
      console.log(error)
        this.loadingScreenService.stopLoading();

        var messageAmigavel = this.getMessageFromKey(error.error.message)

        var titulo = (typeof error.error.titulo === 'undefined' ? 'Ops, problema no acesso! ' : error.error.titulo);
        var mensagemNegocio = (typeof error.error.mensagemNegocio == 'undefined' ? messageAmigavel : error.error.mensagemNegocio);

        
        console.log(messageAmigavel);
        if (error instanceof HttpErrorResponse && this.isErrorPermission(error.status)) {
          Swal.fire({
            type: 'error',
            title: titulo,
            text: mensagemNegocio
          }).then((resultado) => {
            this.loginService.logout();
          });

        } else if (error instanceof HttpErrorResponse && error.status == 401) {
          this.loginService.logout();
            this.router.navigate(['./auth/login']);

            return new EmptyObservable();
        } else if (error.status === 0){
          Swal.fire({
            type: 'error',
            title: 'Sistema sem comunicação',
            text: 'Entre em contato com o Administrador Bantal ' + error.status
          }).then((resultado) => {
            //this.loginService.logout();
          });
        } else if (error.status === 404 ){
          Swal.fire({
            type: 'error',
            title: titulo,
            text: mensagemNegocio
          }).then((resultado) => {
            this.loginService.logout();
          });
        } else if (error.status === 400 ){
          Swal.fire({
            type: 'error',
            title: titulo,
            text: mensagemNegocio
          }).then((resultado) => {

          });
        } else {
            Swal.fire({
              type: 'error',
              title: 'Problema ao realizar a operação',
              text: 'Entre em contato com o Administrador Bantal ' + error.status
            });
        }
    });
  };

  getMessageFromKey(key: string){
    for(var i = 0; i < messages.length; i++) {
      var keyParametro = this.listErrorsMessage[i].key;
      var message = this.listErrorsMessage[i].message;
      if( keyParametro == key){
        return message;
      }
    }
    return this.defaultMessage;
  }

  isErrorPermission(code: number){
    var arrayErrors = [401, 403, 500];

    for(var i = 0; i < arrayErrors.length; i++) {            
      if( arrayErrors[i] == code){
        return true;
      }
    }
  }

}
