import { ParametroModel } from './../../pages/models/parametro.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class NivelCargoServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/nivel-cargo/';

  private service$: Observable<ParametroModel[]>;

  constructor(private http: HttpClient) { }

  getTodosNiveisCargo(): Observable<ParametroModel[]> {
    return this.http.get<ParametroModel[]>(this.url).pipe(shareReplay(1));

  }

}
