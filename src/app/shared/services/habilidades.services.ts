import { ParametroModel } from './../../pages/models/parametro.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { AreaAtuacaoModel } from '../../pages/models/area.atuacao.model';

@Injectable()
export class HabilidadeServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/habilidade/';

  private habilidade$: Observable<ParametroModel[]>;

  constructor(private http: HttpClient) { }

  getTodasHabilidades(): Observable<ParametroModel[]> {
    if (!this.habilidade$) {
      this.habilidade$ = this.http.get<ParametroModel[]>(this.url).pipe(shareReplay(1));
    }
    return this.habilidade$;
  }

}
