import { environment } from './../../../environments/environment';
import { DadosAdministrativo } from './../../pages/models/dados-administrativo.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdministrativoService {

  protected url: string = environment.API + environment.BASE_URL + '/v1/painel-admin/';

  constructor(private http: HttpClient) { }

  getObterDados(): Observable<DadosAdministrativo> {
    return this.http.get<DadosAdministrativo>(this.url);
  }
}
