import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cidade } from '../models/cidades.model';
import { EstadosBr } from '../models/estadosbr.model';
import { map, shareReplay } from 'rxjs/operators';
import * as estados from "../../../assets/data/cidades.json";

@Injectable()
export class EstadoServices {


  private estados$: Observable<EstadosBr[]>;

  private cidades$: Observable<Cidade[]>;

  private cache: Map<string, Observable<Cidade[]>> = new Map<string, Observable<Cidade[]>>();


  constructor(private http: HttpClient) {
    //this.loadCidades();
  }

  loadCidades() {
    for (var _i = 1; _i < 28; _i++) {
      this.getCidades(_i);
    }
  }

  getCidades(idEstado: number) {
    return this.http.get<Cidade[]>('assets/data/cidadesBr.json').pipe(
        map((cidade: Cidade[]) => cidade.filter(c => c.estado == idEstado))
      );
  }

  getEstados(): Observable<EstadosBr[]> {
    return this.http.get<EstadosBr[]>('assets/data/estados.json');
  }

  /*
  * Novo metodo 14-12-2019
  */
  getEstadosBr() {
    if (!this.estados$) {
      this.estados$ = this.http.get<EstadosBr[]>('assets/data/estados.json').pipe(shareReplay(1));
    }
    return this.estados$;
  }

  obterCidades() {
    if (!this.cidades$) {
      this.cidades$ = this.http.get<Cidade[]>('assets/data/cidadesBr.json').pipe(shareReplay(1));
    }
    return this.cidades$;
  }

  set(key: string, value: Observable<Cidade[]>): void {
    this.cache.set(key, value);
  }

  has(key: string): boolean {
    return this.cache.has(key);
  }

  get(key: string): Observable<Cidade[]> {
    return this.cache.get(key);
  }

  obterEstadoFromSigla(val) {
    
    var estadosList = estados.estados;
    estadosList.forEach(function (value) {
      if(value.sigla === val){
        val = value.nome;
      }
    }); 
    return val;
  }

}
