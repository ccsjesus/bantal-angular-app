import { PlanoModel } from './../models/plano.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class PlanoServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/planos/';


  constructor(private http: HttpClient) { }

  getPlanosEmpresa(): Observable<PlanoModel[]> {
      return this.http.get<PlanoModel[]>(this.url + 'empresa').pipe(shareReplay(1));
  }

  getPlanosCandidatos(): Observable<PlanoModel[]> {
      return this.http.get<PlanoModel[]>(this.url + 'candidatos').pipe(shareReplay(1));
  }

}
