import { FUNCAO_EMPRESA, ROLE_EMPLOYER } from './../../pages/constantes';
import { ActivatedRoute, Router } from '@angular/router';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import * as firebase from 'firebase';
import { DatePipe } from '@angular/common';


@Injectable()
export class ChatFirebaseServices {

  constructor(protected router: Router,
              protected route: ActivatedRoute,
              public datepipe: DatePipe) { }

  criarSala(dadosSalaParam: any, funcaoSistema: any ){
    let ref = firebase.database().ref('salas/');

    let sala = { nomesala: dadosSalaParam.codigoselecao + '-' + dadosSalaParam.cpfcandidato}
    let dadosSala = {
      cpfcandidato: dadosSalaParam.cpfcandidato,
      codigoselecao: dadosSalaParam.codigoselecao,
      sala: sala
    }
    sessionStorage.setItem('dadosSala', JSON.stringify(dadosSala));
    ref.orderByChild('nomesala').equalTo(sala.nomesala).once('value', (snapshot: any) => {
      if (snapshot.exists()) {
        if(funcaoSistema === FUNCAO_EMPRESA || funcaoSistema === ROLE_EMPLOYER){
          this.router.navigate(['/chat-empresa']);
        } else {
          this.router.navigate(['/chat-candidato']);
        }
      } else {
        const newRoom = firebase.database().ref('salas/').push();
        newRoom.set(sala);
        if(funcaoSistema === FUNCAO_EMPRESA || funcaoSistema === ROLE_EMPLOYER){
          this.router.navigate(['/chat-empresa']);
        } else {
          this.router.navigate(['/chat-candidato']);
        }
      }
    });
  }

  obterMsg(dadosSalaParam: any, funcaoSistema: any ){
    let ref = firebase.database().ref('chats/');
  }

}
