import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Car } from '../domain/car';

@Injectable()
export class CarService {

    constructor(private http: HttpClient) { }

    getCarsSmall() {
    return this.http.get<any>('assets/data/cars.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => { return data; });
    }

    getCarsMedium() {
    return this.http.get<any>('assets/data/cars.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => { return data; });
    }

    getCarsLarge() {
    return this.http.get<any>('assets/data/cars.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => { return data; });
    }

  getCarsHuge() {
    return this.http.get<any>('assets/data/cars.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => { return data; });
  }

  usuarios1 = {
    "data": [
        {"cpf": "210.225.180-20", "nome": "Reife", "telefone": "15(022)937-82-53", "email": "dsad231ff"},
        {"cpf": "457.684.830-55", "nome": "Inwudir", "telefone": "15(022)937-82-53", "email": "gwregre345"},
        {"cpf": "402.586.360-09", "nome": "Gruagni", "telefone": "15(022)937-82-53", "email": "h354htr"},
        {"cpf": "138.596.000-01", "nome": "Belegtedor", "telefone": "15(022)937-82-53", "email": "j6w54qgh"},
        {"cpf": "628.158.720-05", "nome": "Tarsido", "telefone": "15(022)937-82-53", "email": "hrtwy34"},
        {"cpf": "442.783.170-26", "nome": "Houmirzuf", "telefone": "15(022)937-82-53", "email": "jejtyj"},
        {"cpf": "506.478.500-36", "nome": "Licamog", "telefone": "15(022)937-82-53", "email": "g43gr"},
        {"cpf": "533.855.820-32", "nome": "Mauwiuzi", "telefone": "15(022)937-82-53", "email": "greg34"},
        {"cpf": "389.213.060-48", "nome": "Ondkedey", "telefone": "15(022)937-82-53", "email": "h54hw5"},
        {"cpf": "423.039.740-20", "nome": "Krahwefo", "telefone": "15(022)937-82-53", "email": "245t2s"}
    ]
};

usuarios2 = {
  "data": [
      {"cpf": "499.830.940-45", "nome": "Gimuibor", "telefone": "15(022)937-82-53", "email": "dsad231ff"},
      {"cpf": "227.186.320-11", "nome": "Voyci", "telefone": "15(022)937-82-53", "email": "gwregre345"},
      {"cpf": "967.827.600-37", "nome": "Magrzu", "telefone": "15(022)937-82-53", "email": "h354htr"},
      {"cpf": "978.167.950-68", "nome": "Mauwiuzi", "telefone": "15(022)937-82-53", "email": "j6w54qgh"},
      {"cpf": "841.513.480-00", "nome": "Matrish", "telefone": "15(022)937-82-53", "email": "hrtwy34"},
      {"cpf": "065.989.120-40", "nome": "Licamog", "telefone": "15(022)937-82-53", "email": "jejtyj"},
      {"cpf": "831.953.030-00", "nome": "Buand", "telefone": "15(022)937-82-53", "email": "g43gr"},
      {"cpf": "904.728.740-12", "nome": "Matrish", "telefone": "15(022)937-82-53", "email": "greg34"},
      {"cpf": "403.388.810-18", "nome": "Keauk", "telefone": "15(022)937-82-53", "email": "h54hw5"},
      {"cpf": "196.818.820-75", "nome": "Ondkedey", "telefone": "15(022)937-82-53", "email": "245t2s"}
  ]
};
  getCars(): Observable<any> {
    return observableOf(this.usuarios1);
  }
}