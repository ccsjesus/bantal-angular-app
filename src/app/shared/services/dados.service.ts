import { ExpiracaoPlanoModel } from './../../pages/models/expiracao-plano.model';
import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { UsuarioRequest } from '../../pages/models/usuario.pass';
import { DadosUsuarioModel } from '../../pages/models/dados-usuario.model';

@Injectable()
export class DadosServices {

    protected url: string = environment.API + environment.BASE_URL + '/dados/';

    constructor(private http: HttpClient) { }

    alterarDados(resource: UsuarioRequest): Observable<UsuarioRequest> {
      return this.http.post<UsuarioRequest>(this.url + 'alterar', resource);
    }

    obterDadosUsuario(): Observable<UsuarioRequest> {
      return this.http.get<UsuarioRequest>(this.url);
    }

    obterDadosUsuarioToken(): Observable<DadosUsuarioModel> {
      return this.http.get<DadosUsuarioModel>(this.url + 'obter-dados-usuario');
    }

    obterExpiracaoPlano(): Observable<ExpiracaoPlanoModel> {
      return this.http.get<ExpiracaoPlanoModel>(this.url + 'obter-expiracao-plano');
    }
}
