import { TipoVagaModel } from './../../pages/models/tipo.vaga.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Curriculo } from '../../pages/models/curriculo.model';

@Injectable()
export class TipoVagaServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/tipo-vaga/';

  private tipoVaga$: Observable<TipoVagaModel[]>;

  constructor(private http: HttpClient) { }

  getTipoVaga(): Observable<TipoVagaModel[]> {
     return this.tipoVaga$ = this.http.get<TipoVagaModel[]>(this.url).pipe(shareReplay(1))
  }

  getTipoVagaHabilitadas(): Observable<TipoVagaModel[]> {
     return this.tipoVaga$ = this.http.get<TipoVagaModel[]>(this.url + 'habilitadas/').pipe(shareReplay(1))
  }

   getTipoVagaDesabilitadas(): Observable<TipoVagaModel[]> {
     return this.tipoVaga$ = this.http.get<TipoVagaModel[]>(this.url + 'desabilitadas/').pipe(shareReplay(1))
  }

  incluirTipoVaga(resource: TipoVagaModel): Observable<TipoVagaModel[]> {
     return this.http.post<TipoVagaModel[]>(this.url + 'incluir', resource);
  }

  desabilitarTipoVaga(codigoTipoVaga: string): Observable<string>{
      let url = this.url + 'desabilitar/' + codigoTipoVaga;
      return this.http.get<string>(url);
  }

  habilitarTipoVaga(codigoTipoVaga: string): Observable<string>{
      let url = this.url + 'habilitar/' + codigoTipoVaga;
      return this.http.get<string>(url);
  }

}
