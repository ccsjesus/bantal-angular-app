import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Taxonomia } from '../models/taxonomia.model';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class TaxonomiaServices {

  protected url: string = environment.API + environment.BASE_URL + '/taxonomia/';

  private tipoVaga$: Observable<Taxonomia[]>;

  private areasAtuacao$: Observable<Taxonomia[]>;

  constructor(private http: HttpClient) { }

  getAreasAtuacao(): Observable<Taxonomia[]> {
    if (!this.areasAtuacao$) {
      this.areasAtuacao$ = this.http.get<Taxonomia[]>(this.url + 'areas-atuacao').pipe(shareReplay(1));
    }
    return this.areasAtuacao$;
  }


  getTipoVaga(): Observable<Taxonomia[]> {
    if (!this.tipoVaga$) {
      this.tipoVaga$ = this.http.get<Taxonomia[]>(this.url + 'tipo-vaga').pipe(shareReplay(1));
    }
    return this.tipoVaga$;
  }
}
