import { ParametroModel } from './../../pages/models/parametro.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Profissao } from '../../pages/models/profissao.model';

@Injectable()
export class FuncaoCargoServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/funcao-cargo/';

  private service$: Observable<Profissao[]>;

  constructor(private http: HttpClient) { }

  getTodasFuncoesCargo(): Observable<Profissao[]> {
    return this.http.get<Profissao[]>(this.url).pipe(shareReplay(1));
  }

  recuperarProfissoesPorIdAreaAtuacao(idAreaAtuacao: any): Observable<Profissao[]> {
    let url = this.url + 'area-atuacao/' + idAreaAtuacao;
    return this.http.get<Profissao[]>(url).pipe(shareReplay(1));
  }
  
}
