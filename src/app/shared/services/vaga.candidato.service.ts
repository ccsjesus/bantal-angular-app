import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Vaga } from '../../pages/models/vaga.model';
import { environment } from '../../../environments/environment';
import { InscricaoVaga } from '../../pages/models/inscricao-vaga.model';

@Injectable()
export class VagasCandidatoServices {

    protected url: string = environment.API + environment.BASE_URL + '/vagas-candidato/';

    constructor(private http: HttpClient) { }

    getMinhaVaga(): Observable<Vaga[]> {
      let url = this.url;
      return this.http.get<Vaga[]>(url);
    }
    
    realizarInscricaoVaga(resource: InscricaoVaga): Observable<InscricaoVaga> {
      return this.http.post<InscricaoVaga>(this.url + 'realizar-inscricao-vaga', resource);
    }

    removerInscricaoVaga(cdVaga: string): Observable<string> {
      return this.http.delete<string>(this.url + cdVaga + '/remover-inscricao-vaga');
    }

}
