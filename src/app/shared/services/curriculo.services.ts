import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Curriculo } from '../../pages/models/curriculo.model';
import { FuncaoModel } from '../../pages/models/funcao.model';

@Injectable()
export class CurriculoServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/curriculo/';

  private curriculo$: Observable<Curriculo>;

  private tipoFuncao$: Observable<FuncaoModel[]>;

  private tipoNivel$: Observable<FuncaoModel[]>;

  constructor(private http: HttpClient) { }

  recuperarCurriculosPorIdProfissao(idProfissao: any): Observable<Curriculo[]> {
    let url = this.url + 'profissao/' + idProfissao;
    return this.http.get<Curriculo[]>(url).pipe(shareReplay(1));
  }

  getCurriculoCandidato(): Observable<Curriculo> {
    return this.http.get<Curriculo>(this.url);
  }

  atualizarDadosPessoais(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-dados-pessoais', resource);
  }

  atualizarFormacaoCandidato(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-formacao-candidato', resource);
  }

  atualizarExperienciaProfissionalCandidato(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-experiencia-profissional-candidato', resource);
  }

  atualizarCargoPreteridoCandidato(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-cargo-preterido-candidato', resource);
  }

  atualizarInformacoesPrincipais(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-informacoes-principais-candidato', resource);
  }

  atualizarIdiomasCandidato(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-idiomas-candidato', resource);
  }

  atualizarInformaticaCandidato(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-informatica-candidato', resource);
  }

  getNivel(): Observable<FuncaoModel[]> {
    if (!this.tipoNivel$) {
      this.tipoNivel$ = this.http.get<FuncaoModel[]>(this.url + 'obter-nivel').pipe(shareReplay(1));
    }
    return this.tipoNivel$;
  }

  getFuncaoCargo(): Observable<FuncaoModel[]> {
    if (!this.tipoFuncao$) {
      this.tipoFuncao$ = this.http.get<FuncaoModel[]>(this.url + 'obter-funcao').pipe(shareReplay(1));
    }
    return this.tipoFuncao$;
  }
}
