import { MensagemModel } from './../../pages/models/mensagem';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';


@Injectable()
export class MensagemCandidatoService {

  protected url: string = environment.API + environment.BASE_URL + '/candidato/mensagem/';

    constructor(private http: HttpClient) { }


    inserirMensagem(resource: MensagemModel): Observable<MensagemModel> {
      return this.http.post<MensagemModel>(this.url + 'inserir-mensagem', resource);
    }

    getMensagemCandidato(cdVaga): Observable<MensagemModel[]> {
      let url = this.url + cdVaga;
      return this.http.get<MensagemModel[]>(url).pipe(shareReplay(1));;
    }

    atualizarSitucaoMsgLida(cdVaga: MensagemModel[]): Observable<any> {
      return this.http.post<MensagemModel[]>(this.url + 'atualizar-situacao-mensagem-lida', cdVaga);
    }

    converterToMap(mapValue){
      let jsonObject = {};
      mapValue.forEach((value, key) => {
          jsonObject[key] = value
      });
      return jsonObject;
    }

}
