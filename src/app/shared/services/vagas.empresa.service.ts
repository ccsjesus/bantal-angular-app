import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Vaga } from '../../pages/models/vaga.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class VagasEmpresaServices {

    protected url: string = environment.API + environment.BASE_URL + '/vagas-empresa/';

    constructor(private http: HttpClient) { }

    getVagas(): Observable<Vaga[]> {
      let url = this.url;
      return this.http.get<Vaga[]>(url);
    }

    getVagasVigentes(): Observable<Vaga[]> {
      let url = this.url + 'vigentes';
      return this.http.get<Vaga[]>(url);
    }

    getVagasPorId(codigoVaga: Vaga): Observable<Vaga[]> {
      let url = this.url + codigoVaga;
      return this.http.get<Vaga[]>(url);
    }

}
