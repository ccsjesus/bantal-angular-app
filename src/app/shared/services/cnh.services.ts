import { CNH } from './../../pages/models/cnh.model';
import { ParametroModel } from './../../pages/models/parametro.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class CNHServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/cnh/';

  private service$: Observable<CNH[]>;

  constructor(private http: HttpClient) { }

  getTodasCategoriasCNH(): Observable<CNH[]> {
    return this.http.get<CNH[]>(this.url).pipe(shareReplay(1));

  }

}
