import { SuporteModel } from './../../pages/models/suporte.model';
import { ExpiracaoPlanoModel } from './../../pages/models/expiracao-plano.model';
import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { UsuarioRequest } from '../../pages/models/usuario.pass';
import { DadosUsuarioModel } from '../../pages/models/dados-usuario.model';

@Injectable()
export class SuporteServices {

    protected url: string = environment.API + environment.BASE_URL + '/suporte/';

    constructor(private http: HttpClient) { }

    enviarEmail(resource: SuporteModel): Observable<SuporteModel> {
      return this.http.post<SuporteModel>(this.url + 'enviar-email', resource);
    }
}
