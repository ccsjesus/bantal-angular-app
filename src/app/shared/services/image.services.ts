import { Injectable } from "@angular/core";
import {
  HttpClient
} from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from "rxjs";
import { environment } from '../../../environments/environment';
import { timeout, delay } from 'rxjs/internal/operators';

@Injectable({
  providedIn: "root"
})

export class ImageService {

  public progressSource = new BehaviorSubject<number>(0);

  protected url: string = environment.API + environment.BASE_URL + '/imagem/';

  constructor(private http: HttpClient) {}


  public uploadImage(image: any): Observable<any> {
        return this.http.post(this.url + 'v2/img-upload', image);
  }

  public uploadImagePerfil(image: any): Observable<any> {
        return this.http.post(this.url + 'img-upload-perfil', image);
  }

  public fakeUpload(): Observable<any> {
    return this.http.get('assets/data/estados.json').pipe(timeout(5000));
}
}
