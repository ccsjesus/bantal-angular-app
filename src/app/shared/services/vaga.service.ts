import { of as observableOf,  Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { Vaga } from '../../pages/models/vaga.model';
import { environment } from '../../../environments/environment';

@Injectable()
export class VagaServices {

    protected url: string = environment.API + environment.BASE_URL + '/vagas/';

    constructor(private http: HttpClient) { }

    getCandidatos(): Observable<any> {
      return this.http.get<Vaga[]>(this.url + 'v2/');
    }

    publicarVaga(resource: Vaga): Observable<Vaga[]> {
      return this.http.post<Vaga[]>(this.url + 'v2/publicar', resource);
    }

    getDetalheVaga(cdVaga: string): Observable<Vaga[]> {
      let url = this.url + 'v2/'+ cdVaga;
      return this.http.get<Vaga[]>(url);
    }

    getTodasVaga(): Observable<Vaga[]> {
      let url = this.url + 'obter-todas';
      return this.http.get<Vaga[]>(url);
    }

    atualizarVaga(resource: Vaga): Observable<Vaga[]> {
      return this.http.post<Vaga[]>(this.url + 'v2/atualizar', resource);
    }

    aprovarVaga(codigoVaga: string): Observable<string>{
      let url = this.url +'v1/aprovar/'+ codigoVaga;
      return this.http.get<string>(url);
    }

    excluirVaga(codigoVaga: string): Observable<string>{
      let url = this.url +'v1/'+ codigoVaga;
      return this.http.delete<string>(url);
    }

}
