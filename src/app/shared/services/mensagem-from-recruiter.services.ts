import { MensagemModel } from './../../pages/models/mensagem';
import { FUNCAO_EMPRESA } from './../../pages/constantes';
import { ActivatedRoute, Router } from '@angular/router';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { DatePipe } from '@angular/common';


@Injectable()
export class MensagemFromRecruiterService {

  protected url: string = environment.API + environment.BASE_URL + '/mensagem/';

    constructor(private http: HttpClient) { }

    getMensagemFromRecruiter(cdVaga): Observable<MensagemModel[]> {
      let url = this.url + cdVaga;
      return this.http.get<MensagemModel[]>(url).pipe(shareReplay(1));;
    }

    inserirMensagem(resource: MensagemModel): Observable<MensagemModel> {
      return this.http.post<MensagemModel>(this.url + 'inserir-mensagem', resource);
    }

    getMensagemEmpresa(cdVaga): Observable<MensagemModel[]> {
      let url = this.url + 'obter-mensagens-empresa/'+ cdVaga;
      return this.http.get<MensagemModel[]>(url).pipe(shareReplay(1));
    }

    getMensagensCandidatoEmpresa(cdVaga: MensagemModel): Observable<MensagemModel[]> {
      return this.http.post<MensagemModel[]>(this.url + 'obter-mensagens-candidato-empresa', cdVaga);
    }

    getMensagensNaoLidas(cdVaga): Observable<MensagemModel[]> {
      let url = this.url + 'obter-mensagens-nao-lidas/'+ cdVaga;
      return this.http.get<MensagemModel[]>(url).pipe(shareReplay(1));
    }

    atualizarMensagensNaoLidas(cdVaga): Observable<MensagemModel[]> {
      let url = this.url + 'atualizar-mensagens-nao-lidas/'+ cdVaga;
      return this.http.get<MensagemModel[]>(url).pipe(shareReplay(1));
    }

    getMensagensPorCdVaga(cdVaga: any): Observable<any> {
      return this.http.post<string>(this.url + 'obter-mensagens-codigo-vagas', cdVaga);
    }

    atualizarSitucaoMsgLida(cdVaga: MensagemModel[]): Observable<any> {
      return this.http.post<MensagemModel[]>(this.url + 'atualizar-situacao-mensagem-lida', cdVaga);
    }

    getMensagemNaoLidaUsuarios(cdVaga: any): Observable<any> {
      return this.http.post<MensagemModel[]>(this.url + 'obter-mensagem-nao-lida-usuarios', this.converterToMap(cdVaga));
    }

    converterToMap(mapValue){
      let jsonObject = {};
      mapValue.forEach((value, key) => {
          jsonObject[key] = value
      });
      return jsonObject;
    }

}
