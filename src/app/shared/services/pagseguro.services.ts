import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { Pagamento } from '../../pages/models/pagamento.model';
import { RegisteredCheckout } from '../../pages/models/registered-checkout.model';
import { Observable } from 'rxjs/Rx';
import { PreAprovado } from '../../pages/models/pre-aprovado.model';
import { DetalheTransacao } from '../../pages/models/detalhe-transacao.model';

@Injectable({providedIn: 'root'})
export class PagseguroService {

    protected url: string = environment.API + environment.BASE_URL + '/pagseguro';

    constructor(private http: HttpClient) { }

    checkout(pagamento: Pagamento) : Observable<RegisteredCheckout> {
        return this.http.post(this.url + '/checkout', pagamento);
    }

    checkoutTransparente(pagamento: PreAprovado): Observable<RegisteredCheckout> { 
        return this.http.post(this.url + '/checkout/pre-aprovado', pagamento);
    }

    obterDadosTransacao(codigoTransacao: string): Observable<DetalheTransacao> { 
        return this.http.get<DetalheTransacao>(this.url + '/transacao/codigo/'+ codigoTransacao);
    }
}