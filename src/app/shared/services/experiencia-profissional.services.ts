import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Curriculo } from '../../pages/models/curriculo.model';
import { FuncaoModel } from '../../pages/models/funcao.model';

@Injectable()
export class ExperienciaProfissionalServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/experiencia-profissional/';

  private curriculo$: Observable<Curriculo>;

  private tipoFuncao$: Observable<FuncaoModel[]>;

  private tipoNivel$: Observable<FuncaoModel[]>;

  constructor(private http: HttpClient) { }


  adicionarSemExperiencia(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'sem-experiencia', resource);
  }

  atualizarSemExperiencia(resource: Curriculo): Observable<Curriculo> {
    return this.http.post<Curriculo>(this.url + 'atualizar-sem-experiencia', resource);
  }


}
