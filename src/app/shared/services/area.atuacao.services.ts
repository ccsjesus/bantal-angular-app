import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';
import { Curriculo } from '../../pages/models/curriculo.model';
import { FuncaoModel } from '../../pages/models/funcao.model';
import { AreaAtuacaoModel } from '../../pages/models/area.atuacao.model';
import { AreaAtuacaoDTO } from '../../pages/models/area-atuacao-dto';

@Injectable()
export class AreaAtuacaoServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/area-atuacao/';

  private areaAtuacao$: Observable<AreaAtuacaoModel[]>;

  constructor(private http: HttpClient) { }

  getTodasAreaAtuacao(): Observable<AreaAtuacaoModel[]> {
      return this.areaAtuacao$ = this.http.get<AreaAtuacaoModel[]>(this.url);
  }

  obterTodasAreasAtuacaoAgrupadaPorCurriculo(): Observable<AreaAtuacaoDTO[]> {
    let url = this.url + 'agrupadas/';
    return this.areaAtuacao$ = this.http.get<AreaAtuacaoDTO[]>(url);
}

  getAreaAtuacaoHabilitadas(): Observable<AreaAtuacaoModel[]> {
     return this.areaAtuacao$ = this.http.get<AreaAtuacaoModel[]>(this.url + 'habilitadas/');
  }

   getAreaAtuacaoDesabilitadas(): Observable<AreaAtuacaoModel[]> {
     return this.areaAtuacao$ = this.http.get<AreaAtuacaoModel[]>(this.url + 'desabilitadas/');
  }

  incluirAreaAtuacao(resource: AreaAtuacaoModel): Observable<AreaAtuacaoModel> {
     return this.http.post<AreaAtuacaoModel>(this.url + 'incluir', resource);
  }

  desabilitarAreaAtuacao(codigoTipoVaga: string): Observable<string>{
      let url = this.url + 'desabilitar/' + codigoTipoVaga;
      return this.http.get<string>(url);
  }

  habilitarAreaAtuacao(codigoTipoVaga: string): Observable<string>{
      let url = this.url + 'habilitar/' + codigoTipoVaga;
      return this.http.get<string>(url);
  }

}
