import { PlanoPagamento } from './../models/plano-pagamento';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { FUNCAO_CANDIDATO, FUNCAO_ADMINISTRATOR, FUNCAO_EMPRESA, ROLE_CANDIDATE, ROLE_EMPLOYER, ROLE_ADMIN } from './../../pages/constantes';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class MercadopagoService {

  protected url: string = environment.API_MERCADOPAGO
  protected urlBantal: string = environment.API + environment.BASE_URL + '/pagamento';

  constructor(private http: HttpClient, private router: Router) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  public checkout(data): Observable<any> {
    return this.http.post(this.url + "/checkout", {observe: data})
  }

  public status(data): Observable<any> {
    return this.http.post(this.url + "/status", {observe: data})
  }

  public registrarPagamento(planoPagamento: PlanoPagamento): Observable<any> {
    return this.http.post(this.urlBantal + '/registrarPagamento', planoPagamento);
  }

  public registrarSolicitacaoPagamento(planoPagamento: PlanoPagamento): Observable<any> {
    return this.http.post(this.urlBantal + '/registrarSolicitacaoPagamento', planoPagamento);
  }

  public exibirMensagemPagamento(statusPagamento){
    switch (statusPagamento) {
            case 'approved':
                this.exibirMensagem("Pagamento aprovado!", 'Recebemos seu pagamento', 'success');
                break;
            case 'pending':
                this.exibirMensagem("Pagamento pendente!", 'Seu pagamento está pendente de processamento, assim que for processado seguiremos com o pedido!','warning');
                break;
            case 'failure':
                this.exibirMensagem("Pagamento não aprovado!", 'Verifique os dados e tente novamente', 'error');
                break;
        }
  }

  private exibirMensagem(msgInicial, textMsg, tipoMsg){
    Swal.fire(
      msgInicial,
      textMsg,
      tipoMsg
    ).then((resultado) => {
          this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
              this.router.navigate([this.getURL()]);
          });
    })
  }

  getURL() {
    //this.menuService.navigateHome();
    var dados = JSON.parse(localStorage.getItem('dados'));

    let acesso = localStorage.getItem('funcaoSistema');
    let url;
    if( acesso === FUNCAO_CANDIDATO || acesso === ROLE_CANDIDATE){
      url = '/selecao';
    } else if(acesso === FUNCAO_EMPRESA || acesso === ROLE_EMPLOYER) {
      url= '/empresa';
    } else if (acesso === FUNCAO_ADMINISTRATOR || acesso === ROLE_ADMIN) {
      url = '/empresa';
    }
    return url;
  }

}
