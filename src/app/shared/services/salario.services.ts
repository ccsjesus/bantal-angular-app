import { ParametroModel } from './../../pages/models/parametro.model';
import { of as observableOf, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class SalarioServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/salario/';

  private salario$: Observable<ParametroModel[]>;

  constructor(private http: HttpClient) { }

  getTodosSalarios(): Observable<ParametroModel[]> {
    if (!this.salario$) {
      this.salario$ = this.http.get<ParametroModel[]>(this.url).pipe(shareReplay(1));
    }
    return this.salario$;
  }

}
