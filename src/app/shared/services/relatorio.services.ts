import { of as observableOf, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import { environment } from '../../../environments/environment';

@Injectable()
export class RelatorioServices {

  protected url: string = environment.API + environment.BASE_URL + '/v1/curriculo';

  constructor(private http: HttpClient) { }

  gerarRelatorio(idUsuarios: any): Observable<any> {
    const headers = new HttpHeaders({
      'responseType' : 'blob'
    });
    return this.http.post<Blob>(this.url + '/imprimir', idUsuarios,
     {headers: headers, responseType: 'blob' as 'json' });
  }
  gerarRelatorioCandidato(): Observable<any> {
    const headers = new HttpHeaders({
      'responseType' : 'blob'
    });
    return this.http.post<Blob>(this.url + '/print-candidate',
     {headers: headers, responseType: 'blob' as 'json' });
  }
  

}
