angular.module('app', ['ngCurrencyMask'])

.config(function ($currencyMaskProvider) {
  // $currencyMaskProvider.setCurrency('BRL');
  
  /**
   * This is an example.
   * It will replace all zeros with 8 numbers.
   */
  // $currencyMaskProvider.addMaskMatch(/0/g, 8);
})

.controller('MainCtrl', function ($scope) {
  $scope.currencyOne = 'BRL';
  $scope.currencyTwo = 'USD';
});