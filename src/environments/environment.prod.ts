/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * API: 'https://bantal-full-app.herokuapp.com',
 */
export const environment = {
  production: true,
  API: 'https://www.spring.bantal.com.br',
  BASE_URL: '/api',
  API_MERCADOPAGO: 'https://mercadopago.bantal.com.br/mercadopago',
  URL_SISTEMA: 'https://recrutamento.bantal.com.br'
};
